package com.justshop.dao;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.justshop.business.exception.BusinessErrorTypes;
import com.justshop.business.exception.BusinessException;
import com.justshop.model.CartItem;
import com.justshop.model.Customer;
import com.justshop.model.DeliveryType;
import com.justshop.model.Product;

public class TestCustomerDAO {
	
	CustomerDAO dao; 
	EntityManager em;
	
	
	@Before
	public void setUp(){
		
		dao = new CustomerDAO();
		
		
	}
	
	@Test
	/*@Ignore*/
	public void testInsertCustomer() throws BusinessException {
		Customer customer = new Customer();
		customer.setFirstName("AneeshUniqueName");
		dao.insertCustomer(customer);
		assertEquals(1,dao.searchCustomersByName("AneeshUniqueName").size());
		
		customer = new Customer();
		customer.setFirstName("AzizUniqueName");
		dao.insertCustomer(customer);
		assertEquals(1,dao.searchCustomersByName("AzizUniqueName").size());
		
		customer = new Customer();
		customer.setFirstName("AnwitaUniqueName");
		dao.insertCustomer(customer);
		assertEquals(1,dao.searchCustomersByName("AnwitaUniqueName").size());
		
		
	}
	
	@Test
	/*@Ignore*/
	//TODO inserted by ANEESH on [Sep 26, 2015, 11:07:24 AM]: use existign product bsed on dml
	public void testAddExistingProductToCart() throws BusinessException {
		Customer customer = new Customer();
		customer.setCustomerId(3);
		Product product = new Product();
		product.setProductId(2);
		System.out.println(DeliveryType.AIR.toString());
		CartItem newItem = new CartItem(product,customer,1,DeliveryType.AIR.toString());
		dao.addProductToCart(newItem);
		
		//TODO inserted by ANEESH on [Sep 23, 2015, 5:30:49 PM]:  move to business layer
		
	}
	
	@Test
	/*@Ignore*/
	//TODO inserted by ANEESH on [Sep 26, 2015, 11:07:24 AM]: use existign product bsed on dml
	public void testRemoveExistingProductFromCart() throws BusinessException {
		Customer customer = new Customer();
		customer.setCustomerId(7);
		Product product = new Product();
		product.setProductId(2);
		System.out.println(DeliveryType.AIR.toString());
		CartItem newItem = new CartItem(product,customer,1,DeliveryType.AIR.toString());
		assertTrue(dao.removeProductFromCart(newItem));
		List<CartItem> fromDB = dao.fetchCart(customer);
		assertNotNull(fromDB);
		assertFalse(fromDB.contains(newItem));
	}
	
	@Test
	/*@Ignore*/
	//TODO inserted by ANEESH on [Sep 26, 2015, 11:07:24 AM]: use existign product bsed on dml
	public void testAddExistingProductToCartAgain() throws BusinessException {
		Customer customer = new Customer();
		customer.setCustomerId(3);
		Product product = new Product();
		product.setProductId(2);
		System.out.println(DeliveryType.AIR.toString());
		CartItem newItem = new CartItem(product,customer,1,DeliveryType.AIR.toString());
		dao.addProductToCart(newItem);
		
		//TODO inserted by ANEESH on [Sep 23, 2015, 5:30:49 PM]:  move to business layer
		
	}
	
		
	@Test
	/*@Ignore*/
	//TODO inserted by ANEESH on [Sep 26, 2015, 11:07:24 AM]: use new product bsed on dml
	public void testGetTotalPrice() throws BusinessException {
		//TODO inserted by ANEESH on [Sep 22, 2015, 8:43:06 AM]: make independent
		
		Customer customer = new Customer(1);
		
		assertEquals(150.0,dao.getTotalPrice(customer));
		
		
	}
	
	@Test
	/*@Ignore*/
	//TODO inserted by ANEESH on [Sep 26, 2015, 11:07:24 AM]: use new product bsed on dml
	public void testValidateCustomer() throws BusinessException {
		//TODO inserted by ANEESH on [Sep 22, 2015, 8:43:06 AM]: make independent
		
		Customer customer = dao.validate("jwhat@gmail.com","jwhat@gmail.com");
		
		assertNotNull(customer);
		assertEquals("jwhat@gmail.com", customer.getEmailAddress());
		assertEquals("jwhat@gmail.com", customer.getPassword());
		assertEquals(5, customer.getCustomerId());		
		
	}
	
	@Test
	/*@Ignore*/
	//TODO inserted by ANEESH on [Sep 26, 2015, 11:07:24 AM]: use new product bsed on dml
	public void testValidateCustomerInvalid() {
		//TODO inserted by ANEESH on [Sep 22, 2015, 8:43:06 AM]: make independent
		
		Customer customer = null;
		try {
			customer = dao.validate("kspacey@gmail.com","kspacey");
		} catch (BusinessException e) {
			// TODO Auto-generated catch block
			assertNull(customer);
			assertEquals(BusinessErrorTypes.NOT_VALID_CUSTOMER, e.getErrorType());
			
		}
		
		
		
		
	}
	
	
	
	
	
	
	@After
	public void tearDown(){
		dao = null;
	}

}
