package com.justshop.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.justshop.business.exception.BusinessException;
import com.justshop.model.OrderItem;
import com.justshop.model.Partner;

public class TestPartnerDAO {

	PartnerDAO dao; 
	
	
	@Before
	public void setUp(){
		
		dao = new PartnerDAO();
		
		
	}
	
	@Test
	/*@Ignore*/
	public void testGetPartnersByName() throws BusinessException{
		
		List<Partner> partnersFromDB = dao.getPartnersByName("Apple");
		assertNotNull(partnersFromDB);
		Partner fromDB = partnersFromDB.get(0);
		assertNotNull(fromDB);
		assertEquals("Apple",fromDB.getName());
		assertEquals("6 Greenleaf Ave",fromDB.getAddress());
		assertEquals("San Jose",fromDB.getCity());
		assertEquals("95111",fromDB.getZip());
		assertEquals("USA",fromDB.getCountry());
		assertEquals("CA",fromDB.getState());
	}
	
	@Test
	/*@Ignore*/
	public void testInsertPartner() throws BusinessException{
		Partner partner = new Partner();
		partner.setName("UniqueInsertedPartner1");
		partner.setAddress("5200 Metcalf Ave");
		partner.setCity("Overland Park");
		partner.setCountry("USA");
		partner.setZip("60004");
		partner.setState("KS");
		partner.setPartnerType(1);
		dao.insertPartner(partner);
		List<Partner> partnersFromDB = dao.getPartnersByName("UniqueInsertedPartner1");
		assertNotNull(partnersFromDB);
		Partner fromDB = partnersFromDB.get(0);
		assertNotNull(fromDB);
		assertEquals(partner.getName(),fromDB.getName());
		assertEquals(partner.getAddress(),fromDB.getAddress());
		assertEquals(partner.getState(),fromDB.getState());
		assertEquals(partner.getCity(),fromDB.getCity());
		assertEquals(partner.getCountry(),fromDB.getCountry());
		assertEquals(partner.getPartnerType(),fromDB.getPartnerType());
	}
	
	@Test
	public void testGetOrderRequestsForPartner() throws BusinessException{
		
		int partnerID = 5;//newegg.com
		Partner partner = null;
		
		partner = dao.getByID(partnerID,false);
		assertNotNull(partner);
		List<OrderItem> orderitems = dao.getOrderRequests(partner);
		assertNotNull(orderitems);
		assertEquals(3,orderitems.size());
		
		OrderItem orderItem1 = orderitems.get(0);
		assertEquals(5, orderItem1.getOrderItemID());
		assertEquals(3, orderItem1.getOrder().getOrderID());
		assertEquals(7, orderItem1.getProduct().getProductId());
		assertEquals(5, orderItem1.getDelivery().getDeliveryId());
		assertEquals(2, orderItem1.getQuantity());
		assertEquals(partnerID, orderItem1.getPartner().getPartnerId());

		OrderItem orderItem2 = orderitems.get(1);
		assertEquals(6, orderItem2.getOrderItemID());
		assertEquals(3, orderItem2.getOrder().getOrderID());
		assertEquals(8, orderItem2.getProduct().getProductId());
		assertEquals(6, orderItem2.getDelivery().getDeliveryId());
		assertEquals(2, orderItem2.getQuantity());
		assertEquals(partnerID, orderItem2.getPartner().getPartnerId());
		
	}

	
	
	@After
	public void tearDown(){
		dao = null;
	}
}
