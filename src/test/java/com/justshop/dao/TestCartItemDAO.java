package com.justshop.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.justshop.model.CartItem;
import com.justshop.model.Customer;
import com.justshop.model.DeliveryType;
import com.justshop.model.Product;

public class TestCartItemDAO {
	
	CartItemDAO dao; 
	EntityManager em;
	
	
	@Before
	public void setUp(){
		
		dao = new CartItemDAO();
		
		
	}
	
	@Test
	public void testEquality(){
		
		CartItem newItem = new CartItem(new Product(1),new Customer(1));
		
		Set<CartItem> items = new HashSet<CartItem>();
		
		items.add(new CartItem(new Product(1), new Customer(1)));
		
		assertEquals("Contains returnr true",true,items.contains(newItem));
		
	}
	
	@Test
	public void testInsertCartItem(){
		

		CartItem newItem = new CartItem(new Product(1),new Customer(3),3,DeliveryType.AIR.toString());
		
		CartItem got = dao.insertCartItem(newItem);
		assertNotNull(got);
		assertEquals(3, got.getCustomer().getCustomerId());
		assertEquals(1, got.getProduct().getProductId());
		assertEquals(3, got.getQuantity());
	}

}
