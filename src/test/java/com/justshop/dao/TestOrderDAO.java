package com.justshop.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.justshop.business.exception.BusinessException;
import com.justshop.model.CartItem;
import com.justshop.model.Customer;
import com.justshop.model.Order;
import com.justshop.model.OrderItem;
import com.justshop.model.PaymentDetails;
import com.justshop.model.TrackingStatus;
import com.justshop.vo.ResultListVO;
import com.justshop.vo.ResultVO;

public class TestOrderDAO {

	private OrderDAO orderDAO;
	private CustomerDAO customerDao;
	private PaymentDetailsDAO paymentDAO;
	private ProductDAO productDAO;
	EntityManager em;

	@Before
	public void setUp() {

		orderDAO = new OrderDAO();
		customerDao = new CustomerDAO();
		paymentDAO = new PaymentDetailsDAO();
		productDAO = new ProductDAO();
		em = orderDAO.getEntityManager();

	}

	@Test
	/*@Ignore*/
	public void testInsertOrder() throws BusinessException {
		int customerId = 1;
		int paymentId = 1;
		
		
		int prod1 = 2;
		int prod2 = 3;
		Customer customer = customerDao.getByID(customerId,em,false);
		PaymentDetails payment = paymentDAO.getByID(paymentId,em,false);
		
		//available qty
		Integer p1Qty = customer.getShoppingCartItems().get(0).getProduct().getAvailableQuantity();
		Integer p2Qty = customer.getShoppingCartItems().get(1).getProduct().getAvailableQuantity();
		
		//requested qty
		Integer reqP1Qty = customer.getShoppingCartItems().get(0).getQuantity();
		Integer reqP2Qty = customer.getShoppingCartItems().get(1).getQuantity();
		
		ResultVO<Order> result = orderDAO.insertOrder(customerId, paymentId);
		assertNotNull(result);
		assertTrue(result.isSuccessFlag());
		assertNotNull(result.getModel());
		int orderId = result.getModel().getOrderID();
		// after order inserted
		Order orderFromDB = orderDAO.getByID(orderId,em,false);
		
		assertNotNull(orderFromDB);
		assertNotNull(orderFromDB.getOrderID());
		assertEquals(customerId, orderFromDB.getCustomer().getCustomerId());
		assertEquals(paymentId, orderFromDB.getPaymentDetail().getPaymentDetailID());
		
		List<OrderItem> orderitems = orderFromDB.getOrderItems();
		
		OrderItem each = orderitems.get(0);
		assertEquals(orderId, each.getOrder().getOrderID());
		assertEquals(3, each.getPartner().getPartnerId());
		assertEquals(reqP1Qty, each.getQuantity());
		
		//verifying product info
		assertEquals(prod1, each.getProduct().getProductId());
		//also verifying product qty reduced from available qty in product table
		assertEquals((p1Qty-reqP1Qty),productDAO.getByID(prod1,false).getAvailableQuantity());
		
		
		each = orderitems.get(1);
		assertEquals(orderId, each.getOrder().getOrderID());
		assertEquals(1, each.getPartner().getPartnerId());
		assertEquals(reqP2Qty, each.getQuantity());
		
		//verifying product info
		assertEquals(prod2, each.getProduct().getProductId());
		//also verifying product qty reduced from available qty in product table
		assertEquals((p2Qty-reqP2Qty),productDAO.getByID(prod2,false).getAvailableQuantity());
		
		// the shopping cart for customer should be empty after order placement
		List<CartItem> cartItemsInCart = customerDao.fetchCart(customer);
		assertNotNull(cartItemsInCart);
		assertEquals(0,cartItemsInCart.size());
		
		
	}
	
	
	@Test
	/*@Ignore*/
	public void testInsertOrderUnavailableProduct() throws BusinessException {
		int customerId = 4;
		int paymentId = 4;
		
		
		int prod1 = 2;
		int prod2 = 6;
		Customer customer = customerDao.getByID(customerId,em,false);
		PaymentDetails payment = paymentDAO.getByID(paymentId,em,false);
		
		//available qty
		Integer p1Qty = customer.getShoppingCartItems().get(0).getProduct().getAvailableQuantity();
		Integer p2Qty = customer.getShoppingCartItems().get(1).getProduct().getAvailableQuantity();
		
		//requested qty
		Integer reqP1Qty = customer.getShoppingCartItems().get(0).getQuantity();
		Integer reqP2Qty = customer.getShoppingCartItems().get(1).getQuantity();
		
		ResultVO<Order> result = orderDAO.insertOrder(customerId, paymentId);
		System.out.println(result.getMessage());
		assertNotNull(result);
		assertTrue(result.isSuccessFlag());
		assertNotNull(result.getModel());
		int orderId = result.getModel().getOrderID();
		// after order inserted
		Order orderFromDB = orderDAO.getByID(orderId,em,false);
		
		assertNotNull(orderFromDB);
		assertNotNull(orderFromDB.getOrderID());
		assertEquals(customerId, orderFromDB.getCustomer().getCustomerId());
		assertEquals(paymentId, orderFromDB.getPaymentDetail().getPaymentDetailID());
		
		List<OrderItem> orderitems = orderFromDB.getOrderItems();
		
		assertNotNull(orderitems);
		assertEquals(1,orderitems.size()); //only one item should get inserted the other is not available
		
		OrderItem each = orderitems.get(0);
		assertEquals(orderId, each.getOrder().getOrderID());
		assertEquals(3, each.getPartner().getPartnerId());
		assertEquals(reqP1Qty, each.getQuantity());
		
		//verifying product info
		assertEquals(prod1, each.getProduct().getProductId());
		//also verifying product qty reduced from available qty in product table
		assertEquals((p1Qty-reqP1Qty),productDAO.getByID(prod1,false).getAvailableQuantity());
		
		
		
		//also verifying product qty reduced from available qty in product table
		assertEquals(p2Qty,productDAO.getByID(prod2, false).getAvailableQuantity()); // Product 2 should have same qty as before since it was not ordered
				
		
		
	}

	@Test
	/*@Ignore*/
	public void testGetOrder() throws BusinessException {

		int orderId = 1;
		
		Order order = orderDAO.getByID(orderId,em,false);
		System.out.println(order);
		assertNotNull(order);
		assertEquals(1, order.getCustomer().getCustomerId());
		assertEquals(2, order.getPaymentDetail().getPaymentDetailID());
		// TODO inserted by ANEESH on [Sep 26, 2015, 5:11:00 PM]: Fix this after
		// DML
		assertEquals(225.0, order.getTotalPaid());
		List<OrderItem> orderitems = order.getOrderItems();
		assertNotNull(orderitems);
		OrderItem orderItem1 = orderitems.get(0);

		assertEquals(1, orderItem1.getOrderItemID());
		assertEquals(orderId, orderItem1.getOrder().getOrderID());
		assertEquals(1, orderItem1.getProduct().getProductId());
		assertEquals(1, orderItem1.getDelivery().getDeliveryId());
		assertEquals(2, orderItem1.getPartner().getPartnerId());

		OrderItem orderItem2 = orderitems.get(1);
		assertEquals(2, orderItem2.getOrderItemID());
		assertEquals(orderId, orderItem2.getOrder().getOrderID());
		assertEquals(2, orderItem2.getProduct().getProductId());
		assertEquals(2, orderItem2.getDelivery().getDeliveryId());
		assertEquals(3, orderItem2.getPartner().getPartnerId());
	}

	/*@Test
	public void testUpdateOrder() {
		// TODO inserted by ANEESH on [Sep 22, 2015, 8:43:06 AM]: make
		// independent

		fail();

	}*/

	@Test
	/*@Ignore*/
	public void testCancelOrder() throws BusinessException {
		// TODO inserted by ANEESH on [Sep 22, 2015, 8:43:06 AM]: make
		// independent

		Order order = new Order(2);
		
		boolean cancelled = orderDAO.cancelOrder(2);
		assertTrue(cancelled);
		Order fromDB = orderDAO.getByID(2,em,false);
		assertTrue(fromDB.getIsCancelled());
		List<OrderItem> orderItems = fromDB.getOrderItems();
		for (OrderItem each : orderItems) {
			assertTrue("Order Item[" + each.getOrderItemID() + "] is not cancelled", each.getIsCancelled());
			assertEquals("Delivery Request[" + each.getDelivery().getDeliveryId() + "] is not cancelled",
					TrackingStatus.CANCELED.toString(), each.getDelivery().getTrackingStatus());
		}

	}

	@After
	public void tearDown() {
		orderDAO.closeEntityManager(em);
		orderDAO = null;		
		em=null;
		customerDao = null;
		paymentDAO = null;
		productDAO = null;
	}

}
