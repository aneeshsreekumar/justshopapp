package com.justshop.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Date;

import javax.persistence.EntityManager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.justshop.business.exception.BusinessException;
import com.justshop.model.Customer;
import com.justshop.model.PaymentDetails;

public class TestPaymentDetailsDAO {
	
	PaymentDetailsDAO dao; 
	CustomerDAO customerDAO;
	EntityManager em;
	
	
	@Before
	public void setUp(){
		
		dao = new PaymentDetailsDAO();
		customerDAO = new CustomerDAO();
		em = dao.getEntityManager();
		
	}

	
	@Test
	public void testInsertPaymentDetails() throws BusinessException {
		
		Customer customer = new Customer(3); 
		PaymentDetails paymentdetails = new PaymentDetails("1234 5678 1234 5678", new Date(),"Aneesh S",customer);
		dao.insertPaymentDetails(paymentdetails);;
		Customer fromDB = customerDAO.getByID(3,em,false);
		assertNotNull(fromDB);
		assertEquals(1,fromDB.getPaymentDetails().size());
		
	}
	
	@Test(expected=BusinessException.class)
	public void testInsertPaymentDetailsInvalidCustomer() throws BusinessException {
		
		Customer customer = new Customer(100); 
		PaymentDetails paymentdetails = new PaymentDetails("1234 5678 1234 5678", new Date(),"Aneesh S",customer);
		dao.insertPaymentDetails(paymentdetails);;
		
		
	}

	
	@Test
	public void testGetPaymentDetails() throws BusinessException {
		PaymentDetails payment = dao.getByID(1,false);
		assertNotNull(payment);
		assertEquals("0123 4567 8901 2345",payment.getCreditCardNumber());
		assertEquals("VISA",payment.getCreditCardType());
		assertEquals("Aneesh Sreekumar",payment.getNameOnCard());
		assertEquals(1,payment.getCustomer().getCustomerId());
	}
	
	
	@After
	public void tearDown(){
		dao.closeEntityManager(em);
		dao = null;
		customerDAO =null;
		
	}

}
