package com.justshop.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.justshop.model.Partner;
import com.justshop.model.Product;
import com.justshop.vo.ResultVO;

public class TestProductDAO{
	
	ProductDAO dao; 
	EntityManager em;
	
	
	@Before
	public void setUp(){
		
		dao = new ProductDAO();
		
		
	}
	
	
	@Test
	public void testInsertProduct(){
		
		Product product = new Product();
		product.setName("InsertedProduct1");
		product.setDescription("Laptop");
		product.setAvailableQuantity(2);
		product.setKeyWords("laptop dell hp asus");
		product.setPartner(new Partner(1));
		dao.insertProduct(product);
		assertEquals(1, dao.searchProductsByName("InsertedProduct1").size());
		
		product = new Product();
		product.setName("InsertedProduct2");
		product.setDescription("Laptop");
		product.setAvailableQuantity(2);
		product.setKeyWords("laptop dell hp");
		product.setPartner(new Partner(2));
		dao.insertProduct(product);
		assertEquals(1, dao.searchProductsByName("InsertedProduct2").size());
		
		product = new Product();
		product.setName("InsertedProduct3");
		product.setDescription("Mobile");
		product.setAvailableQuantity(2);
		product.setKeyWords("phone mobile");
		product.setPartner(new Partner(3));
		dao.insertProduct(product);
		assertEquals(1, dao.searchProductsByName("InsertedProduct3").size());
		
		//TODO inserted by ANEESH on [Sep 23, 2015, 2:27:45 PM]: add assert get
	}
	
	
	@Test
	public void testInsertBatchProducts(){
		Partner partner = new Partner(2);
		List<ResultVO<Product>> products = new ArrayList<ResultVO<Product>>();
		int batchSize = 10;//100 - 0.28s, 1000 - 2.9s, 10000 - 16s
		
		Product product = null;
		
		for(int i=1;i<=batchSize;i++){
		
			product =  new Product();
			
			product.setName("testInsertBatchProducts"+i);
			product.setDescription("Batch Products");
			product.setAvailableQuantity(2+i);
			product.setKeyWords("batch inserted product");
			product.setPartner(partner);
			products.add(new ResultVO<Product>(product));
			
		}	
		
		
		boolean success = dao.insertProductBatch(products);
			
		assertEquals(true, success);
		assertEquals(batchSize,dao.searchProductsByName("testInsertBatchProducts").size());
				
		//TODO inserted by ANEESH on [Sep 23, 2015, 2:27:45 PM]: add assert get
	}
	
	
	@Test
	public void testInsertBatchProductsFailInBetween(){
		Partner partner = new Partner(2);
		List<ResultVO<Product>> products = new ArrayList<ResultVO<Product>>();
		int batchSize = 3;
		
		Product product = null;
		
		for(int i=1;i<=batchSize;i++){
		
			product =  new Product();
			
			product.setName("testInsertBatchProductsFailInBetween"+i);
			product.setDescription("Batch Products");
			product.setAvailableQuantity(2+i);
			product.setKeyWords("batch inserted product");
			if(batchSize<3)
				product.setPartner(partner); //hence last product would not have partner and should fail entire batch
			products.add(new ResultVO<Product>(product));
			
		}	
		
		
		boolean success = dao.insertProductBatch(products);
			
		assertEquals(false, success);
		assertEquals(0,dao.searchProductsByName("testInsertBatchProductsFailInBetween").size());
		
		
		
				
		//TODO inserted by ANEESH on [Sep 23, 2015, 2:27:45 PM]: add assert get
	}
	
	
	
	@Test
	public void testUpdateProduct(){
		Product product = new Product(5,"TestProductDAO.testUpdateProduct",3,"Dell Inspiron","baby einstein take along newkeyword");
		product.setUnitPrice(25.0);
		product.setStatus("In Store");
		dao.updateProduct(product);
		Product fromDB = dao.getByID(5,false);
		assertEquals(product, fromDB);
		assertEquals(product.getName(), fromDB.getName());
		assertEquals(product.getDescription(), fromDB.getDescription() );
		assertEquals(product.getAvailableQuantity(), fromDB.getAvailableQuantity() );
		assertEquals(product.getKeyWords(), fromDB.getKeyWords());
		assertEquals(product.getUnitPrice(), fromDB.getUnitPrice());
		assertEquals(product.getStatus(), fromDB.getStatus());
	}
	
	/*@Test
	/*@Ignore
	public void testSearchProductByKeyWordsUsingLuceneFullText(){
		//TODO inserted by ANEESH on [Sep 23, 2015, 4:21:41 PM]: Not working as expected
		List<Product> products = dao.searchProductsByKeyWord("dell");
		assertEquals(2, products.size());
		products = dao.searchProductsByKeyWord("hp");
		assertEquals(1, products.size());
		products = dao.searchProductsByKeyWord("laptop");
		assertEquals(1, products.size());
		products = dao.searchProductsByKeyWord("dell hp");
		//TODO inserted by ANEESH on [Sep 22, 2015, 4:02:49 PM]: check if possible to have dell hp considered as one single phrase
		
		 * assertEquals(1, products.size());
		assertEquals(1, products.size());
		//assertEquals(Arrays.asList(new Product(1),new Product(2)),products);
		
	}*/
	
	@Test
	public void testSearchProduct(){
		//TODO inserted by ANEESH on [Sep 23, 2015, 4:21:41 PM]: Not working as expected
		List<Product> products = dao.searchProducts("samsung");
		assertEquals(2, products.size());
		products = dao.searchProducts("galaxy");
		assertEquals(1, products.size());
		products = dao.searchProducts("tablet");
		assertEquals(2, products.size());
		products = dao.searchProducts("baby");
		assertEquals(3, products.size());
		//assertEquals(Arrays.asList(new Product(1),new Product(2)),products);
		
	}
	
		
	
	@Test
	public void testGetProductByID(){
		Product prod = dao.getByID(1,false);
		assertNotNull("Product not found",prod);
		assertEquals("Apple iPad 2 MC769LL Tablet 16GB, WiFi, Black (Certified Refurbished)",prod.getName());
		assertEquals("glossy widescreen Multi-Touch display with IPS technology",prod.getDescription());
		assertEquals(2,prod.getPartner().getPartnerId());
		assertEquals("apple ipad iOS A5 tablet",prod.getKeyWords());
		assertEquals("In Store",prod.getStatus());
		assertEquals(30,prod.getAvailableQuantity());
		
	}
	
	
	
	@After
	public void tearDown(){
		dao = null;
	}

}
