package com.justshop.service.representation;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestHeaderRepresentation {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		CustomerHeaderRepresentation header = new CustomerHeaderRepresentation();
		CustomerMiniRepresentation customer = new CustomerMiniRepresentation();

		List<LinkRepresentation> links = new ArrayList<LinkRepresentation>();
		LinkRepresentation self = new LinkRepresentation(LinkRepresentationTypes.SELF_CUSTOMER, "");
		links.add(self);
		LinkRepresentation cart = new LinkRepresentation(LinkRepresentationTypes.GET_CART, "");
		links.add(cart);
		customer.setLinks(links);
		header.setCustomer(customer);
		try {

			File file = new File(
					"C:\\Users\\ANEESH\\Projects\\workspace_a\\JustShop\\src\\test\\java\\com\\justshop\\service\\representation\\file.xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(HeaderRepresentation.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			jaxbMarshaller.marshal(header, file);
			jaxbMarshaller.marshal(header, System.out);

		} catch (JAXBException e) {
			e.printStackTrace();
		}

	}

}
