package com.justshop.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.justshop.business.exception.BusinessErrorTypes;
import com.justshop.business.exception.BusinessException;
import com.justshop.model.Partner;
import com.justshop.model.Product;
import com.justshop.vo.ResultListVO;
import com.justshop.vo.ResultVO;

public class TestPartnerBusiness {
	PartnerBusinessFacade facade;
	

	@Before
	public void setUp() {

		facade = new PartnerBusiness();

	}

	@Test
	/*@Ignore*/
	public void testGet() throws BusinessException {

		ResultVO<Partner> partnerVO = facade.get(2);
		assertNotNull(partnerVO);
		assertEquals(BusinessErrorTypes.SUCCESS, partnerVO.getMessage());
		assertEquals(2, partnerVO.getModel().getPartnerId());
		assertEquals("Walmart", partnerVO.getModel().getName());
		assertEquals("394 Manchester Blvd", partnerVO.getModel().getAddress());
	}
	
	@Test
	/*@Ignore*/
	public void testGetByName() throws BusinessException {
		ResultListVO<Partner> partnerVO = facade.getByName("Walmart");
		assertNotNull(partnerVO);
		assertEquals(BusinessErrorTypes.SUCCESS, partnerVO.getMessage());
		assertEquals(2, partnerVO.getModelList().size());
		assertEquals("Walmart",partnerVO.getModelList().get(0).getName());
		assertEquals("Walmart.com",partnerVO.getModelList().get(1).getName());
	}

	@Test
	/*@Ignore*/
	public void testCreate() throws BusinessException {
		Partner partner = new Partner();
		partner.setName("BusinessTestCasePartner1");
		partner.setAddress("5000 Metcalf Ave");
		partner.setCity("Overland Park");
		partner.setCountry("USA");
		partner.setZip("60004");
		partner.setState("KS");
		partner.setPartnerType(2);

		
		ResultVO<Partner> partnerVO = facade.create(partner);
		
		
		assertNotNull(partnerVO);
		assertEquals(BusinessErrorTypes.SUCCESS, partnerVO.getMessage());
		
		partnerVO = facade.get(partnerVO.getModel().getPartnerId());// getting from DB
		
		assertEquals(partner.getName(), partnerVO.getModel().getName());
		assertEquals(partner.getAddress(), partnerVO.getModel().getAddress());
		assertEquals(partner.getCity(), partnerVO.getModel().getCity());
		assertEquals(partner.getCountry(), partnerVO.getModel().getCountry());
		assertEquals(partner.getZip(), partnerVO.getModel().getZip());
		assertEquals(partner.getState(), partnerVO.getModel().getState());
		assertEquals(partner.getPartnerType(), partnerVO.getModel().getPartnerType());

		// assertEquals(1,facade.getByName("BusinessTestCaseUniqueName").size());
	}
	
	@Test
	/*@Ignore*/
	public void testUpdatePartner() throws BusinessException {
		
		Partner partner = new Partner();
		partner.setPartnerId(5);
		partner.setName("Newegg-NameUpdated");
		partner.setAddress("5000 Metcalf Ave");
		partner.setCity("Overland Park");
		partner.setCountry("USA");
		partner.setZip("60004");
		partner.setState("KS");
		partner.setPartnerType(2);

		
		ResultVO<Partner> partnerVO = facade.update(partner);
		
		
		assertNotNull(partnerVO);
		assertEquals(BusinessErrorTypes.SUCCESS, partnerVO.getMessage());
		
		partnerVO = facade.get(partnerVO.getModel().getPartnerId());// getting from DB
		
		assertEquals(partner.getName(), partnerVO.getModel().getName());
		assertEquals(partner.getAddress(), partnerVO.getModel().getAddress());
		assertEquals(partner.getCity(), partnerVO.getModel().getCity());
		assertEquals(partner.getCountry(), partnerVO.getModel().getCountry());
		assertEquals(partner.getZip(), partnerVO.getModel().getZip());
		assertEquals(partner.getState(), partnerVO.getModel().getState());
		assertEquals(partner.getPartnerType(), partnerVO.getModel().getPartnerType());
		
	}

	@Test
	/*@Ignore*/
	public void testAddValidProduct() throws BusinessException {
		
		Product product = new Product();
		product.setName("TestPartnerBusiness.testAddValidProduct1");
		product.setDescription("Laptop");
		product.setAvailableQuantity(2);
		product.setKeyWords("laptop dell hp asus");
		product.setPartner(new Partner(2));
		product.setStatus("In Store");
		
		
		ResultVO<Product> productVO = facade.addProduct(product);
		assertNotNull(productVO);
		assertEquals(BusinessErrorTypes.SUCCESS, productVO.getMessage());
		assertEquals(product.getName(), productVO.getModel().getName());
		assertEquals(product.getDescription(), productVO.getModel().getDescription());
		assertEquals(product.getAvailableQuantity(), productVO.getModel().getAvailableQuantity());
		assertEquals(product.getKeyWords(), productVO.getModel().getKeyWords());
		assertNotNull(productVO.getModel().getPartner());
		assertEquals(product.getPartner().getPartnerId(), productVO.getModel().getPartner().getPartnerId());
		
	}
	
	@Test
	/*@Ignore*/
	public void testAddProductWithNoPartner() throws BusinessException {
		
		Product product = new Product();
		product.setName("TestPartnerBusinessInsertedProduct2");
		product.setDescription("Laptop");
		product.setAvailableQuantity(2);
		product.setKeyWords("laptop dell hp asus");
		product.setPartner(new Partner());
		
		
		ResultVO<Product> productVO = facade.addProduct(product);
		assertNotNull(productVO);
		assertEquals(BusinessErrorTypes.NO_PARTNER_ID_GIVEN, productVO.getMessage());
		assertEquals(null,productVO.getModel());
		
	}
	
	@Test
	/*@Ignore*/
	public void testAddProductWithNoInputGiven() throws BusinessException {
		
		Product product = null;
		
		
		ResultVO<Product> productVO = facade.addProduct(product);
		assertNotNull(productVO);
		assertEquals(BusinessErrorTypes.NO_PRODUCTS_FOUND, productVO.getMessage());
		assertEquals(null,productVO.getModel());
		
	}
	
	@Test
	/*@Ignore*/
	public void testAddProductWithExistingProductID() throws BusinessException {
		
		Product product = new Product(1);
		product.setName("TestPartnerBusinessInsertedProduct2");
		product.setDescription("Laptop");
		product.setAvailableQuantity(2);
		product.setKeyWords("laptop dell hp asus");
		product.setPartner(new Partner(3));
		
		
		ResultVO<Product> productVO = facade.addProduct(product);
		assertNotNull(productVO);
		assertEquals(BusinessErrorTypes.PRODUCT_EXISTS, productVO.getMessage());
		assertEquals(null,productVO.getModel());
		
	}
	
	@Test
	/*@Ignore*/
	public void testAddProductWithNonExistentPartnerID() throws BusinessException {
		
		Product product = new Product();
		product.setName("TestPartnerBusinessInsertedProduct2");
		product.setDescription("Laptop");
		product.setAvailableQuantity(2);
		product.setKeyWords("laptop dell hp asus");
		product.setPartner(new Partner(100));
		
		
		ResultVO<Product> productVO = facade.addProduct(product);
		assertNotNull(productVO);
		assertEquals(BusinessErrorTypes.NO_PARTNER_FOUND, productVO.getMessage());
		assertEquals(null,productVO.getModel());
		
	}
	
	@Test
	
	public void testAddProductsBatchValid() throws BusinessException {
		int batchSize = 10;
		Partner partner = new Partner(3);
		String name ="TestPartnerBusiness.testAddProductsBatchValid";
		List<Product> products = new ArrayList<Product>(batchSize);
		
		
		Product product = null;
		
		for(int i=1;i<=batchSize;i++){
		
			product =  new Product();			
			product.setName(name+i);
			product.setDescription("Batch Products");
			product.setAvailableQuantity(2+i);
			product.setKeyWords("batch inserted product");
			product.setPartner(partner);
			product.setStatus("In Store");
			products.add(product);
			
			
		}	
		Map<Integer,ResultVO<Product>> productVOMap = facade.addProducts(products);
		assertNotNull(productVOMap);
		assertEquals(batchSize, productVOMap.size());
		
		for(int index=0;index<batchSize; index++){
			assertEquals(BusinessErrorTypes.SUCCESS, productVOMap.get(index+1).getMessage());
			assertEquals(products.get(index).getName(), productVOMap.get(index+1).getModel().getName());
			assertEquals(products.get(index).getDescription(), productVOMap.get(index+1).getModel().getDescription());
			assertEquals(products.get(index).getAvailableQuantity(), productVOMap.get(index+1).getModel().getAvailableQuantity());
			assertEquals(products.get(index).getKeyWords(), productVOMap.get(index+1).getModel().getKeyWords());
			assertNotNull(productVOMap.get(index+1).getModel().getPartner());
			assertEquals(products.get(index).getPartner().getPartnerId(), productVOMap.get(index+1).getModel().getPartner().getPartnerId());	
			index++;
		}
		
		
	}
	
	@Test
	/*@Ignore*/
	public void testAddProductsBatchWithOneInvalidPartner() throws BusinessException {
		int batchSize = 10;
		Partner partner = new Partner(3);
		String name ="TestPartnerBusiness.testAddProductsBatchWithOneInvalidPartner";
		List<Product> products = new ArrayList<Product>(batchSize);
		
		
		Product product = null;
		
		for(int i=1;i<=batchSize;i++){
		
			product =  new Product();			
			product.setName(name+i);
			product.setDescription("Batch Products");
			product.setAvailableQuantity(2+i);
			product.setKeyWords("batch inserted product");
			product.setStatus("In Store");
			if(i<batchSize)
				product.setPartner(partner); // will cause last product partner to be null which would cause it to fail entire batch
			
			products.add(product);
			
		}	
		Map<Integer,ResultVO<Product>> productVOMap = facade.addProducts(products);
		assertNotNull(productVOMap);
		assertEquals(batchSize, productVOMap.size());
		
		for(int index=0;index<batchSize;index++){
			if(index==9){//the last record fails with no partner id given message
				assertEquals(BusinessErrorTypes.NO_PARTNER_ID_GIVEN,  productVOMap.get(index+1).getMessage());
				assertEquals(products.get(index).getName(), productVOMap.get(index+1).getModel().getName()); 
				assertEquals(products.get(index).getDescription(), productVOMap.get(index+1).getModel().getDescription());
				assertEquals(products.get(index).getAvailableQuantity(), productVOMap.get(index+1).getModel().getAvailableQuantity());
				assertEquals(products.get(index).getKeyWords(), productVOMap.get(index+1).getModel().getKeyWords());
				assertNotNull(productVOMap.get(index+1).getModel().getPartner());
				assertEquals(products.get(index).getPartner().getPartnerId(), productVOMap.get(index+1).getModel().getPartner().getPartnerId());
					
			}else{// all others pass
				assertEquals(BusinessErrorTypes.SUCCESS, productVOMap.get(index+1).getMessage());
				assertEquals(products.get(index).getName(), productVOMap.get(index+1).getModel().getName());
				assertEquals(products.get(index).getDescription(), productVOMap.get(index+1).getModel().getDescription());
				assertEquals(products.get(index).getAvailableQuantity(), productVOMap.get(index+1).getModel().getAvailableQuantity());
				assertEquals(products.get(index).getKeyWords(), productVOMap.get(index+1).getModel().getKeyWords());
				assertNotNull(productVOMap.get(index+1).getModel().getPartner());
				assertEquals(products.get(index).getPartner().getPartnerId(), productVOMap.get(index+1).getModel().getPartner().getPartnerId());
				
			}
				
			index++;
		}
		
		
	}

	
	@After
	public void tearDown() {
		facade = null;
	}

}
