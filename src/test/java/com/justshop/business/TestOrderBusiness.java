package com.justshop.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.justshop.business.exception.BusinessErrorTypes;
import com.justshop.business.exception.BusinessException;
import com.justshop.model.Order;
import com.justshop.model.OrderItem;
import com.justshop.vo.ResultVO;

public class TestOrderBusiness {
	OrderBusinessFacade facade;
	

	@Before
	public void setUp() {

		facade = new OrderBusiness();

	}

	@Test
	/*@Ignore*/
	public void testGetOrder() throws BusinessException {
		
		int orderId = 2;
		ResultVO<Order> orderVO = facade.getOrder(orderId);
		assertNotNull(orderVO);
		assertEquals(BusinessErrorTypes.SUCCESS, orderVO.getMessage());
		Order order = orderVO.getModel();
		assertNotNull(order);
		assertEquals(2, orderVO.getModel().getOrderID());
		
		
		assertEquals(2, order.getCustomer().getCustomerId());
		assertEquals(1, order.getPaymentDetail().getPaymentDetailID());
		// TODO inserted by ANEESH on [Sep 26, 2015, 5:11:00 PM]: Fix this after
		// DML
		assertEquals(200.0, order.getTotalPaid());
		List<OrderItem> orderitems = order.getOrderItems();
		assertNotNull(orderitems);
		OrderItem orderItem = orderitems.get(0);

		assertEquals(3, orderItem.getOrderItemID());
		assertEquals(orderId, orderItem.getOrder().getOrderID());
		assertEquals(3, orderItem.getProduct().getProductId());
		assertEquals(3, orderItem.getDelivery().getDeliveryId());
		assertEquals(1, orderItem.getPartner().getPartnerId());

		OrderItem orderItem2 = orderitems.get(1);
		assertEquals(4, orderItem2.getOrderItemID());
		assertEquals(orderId, orderItem2.getOrder().getOrderID());
		assertEquals(4, orderItem2.getProduct().getProductId());
		assertEquals(4, orderItem2.getDelivery().getDeliveryId());
		assertEquals(2, orderItem2.getPartner().getPartnerId());
	}
	
	
	@Test
	/*@Ignore*/
	public void testGetOrderNotFound() throws BusinessException {

		ResultVO<Order> orderVO = facade.getOrder(100);
		assertNotNull(orderVO);
		assertEquals(BusinessErrorTypes.NO_ORDER_FOUND, orderVO.getMessage());
		
	}
	
	@Test
	/*@Ignore*/
	public void testGetOrderNoInputGiven() throws BusinessException {

		ResultVO<Order> orderVO = facade.getOrder(null);
		assertNotNull(orderVO);
		assertEquals(BusinessErrorTypes.NO_INPUT_GIVEN, orderVO.getMessage());
		
	}
	
	

	@Test
	/*@Ignore*/
	public void testConfirmOrderValid() throws BusinessException {
		
		int customerId = 5;
		int paymentId = 5;
		
		
		ResultVO<Order> result = facade.confirmOrder(customerId, paymentId);
		assertNotNull(result);
		assertNotNull(result.getMessage());
		//assertTrue(result.getMessage().contains(BusinessErrorTypes.SUCCESS));
		assertTrue(result.isSuccessFlag());
		assertNotNull(result.getModel());
		Integer orderId = result.getModel().getOrderID();
		// after order inserted
		ResultVO<Order> orderFromDB = facade.getOrder(orderId);
		
		assertNotNull(orderFromDB);
		assertNotNull(orderFromDB.getModel());
		assertNotNull(orderFromDB.getModel().getOrderID());
		assertEquals(customerId, orderFromDB.getModel().getCustomer().getCustomerId());
		assertEquals(paymentId, orderFromDB.getModel().getPaymentDetail().getPaymentDetailID());
	}
	
	@Test
	/*@Ignore*/
	public void testConfirmOrderInValidNoCustomer() throws BusinessException {
		
		int customerId = 100;
		int paymentId = 1;
				
		ResultVO<Order> result = facade.confirmOrder(customerId, paymentId);
		assertNotNull(result);
		assertEquals(BusinessErrorTypes.NO_CUSTOMER_EXISTS,result.getMessage());
		assertTrue(!result.isSuccessFlag());
		assertEquals(null,result.getModel());
	}
	
	@Test
	/*@Ignore*/
	public void testConfirmOrderInValidNoPayment() throws BusinessException {
		
		int customerId = 6;//customer having no payment
		int paymentId = 1;
		
		
		ResultVO<Order> result = facade.confirmOrder(customerId, paymentId);
		assertNotNull(result);
		assertEquals(BusinessErrorTypes.NO_PAYMENTS_ADDED,result.getMessage());
		assertTrue(!result.isSuccessFlag());
		assertEquals(null,result.getModel());
	}
	
	@Test
	/*@Ignore*/
	public void testConfirmOrderInValidPaymtSelNotExist() throws BusinessException {
		
		int customerId = 5;
		int paymentId = 4;// invalid payment selected
		
		
		ResultVO<Order> result = facade.confirmOrder(customerId, paymentId);
		assertNotNull(result);
		assertEquals(BusinessErrorTypes.SELECTED_PAYMENT_NOT_FOUND,result.getMessage());
		assertTrue(!result.isSuccessFlag());
		assertEquals(null,result.getModel());
	}
	
	@Test
	/*@Ignore*/
	public void testConfirmOrderInValidNoInputGiven() throws BusinessException {
		
		
		
		
		ResultVO<Order> result = facade.confirmOrder(null, null);
		assertNotNull(result);
		assertEquals(BusinessErrorTypes.NO_INPUT_GIVEN,result.getMessage());
		assertTrue(!result.isSuccessFlag());
		assertEquals(null,result.getModel());
	}
	
	@Test
	/*@Ignore*/
	public void testCancelOrder() throws BusinessException{
		int orderId = 2;
		ResultVO<Order> orderVO = facade.cancelOrder(orderId);
		assertNotNull(orderVO);
		assertEquals(BusinessErrorTypes.SUCCESS, orderVO.getMessage());
		//detailed test already done in dao level
	}
	
	@Test
	/*@Ignore*/
	public void testCancelOrderNoInputGiven() throws BusinessException{
		
		ResultVO<Order> orderVO = facade.cancelOrder(null);
		assertNotNull(orderVO);
		assertEquals(BusinessErrorTypes.NO_INPUT_GIVEN, orderVO.getMessage());
		//detailed test already done in dao level
	}
	
	@Test
	/*@Ignore*/
	public void testCancelOrderNoOrderFound() throws BusinessException{
		
		ResultVO<Order> orderVO = facade.cancelOrder(100);
		assertNotNull(orderVO);
		assertEquals(BusinessErrorTypes.NO_ORDER_FOUND, orderVO.getMessage());
		//detailed test already done in dao level
	}
	
	@After
	public void tearDown() {
		facade = null;
	}

}
