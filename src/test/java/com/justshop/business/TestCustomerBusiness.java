package com.justshop.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.justshop.business.exception.BusinessErrorTypes;
import com.justshop.business.exception.BusinessException;
import com.justshop.model.CartItem;
import com.justshop.model.Customer;
import com.justshop.model.DeliveryType;
import com.justshop.model.PaymentDetails;
import com.justshop.model.Product;
import com.justshop.service.representation.PaymentRepresentation;
import com.justshop.vo.ResultListVO;
import com.justshop.vo.ResultVO;

public class TestCustomerBusiness {
	CustomerBusinessFacade facade;
	

	@Before
	public void setUp() {

		facade = new CustomerBusiness();

	}

	@Test
	/*@Ignore*/
	public void testGet() throws BusinessException {

		ResultVO<Customer> customerVO = facade.get(1);
		assertNotNull(customerVO);
		assertEquals(BusinessErrorTypes.SUCCESS, customerVO.getMessage());
		assertEquals(1, customerVO.getModel().getCustomerId());
		assertEquals("James", customerVO.getModel().getFirstName());
		assertEquals("Butt", customerVO.getModel().getLastName());
	}

	@Test
	/*@Ignore*/
	public void testCreate() throws BusinessException {
		Customer customer = new Customer("Aneesh", "Sreekumar", "asreekumar@luc.edu", "888 888 8888");

		customer.setFirstName("BusinessTestCaseUniqueName");
		ResultVO<Customer> customerVO = facade.create(customer);
		
		assertNotNull(customerVO);
		assertEquals(BusinessErrorTypes.SUCCESS, customerVO.getMessage());
		
		assertEquals(customer.getLastName(), customerVO.getModel().getLastName());
		assertEquals(customer.getFirstName(), customerVO.getModel().getFirstName());
		assertEquals(customer.getEmailAddress(), customerVO.getModel().getEmailAddress());
		assertEquals(customer.getPhoneNumber(), customerVO.getModel().getPhoneNumber());

		// assertEquals(1,facade.getByName("BusinessTestCaseUniqueName").size());
	}

	@Test
	/*@Ignore*/
	public void testAddProduct() throws BusinessException {
		int customerID = 5;
		int productID = 7;
		Customer customer = new Customer();
		customer.setCustomerId(customerID);
		Product product = new Product();
		product.setProductId(productID);
		CartItem newItem = new CartItem(product, customer, 3, DeliveryType.AIR.toString());
		ResultVO<CartItem> cartItem = facade.addProduct(newItem);
		assertNotNull(cartItem);
		assertEquals(BusinessErrorTypes.SUCCESS, cartItem.getMessage());
		ResultListVO<CartItem> cartItemsInCart = facade.getCart(customerID);
		assertNotNull(cartItemsInCart);
		assertNotNull(cartItemsInCart.getModelList());
		assertTrue(cartItemsInCart.getModelList().contains(newItem));

	}

	@Test
	/*@Ignore*/
	public void testGetByName() throws BusinessException {
		ResultListVO<Customer> customerVOs = facade.getByName("James");
		assertNotNull(customerVOs);
		assertNotNull(customerVOs.getModelList());
		assertEquals(BusinessErrorTypes.SUCCESS, customerVOs.getMessage());
		assertEquals(1, customerVOs.getModelList().get(0).getCustomerId());
	}


	/*	custid	prodid	qty	deliverytype
	
	 * 		2	1	4	GROUND	
			2	2	3	AIR
	 */
	@Test
	/*@Ignore*/
	public void testGetCart() throws BusinessException {

		ResultListVO<CartItem> cartItemsInCart = facade.getCart(2);
		assertNotNull(cartItemsInCart);
		assertNotNull(cartItemsInCart.getModelList());
		assertEquals(2,cartItemsInCart.getModelList().size());
			List<CartItem> cartItems = cartItemsInCart.getModelList();
		assertNotNull(cartItems);
		CartItem cartItem1 = cartItems.get(0);//first item

		assertEquals(2, cartItem1.getCustomer().getCustomerId());
		assertEquals(1, cartItem1.getProduct().getProductId());
		assertEquals(4, cartItem1.getQuantity());
		assertEquals(DeliveryType.GROUND.toString(), cartItem1.getDeliveryType());

		cartItem1 = cartItems.get(1);// second item
		assertEquals(2, cartItem1.getCustomer().getCustomerId());
		assertEquals(2, cartItem1.getProduct().getProductId());
		assertEquals(3, cartItem1.getQuantity());
		assertEquals(DeliveryType.AIR.toString(), cartItem1.getDeliveryType());
		
		
	}
	
	@Test
	/*@Ignore*/
	public void testGetPaymentForValidCustomer() throws BusinessException {

		int paymentDtlId = 1;
		int customerId = 1;
		PaymentDetails model = new PaymentDetails(paymentDtlId);
		model.setCustomer(new Customer(customerId));
		ResultVO<PaymentDetails> result = facade.getPayment(model);
		assertNotNull(result);
		assertEquals(BusinessErrorTypes.SUCCESS, result.getMessage());
		assertEquals(customerId, result.getModel().getCustomer().getCustomerId());
		assertEquals(paymentDtlId, result.getModel().getPaymentDetailID());
		assertEquals("0123 4567 8901 2345", result.getModel().getCreditCardNumber());
		assertEquals("Aneesh Sreekumar", result.getModel().getNameOnCard());
	}

	
	@Test
	/*@Ignore*/
	public void testGetPaymentRepresentationValidCustomer() throws BusinessException {

		int paymentDtlId = 1;
		int customerId = 1;
		PaymentDetails model = new PaymentDetails(paymentDtlId);
		model.setCustomer(new Customer(customerId));
		ResultVO<PaymentDetails> result = facade.getPayment(model);
		assertNotNull(result);
		assertEquals(BusinessErrorTypes.SUCCESS, result.getMessage());
		assertEquals(customerId, result.getModel().getCustomer().getCustomerId());
		assertEquals(paymentDtlId, result.getModel().getPaymentDetailID());
		assertEquals("0123 4567 8901 2345", result.getModel().getCreditCardNumber());
		assertEquals("Aneesh Sreekumar", result.getModel().getNameOnCard());
		PaymentRepresentation representation = result.getModel().createRepresentationFromModel();
		System.out.println(representation);
	}

	@Test
	/*@Ignore*/
	public void testGetPaymentForInValidCustomer() throws BusinessException {
		//TODO inserted by ANEESH on [Oct 28, 2015, 10:18:10 PM]: Refactor validateOrder in OrderDAO and create one function for 
		// testing valid payment for customer.
		int paymentDtlId = 1;
		int customerId = 2;
		PaymentDetails model = new PaymentDetails(paymentDtlId);
		model.setCustomer(new Customer(customerId));
		ResultVO<PaymentDetails> result = facade.getPayment(model);
		assertNotNull(result);
		assertEquals(BusinessErrorTypes.SELECTED_PAYMENT_NOT_FOUND, result.getMessage());
		assertNull(result.getModel());
	}
	
	

	@After
	public void tearDown() {
		facade = null;
	}

}
