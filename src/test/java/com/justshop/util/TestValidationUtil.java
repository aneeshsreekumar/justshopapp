package com.justshop.util;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestValidationUtil {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsValidDate() {
		assertTrue(ValidationUtil.isValidDate("12/12/2015"));
		assertTrue(ValidationUtil.isValidDate("12/14/2015"));
		
		assertFalse(ValidationUtil.isValidDate("13/12/2015"));
		assertFalse(ValidationUtil.isValidDate("12-12/2015"));
	}

	@Test
	public void testIsValidEmail() {
		
		assertFalse(ValidationUtil.isValidEmail("a@.g.com"));
		assertFalse(ValidationUtil.isValidEmail("a@.g.co"));
		
		assertTrue(ValidationUtil.isValidEmail("a@g.com"));
	}

	@Test
	public void testIsValidPhone() {
		assertTrue(ValidationUtil.isValidPhone("913-999-9999"));
		
		assertFalse(ValidationUtil.isValidPhone("913/999-9999"));
	}

}
