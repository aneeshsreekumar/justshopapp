package com.justshop.util;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestUtility {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testReplaceParamters() {
		String value = "/customer/{customerID: //d}/cart/{productID: //d}/some";
		String result ="/customer/1/cart/2/some"; 
		value = Utility.replaceParameters(value, "1","2");
		assertEquals(result,value);
		
	}
	
	@Test
	public void testAppendParameters(){
		String value = "/customer/some";
		String result ="/customer/some/1/2"; 
		value = Utility.appendParameters(value, "1","2");
		assertEquals(result,value);
		
	}


}
