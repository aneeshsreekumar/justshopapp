package com.justshop.vo;

import com.justshop.business.exception.BusinessErrorTypes;

public class ResultVO<T> {
	
	private BusinessErrorTypes message;
	private boolean successFlag;
	private T model;
	
	public BusinessErrorTypes getMessage() {
		return message;
	}
	public void setMessage(BusinessErrorTypes message) {
		this.message = message;
	}
	public boolean isSuccessFlag() {
		return successFlag;
	}
	public void setSuccessFlag(boolean successFlag) {
		this.successFlag = successFlag;
	}
	public T getModel() {
		return model;
	}
	public void setModel(T model) {
		this.model = model;
	}
	public ResultVO(BusinessErrorTypes message, boolean successFlag, T model) {
		super();
		this.message = message;
		this.successFlag = successFlag;
		this.model = model;
	}
	
	public ResultVO(T model) {
		super();
		
		this.model = model;
	}
	
	public ResultVO() {
		super();
		
		
	}
	/*public void appendMessage(String success) {
		// TODO Auto-generated method stub
		this.message = message + Constants.MESSAGE_SEPARATOR + success;
	}*/
	@Override
	public String toString() {
		return "ResultVO [message=" + message.toString() + ", successFlag=" + successFlag + ", model=" + model + "]";
	}
	
	
	

}
