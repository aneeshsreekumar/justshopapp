package com.justshop.vo;

import java.util.List;

import com.justshop.business.exception.BusinessErrorTypes;

public class ResultListVO<T> {
	
	private BusinessErrorTypes message;
	private boolean successFlag;
	private List<T> modelList;
	
	public List<T> getModelList() {
		return modelList;
	}
	public void setModelList(List<T> modelList) {
		this.modelList = modelList;
	}
	public BusinessErrorTypes getMessage() {
		return message;
	}
	public void setMessage(BusinessErrorTypes message) {
		this.message = message;
	}
	public boolean isSuccessFlag() {
		return successFlag;
	}
	public void setSuccessFlag(boolean successFlag) {
		this.successFlag = successFlag;
	}
	
	
	
	public ResultListVO(BusinessErrorTypes message, boolean successFlag, List<T> modelList) {
		super();
		this.message = message;
		this.successFlag = successFlag;
		this.modelList = modelList;
	}
	
	

}
