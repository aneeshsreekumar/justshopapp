package com.justshop.business;

import com.justshop.business.exception.BusinessException;
import com.justshop.model.Order;
import com.justshop.vo.ResultListVO;
import com.justshop.vo.ResultVO;

public interface OrderBusinessFacade {

	
		public ResultVO<Order> confirmOrder(Integer customerId, Integer paymentDetailId) throws BusinessException;
		public ResultVO<Order> getOrder(Integer orderId) throws BusinessException;
		public ResultListVO<Order> getOrders(Integer customerId) throws BusinessException;
		public ResultVO<Order> cancelOrder(Integer orderId) throws BusinessException;
		
	
}
