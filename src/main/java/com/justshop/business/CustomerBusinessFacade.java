package com.justshop.business;

import com.justshop.business.exception.BusinessException;
import com.justshop.model.CartItem;
import com.justshop.model.Customer;
import com.justshop.model.PaymentDetails;
import com.justshop.model.Product;
import com.justshop.vo.ResultListVO;
import com.justshop.vo.ResultVO;

public interface CustomerBusinessFacade {
	
	
	public ResultVO<Customer> get(Integer customerID) throws BusinessException;
	public ResultListVO<Customer> getByName(String customerName) throws BusinessException;
	public ResultListVO<CartItem> getCart(Integer customerID) throws BusinessException;
	public ResultVO<Customer> create(Customer customer) throws BusinessException;
	
	//products
	//search products
	public ResultListVO<Product> getProductsByName(String productName) throws BusinessException;
	public ResultListVO<Product> getProductsByKeyWords(String keyWords) throws BusinessException;
	//add product to cart
	public ResultVO<CartItem> addProduct(CartItem cartItem) throws BusinessException;
	
	//payment
	public ResultVO<PaymentDetails> createPayment(PaymentDetails payment) throws BusinessException;
	public ResultVO<PaymentDetails> getPayment(PaymentDetails payment) throws BusinessException;
	public ResultVO<CartItem> updateProduct(CartItem model) throws BusinessException;
	public ResultVO<Customer> validate(Customer customer) throws BusinessException;
	public ResultListVO<PaymentDetails> getAllPayments(Integer customerId) throws BusinessException;
	public ResultVO<CartItem> deleteProduct(Integer customerId, Integer productId) throws BusinessException;
	public ResultVO<Product> getProduct(Integer productID) throws BusinessException;
	public ResultVO<CartItem> getCartItem(Integer customerId, Integer cartItemId) throws BusinessException;
	

}
