package com.justshop.business.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import com.justshop.service.representation.ErrorRepresentation;

public class BusinessExceptionMapper implements ExceptionMapper<BusinessException> {

	@Override
	public Response toResponse(BusinessException exception) {
		// TODO Auto-generated method stub
		System.out.println("came here in BusinessExceptionMapper:"+exception.getErrorType());
		BusinessErrorTypes errorType = exception.getErrorType();
		Response.Status status = errorType.getStatusCode();
		ErrorRepresentation error = new ErrorRepresentation(errorType.getErrorMessage(),errorType.getErrorCode());
		return Response.status(status).entity(error).build();
	}

}
