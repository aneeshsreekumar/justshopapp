package com.justshop.business.exception;

public class BusinessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BusinessErrorTypes errorType;

	public BusinessException(BusinessErrorTypes message) {
		// TODO Auto-generated constructor stub
		this.errorType = message;
	}

	public BusinessErrorTypes getErrorType() {
		return errorType;
	}

	public void setErrorType(BusinessErrorTypes errorType) {
		this.errorType = errorType;
	}
	
	

}
