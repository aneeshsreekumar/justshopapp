package com.justshop.business;

import java.util.List;
import java.util.Map;

import com.justshop.business.exception.BusinessException;
import com.justshop.model.OrderItem;
import com.justshop.model.Partner;
import com.justshop.model.Product;
import com.justshop.vo.ResultListVO;
import com.justshop.vo.ResultVO;

public interface PartnerBusinessFacade {
	
	
	public ResultVO<Partner> get(Integer partnerID) throws BusinessException;
	public ResultListVO<Partner> getByName(String partnerName) throws BusinessException;
	
	public ResultVO<Partner> create(Partner partner) throws BusinessException;
	public ResultListVO<OrderItem> getOrderRequests(Integer partnerID) throws BusinessException;
	public ResultVO<Product> addProduct(Product product) throws BusinessException;
	public Map<Integer, ResultVO<Product>> addProducts(List<Product> products) throws BusinessException;
	public ResultVO<Partner> update(Partner model) throws BusinessException;
	public ResultVO<Partner> validate(Partner partner) throws BusinessException;	
	

}
