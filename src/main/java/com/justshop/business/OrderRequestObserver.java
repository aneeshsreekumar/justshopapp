package com.justshop.business;

import java.util.List;

import com.justshop.model.OrderItem;

public interface OrderRequestObserver {
	
	public void orderRequestCreated(List<OrderItem> orderItem);

}
