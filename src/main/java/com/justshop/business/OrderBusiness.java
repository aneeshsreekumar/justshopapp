package com.justshop.business;

import java.util.ArrayList;
import java.util.List;

import com.justshop.business.exception.BusinessErrorTypes;
import com.justshop.business.exception.BusinessException;
import com.justshop.dao.OrderDAO;
import com.justshop.model.Order;
import com.justshop.model.OrderItem;
import com.justshop.vo.ResultListVO;
import com.justshop.vo.ResultVO;

public class OrderBusiness implements OrderBusinessFacade,OrderRequestSubject {
	
	private OrderDAO orderDAO;
	private OrderRequestObserver observer;
	
	public OrderBusiness() {
		super();
		// TODO Auto-generated constructor stub
		
		orderDAO = new OrderDAO();
		
		

	}

	public ResultVO<Order> confirmOrder(Integer customerId, Integer paymentDetailId) throws BusinessException {

		ResultVO<Order> result = null;
		if (customerId != null && paymentDetailId != null) {

			result = orderDAO.insertOrder(customerId, paymentDetailId);
			if (isOrderSuccessful(result)) {
				// notify partners
				if (observer != null) {
					observer.orderRequestCreated(result.getModel().getOrderItems());
				}
			}

		} else {
			// no input
			result = new ResultVO<Order>(BusinessErrorTypes.NO_INPUT_GIVEN, false, null);
		}
		System.out.println("result.getMessage() in orderbusiness:" + result.getMessage());
		return result;
	}
	
	public boolean isOrderSuccessful(ResultVO<Order> result){
		
		return (result != null && result.isSuccessFlag() && result.getModel() != null
				&& result.getModel().getOrderItems() != null);
	}

	public ResultVO<Order> getOrder(Integer orderId) throws BusinessException {
		// TODO Auto-generated method stub
		ResultVO<Order> orderVO = null;
		Order fromDB = null;
		if (orderId != null) {

			fromDB = orderDAO.getDetailsByID(orderId,false);
			if (fromDB!= null) {
				orderVO = new ResultVO<Order>(BusinessErrorTypes.SUCCESS, true, fromDB);
			} else {
				orderVO = new ResultVO<Order>(BusinessErrorTypes.NO_ORDER_FOUND, false, new Order(orderId));
			}
		} else {
			orderVO = new ResultVO<Order>(BusinessErrorTypes.NO_INPUT_GIVEN, false, null);
		}

		return orderVO;
	}
	
	public ResultListVO<Order> getOrders(Integer customerID) throws BusinessException {
		// TODO Auto-generated method stub
		ResultListVO<Order> orderVO = null;
		
		if (customerID != null) {

			List<Order> fromDB = orderDAO.getOrdersForCustomer(customerID);
			if (fromDB!= null && !fromDB.isEmpty()) {
				orderVO = new ResultListVO<Order>(BusinessErrorTypes.SUCCESS, true, fromDB);
			} else {
				orderVO = new ResultListVO<Order>(BusinessErrorTypes.NO_ORDER_FOUND, false, new ArrayList<Order>());
			}
		} else {
			orderVO = new ResultListVO<Order>(BusinessErrorTypes.NO_INPUT_GIVEN, false, null);
		}

		return orderVO;
	}

	public ResultVO<Order> cancelOrder(Integer orderId) throws BusinessException {
		// TODO Auto-generated method stub
		ResultVO<Order> orderVO = null;
		if (orderId != null) {
			Order order = new Order(orderId);
			if (orderDAO.isExists(orderId)) {
				if(orderDAO.cancelOrder(orderId)){
					orderVO = new ResultVO<Order>(BusinessErrorTypes.SUCCESS, true, order);
				}else{
					orderVO = new ResultVO<Order>(BusinessErrorTypes.ORDER_CANCEL_ERROR, false, order);
				}
			} else {
				orderVO = new ResultVO<Order>(BusinessErrorTypes.NO_ORDER_FOUND, false, order);
			}
		} else {
			orderVO = new ResultVO<Order>(BusinessErrorTypes.NO_INPUT_GIVEN, false, null);
		}

		return orderVO;
	}
	
	public void subscribe(OrderRequestObserver observer) {
		// TODO Auto-generated method stub
		this.observer=observer;
	}

	public void notifyObserver(List<OrderItem> orderItem) {
		// TODO Auto-generated method stub
		observer.orderRequestCreated(orderItem);
	}


}
