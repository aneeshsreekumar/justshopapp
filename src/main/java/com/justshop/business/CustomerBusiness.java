package com.justshop.business;

import java.util.List;

import org.hibernate.sql.ordering.antlr.GeneratedOrderByFragmentRendererTokenTypes;

import com.justshop.business.exception.BusinessErrorTypes;
import com.justshop.business.exception.BusinessException;
import com.justshop.dao.CartItemDAO;
import com.justshop.dao.CustomerDAO;
import com.justshop.dao.PaymentDetailsDAO;
import com.justshop.dao.ProductDAO;
import com.justshop.model.CartItem;
import com.justshop.model.Customer;
import com.justshop.model.Order;
import com.justshop.model.PaymentDetails;
import com.justshop.model.Product;
import com.justshop.util.Constants;
import com.justshop.vo.ResultListVO;
import com.justshop.vo.ResultVO;

public class CustomerBusiness implements CustomerBusinessFacade {

	private CustomerDAO customerDao;	
	private ProductDAO productDAO;
	private PaymentDetailsDAO paymentDAO;
	private CartItemDAO cartItemDAO;
	

	public CustomerBusiness() {
		super();
		// TODO Auto-generated constructor stub
		customerDao = new CustomerDAO();
		productDAO = new ProductDAO();
		paymentDAO = new PaymentDetailsDAO();

	}
	
	

	public ResultVO<Customer> get(Integer customerID) throws BusinessException {
		// TODO Auto-generated method stub
		ResultVO<Customer> customerVO = null;
		Customer customer = customerDao.getByID(customerID,false);
		if(customer!=null && customer.getCustomerId()!=null){
			customerVO = new ResultVO<Customer>(BusinessErrorTypes.SUCCESS,true,customer);
		}else{
			customerVO = new ResultVO<Customer>(BusinessErrorTypes.NO_CUSTOMER_EXISTS,false,new Customer(customerID));
		}
		
		return customerVO;
		
	}

	public ResultVO<Customer> create(Customer customer) throws BusinessException {
		// TODO Auto-generated method stub
		//TODO inserted by ANEESH on [Sep 27, 2015, 7:06:59 PM]:  business validations
		ResultVO<Customer> customerVO = null;
		if(customer!=null){
			customerVO = new ResultVO<Customer>(BusinessErrorTypes.NO_INPUT_GIVEN,false,null);
		}
		if(customer.getCustomerId()!=null && customerDao.isExists(customer.getCustomerId())){
			customerVO = new ResultVO<Customer>(BusinessErrorTypes.CUSTOMER_EXISTS,false,null);
			
		}else{
			Customer fromDB = customerDao.insertCustomer(customer);
			if(fromDB.getCustomerId()!=null){
				customerVO = new ResultVO<Customer>(BusinessErrorTypes.SUCCESS,true,fromDB);
			}else{
				customerVO = new ResultVO<Customer>(BusinessErrorTypes.CUSTOMER_CREATION_ERROR,false,null);
			}	
		}		
		return customerVO;
	}

//TODO inserted by ANEESH on [Sep 27, 2015, 9:50:58 PM]: add validations
	public ResultVO<CartItem> addProduct(CartItem cartItem) throws BusinessException {
		ResultVO<CartItem> result = null;
		if(cartItem!= null){
			//TODO inserted by ANEESH on [Sep 27, 2015, 12:06:52 AM]: Add business validations and messages to show
			CartItem createdCartItem = customerDao.addProductToCart(cartItem);
			if(createdCartItem!=null){
				result = new ResultVO<CartItem>(BusinessErrorTypes.SUCCESS, true, createdCartItem);
			}else{
				result= new ResultVO<CartItem>(BusinessErrorTypes.CARTITEM_ADDITION_ERROR, false, null);
			}
			
			
		}else {
			result = new ResultVO<CartItem>(BusinessErrorTypes.NO_INPUT_GIVEN, false, null);
		}
		return result;
	}
	
	public ResultVO<CartItem> updateProduct(CartItem cartItem) throws BusinessException {
		ResultVO<CartItem> result = null;
		if(cartItem!= null){
			//TODO inserted by ANEESH on [Sep 27, 2015, 12:06:52 AM]: Add business validations and messages to show
			CartItem updatedCartItem = customerDao.updateCartItem(cartItem);
			if(updatedCartItem!=null){
				result = new ResultVO<CartItem>(BusinessErrorTypes.SUCCESS, true, updatedCartItem);
			}else{
				result= new ResultVO<CartItem>(BusinessErrorTypes.CARTITEM_UPDATION_ERROR, false, null);
			}
			
			
		}else {
			result = new ResultVO<CartItem>(BusinessErrorTypes.NO_INPUT_GIVEN, false, null);
		}
		return result;
	}

	public ResultListVO<Customer> getByName(String customerName) throws BusinessException {
		// TODO Auto-generated method stub
		
		ResultListVO<Customer> listOfCustomers = null;
		if(customerName != null && !Constants.EMPTY_STRING.equals(customerName)){
			List<Customer> customerList = customerDao.searchCustomersByName(customerName);
			if(customerList!=null && customerList.size()>0){
				listOfCustomers = new ResultListVO<Customer>(BusinessErrorTypes.SUCCESS, true, customerList);
			}else{
				listOfCustomers = new ResultListVO<Customer>(BusinessErrorTypes.NO_PRODUCTS_FOUND, false, null);
			}
		}else {
			listOfCustomers = new ResultListVO<Customer>(BusinessErrorTypes.NO_INPUT_GIVEN, false, null);
		}
		
		return listOfCustomers;
	}

	public ResultListVO<CartItem> getCart(Integer customerID) throws BusinessException {
		// TODO Auto-generated method stub
		ResultListVO<CartItem> listOfCartItems = null;
		if(customerID != null){
			List<CartItem> cartItems = customerDao.fetchCart(new Customer(customerID));
			if(cartItems!=null && cartItems.size()>0){
				listOfCartItems = new ResultListVO<CartItem>(BusinessErrorTypes.SUCCESS, true, cartItems);
			}else{
				listOfCartItems = new ResultListVO<CartItem>(BusinessErrorTypes.CART_FETCH_ERROR, false, null);
			}
		}else {
			listOfCartItems = new ResultListVO<CartItem>(BusinessErrorTypes.NO_INPUT_GIVEN, false, null);
		}
		
		return listOfCartItems;
	}



	public ResultListVO<Product> getProductsByName(String productName) throws BusinessException {
		// TODO Auto-generated method stub
		ResultListVO<Product> listOfProducts = null;
		if (productName != null && !Constants.EMPTY_STRING.equals(productName)) {
			List<Product> productList = productDAO.searchProductsByName(productName);
			if (productList != null && productList.size() > 0) {
				listOfProducts = new ResultListVO<Product>(BusinessErrorTypes.SUCCESS, true, productList);
			} else {
				listOfProducts = new ResultListVO<Product>(BusinessErrorTypes.NO_PRODUCTS_FOUND, false, null);
			}

		} else {
			listOfProducts = new ResultListVO<Product>(BusinessErrorTypes.NO_INPUT_GIVEN, false, null);
		}

		return listOfProducts;
	}



	public ResultListVO<Product> getProductsByKeyWords(String keyWords) throws BusinessException {
		// TODO Auto-generated method stub
		ResultListVO<Product> listOfProducts = null;
		if (keyWords != null && !Constants.EMPTY_STRING.equals(keyWords)) {
			List<Product> productList = productDAO.searchProducts(keyWords);
			if (productList != null && productList.size() > 0) {
				listOfProducts = new ResultListVO<Product>(BusinessErrorTypes.SUCCESS, true, productList);
			} else {
				listOfProducts = new ResultListVO<Product>(BusinessErrorTypes.NO_PRODUCTS_FOUND, false, null);
			}

		} else {
			listOfProducts = new ResultListVO<Product>(BusinessErrorTypes.NO_INPUT_GIVEN, false, null);
		}

		return listOfProducts;
	}



	public ResultVO<PaymentDetails> createPayment(PaymentDetails payment) throws BusinessException {
		// TODO Auto-generated method stub
		ResultVO<PaymentDetails> paymentVO = null;
		System.out.println("inside createPayment:" + payment);
		if (payment != null) {
			
			if (payment.getPaymentDetailID()!=null && paymentDAO.isExists(payment.getPaymentDetailID())) {
				System.out.println("inside paymet exists");
				paymentVO = new ResultVO<PaymentDetails>(BusinessErrorTypes.PAYMENT_EXISTS, false, null);
				return paymentVO;
			}else if(payment.getCustomer().getCustomerId()!=null){
				System.out.println("inside insert payment");
				PaymentDetails fromDB = paymentDAO.insertPaymentDetails(payment);
				System.out.println("inside inser fromBD:"+ fromDB);
				if (fromDB != null && fromDB.getPaymentDetailID() != null) {
					paymentVO = new ResultVO<PaymentDetails>(BusinessErrorTypes.SUCCESS, true, fromDB);
				} else {
					paymentVO = new ResultVO<PaymentDetails>(BusinessErrorTypes.PAYMENT_CREATION_ERROR, false, null);
				}
			}
		} else {
			paymentVO = new ResultVO<PaymentDetails>(BusinessErrorTypes.NO_INPUT_GIVEN, false, null);
			return paymentVO;
		}
		return paymentVO;
	}



	public ResultVO<PaymentDetails> getPayment(PaymentDetails payment) throws BusinessException {
		// TODO Auto-generated method stub
		ResultVO<PaymentDetails> paymentVO = null;
		if (payment != null && payment.getPaymentDetailID()!=null) {
			paymentVO = customerDao.getCustomerPayment(payment.getCustomer().getCustomerId(), payment.getPaymentDetailID(), customerDao.getEntityManager(), true);
			
		} else {
			paymentVO = new ResultVO<PaymentDetails>(BusinessErrorTypes.NO_INPUT_GIVEN, false, null);
		}

		return paymentVO;
	}



	@Override
	public ResultVO<Customer> validate(Customer request) throws BusinessException {
		// TODO Auto-generated method stub
		ResultVO<Customer> customerVO = null;
		Customer customer = customerDao.validate(request.getEmailAddress(),request.getPassword());
		if(customer!=null && customer.getCustomerId()!=null){
			customerVO = new ResultVO<Customer>(BusinessErrorTypes.SUCCESS,true,customer);
		}
		return customerVO;
	}



	@Override
	public ResultListVO<PaymentDetails> getAllPayments(Integer customerId) throws BusinessException {
		// TODO Auto-generated method stub
		ResultListVO<PaymentDetails> listOfPaymentDetailss = null;
		if(customerId != null){
			listOfPaymentDetailss = customerDao.getCustomerPayments(customerId);
		}		
		return listOfPaymentDetailss;
	}
	
	
	public ResultVO<CartItem> deleteProduct(Integer customerId, Integer productId) throws BusinessException {
		// TODO Auto-generated method stub
		ResultVO<CartItem> cartItem = null;
		if (customerId != null && productId !=null) {
			CartItem item = new CartItem(new Product(productId),new Customer(customerId));
			if (customerDao.isExists(customerId)) {
				if(customerDao.removeProductFromCart(item)){
					cartItem = new ResultVO<CartItem>(BusinessErrorTypes.SUCCESS, true, item);
				}else{
					cartItem = new ResultVO<CartItem>(BusinessErrorTypes.CARTITEM_DELETION_ERROR, false, item);
				}
			} else {
				cartItem = new ResultVO<CartItem>(BusinessErrorTypes.NO_CUSTOMER_EXISTS, false, item);
			}
		} else {
			cartItem = new ResultVO<CartItem>(BusinessErrorTypes.NO_INPUT_GIVEN, false, null);
		}

		return cartItem;
	}



	@Override
	public ResultVO<Product> getProduct(Integer productID) throws BusinessException {
		// TODO Auto-generated method stub
		ResultVO<Product> productVO = null;
		Product product = productDAO.getByID(productID,false);
		if(product!=null && product.getProductId()!=null){
			productVO = new ResultVO<Product>(BusinessErrorTypes.SUCCESS,true,product);
		}else{
			productVO = new ResultVO<Product>(BusinessErrorTypes.NO_PRODUCTS_FOUND,false,new Product(productID));
		}
		
		return productVO;
	}



	@Override
	public ResultVO<CartItem> getCartItem(Integer customerId, Integer cartItemId) throws BusinessException {
		// TODO Auto-generated method stub
		ResultVO<CartItem> result = null;
		if(customerId != null && cartItemId!=null){
			CartItem cartItem = customerDao.getCartItem(customerId,cartItemId);
			if(cartItem!=null){
				result = new ResultVO<CartItem>(BusinessErrorTypes.SUCCESS, true, cartItem);
			}else{
				result = new ResultVO<CartItem>(BusinessErrorTypes.CART_FETCH_ERROR, false, null);
			}
		}else {
			result = new ResultVO<CartItem>(BusinessErrorTypes.NO_INPUT_GIVEN, false, null);
		}
		
		return result;
	}

	
}
