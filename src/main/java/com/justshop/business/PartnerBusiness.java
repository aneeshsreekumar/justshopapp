package com.justshop.business;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.justshop.business.exception.BusinessErrorTypes;
import com.justshop.business.exception.BusinessException;
import com.justshop.dao.PartnerDAO;
import com.justshop.dao.ProductDAO;
import com.justshop.model.Customer;
import com.justshop.model.OrderItem;
import com.justshop.model.Partner;
import com.justshop.model.Product;
import com.justshop.service.representation.ProductRequest;
import com.justshop.util.Constants;
import com.justshop.util.ValidationUtil;
import com.justshop.vo.ResultListVO;
import com.justshop.vo.ResultVO;

public class PartnerBusiness implements PartnerBusinessFacade, OrderRequestObserver {
	// private CustomerDAO customerDao;
	private ProductDAO productDAO;
	private PartnerDAO partnerDAO;

	public PartnerBusiness() {
		super();
		// TODO Auto-generated constructor stub
		// customerDao = new CustomerDAO();
		productDAO = new ProductDAO();
		partnerDAO = new PartnerDAO();

	}
	@Override
	public ResultListVO<OrderItem> getOrderRequests(Integer partnerID) throws BusinessException {
		// TODO Auto-generated method stub
		ResultListVO<OrderItem> listOfOrderItems = null;
		if (partnerID != null && !Constants.EMPTY_STRING.equals(partnerID)) {
			Partner fetched = partnerDAO.getByID(partnerID, false);
			if (fetched == null) {
				listOfOrderItems = new ResultListVO<OrderItem>(BusinessErrorTypes.NO_PARTNER_FOUND, false, null);
			} else {
				List<OrderItem> orderItemList = partnerDAO.getOrderRequests(fetched);
				if (orderItemList != null && orderItemList.size() > 0) {
					listOfOrderItems = new ResultListVO<OrderItem>(BusinessErrorTypes.SUCCESS, true, orderItemList);
				} else {
					listOfOrderItems = new ResultListVO<OrderItem>(BusinessErrorTypes.NO_ORDERS_FOUND, false, null);
				}
			}
		} else {
			listOfOrderItems = new ResultListVO<OrderItem>(BusinessErrorTypes.NO_INPUT_GIVEN, false, null);
		}

		return listOfOrderItems;
	}
	@Override
	public ResultVO<Product> addProduct(Product product) throws BusinessException {
		// TODO Auto-generated method stub
		ResultVO<Product> productVO = null;
		System.out.println("entering add product");
		BusinessErrorTypes validationMessage = isValidProduct(product);
		if (BusinessErrorTypes.SUCCESS.equals(validationMessage)) {

			product.getPartner().addProduct(product);// associating product with
														// partner
			Product fromDB = productDAO.insertProduct(product);

			if (fromDB.getProductId() != null) {
				productVO = new ResultVO<Product>(BusinessErrorTypes.SUCCESS, true, fromDB);
			} else {
				productVO = new ResultVO<Product>(BusinessErrorTypes.PRODUCT_CREATION_ERROR, false, null);
			}
		} else {
			productVO = new ResultVO<Product>(validationMessage, false, null);
		}
		System.out.println("exiting add product" + productVO);
		return productVO;
	}
	
	public BusinessErrorTypes isValidProduct(Product product) throws BusinessException {

		BusinessErrorTypes validProduct = BusinessErrorTypes.ERROR;
		if (product != null) {
			if (product.getProductId() != null && productDAO.isExists(product.getProductId())) {
				System.out.println("existing product");
				validProduct = BusinessErrorTypes.PRODUCT_EXISTS;
			} else {// product does not exist hence can create
				if (product.getPartner() != null && product.getPartner().getPartnerId() != null) {
					System.out.println("partner id not null");
					Partner partner = partnerDAO.getByID(product.getPartner().getPartnerId(), false);
					if (partner != null) {
						if(!ValidationUtil.isValid(product.getName())){
							validProduct = BusinessErrorTypes.BATCH_PRODUCT_NO_NAME;
						}else if(!ValidationUtil.isValid(product.getDescription())){
							validProduct = BusinessErrorTypes.BATCH_PRODUCT_NO_DESCRIPTION;
						}else if(!ValidationUtil.isValid(product.getKeyWords())){
							validProduct = BusinessErrorTypes.BATCH_PRODUCT_NO_KEYWORDS;
						}else if(!ValidationUtil.isValid(product.getStatus())){
							validProduct = BusinessErrorTypes.BATCH_PRODUCT_NO_STATUS;
						}else{
							System.out.println("is valid product:" + product);
							validProduct = BusinessErrorTypes.SUCCESS;	
						}
						
						
					} else {
						System.out.println("partner not there in db");
						validProduct = BusinessErrorTypes.NO_PARTNER_FOUND;
					}
				} else {// partner id not given
					System.out.println("partner id not given");
					validProduct = BusinessErrorTypes.NO_PARTNER_ID_GIVEN;
				}
			}
		} else {
			System.out.println("partner there in db");
			validProduct = BusinessErrorTypes.NO_PRODUCTS_FOUND;
		}
		return validProduct;
	}
	@Override
	public Map<Integer,ResultVO<Product>> addProducts(List<Product> inputProducts) throws BusinessException {
		// TODO Auto-generated method stub
		Map<Integer,ResultVO<Product>> productVOList = null;
		ResultVO<Product> productVO = null;
		Map<Integer,ResultVO<Product>> validProducts = null;
		System.out.println("entering add products");
		if (inputProducts != null && inputProducts.size() > 0) {
			productVOList = new HashMap<Integer,ResultVO<Product>>(inputProducts.size());// for resizing optimization
			validProducts = new HashMap<Integer,ResultVO<Product>>(inputProducts.size());// for resizing optimization
			BusinessErrorTypes validationMessage = BusinessErrorTypes.ERROR;
			int index = 1;
			for (Product eachProduct : inputProducts) {
				validationMessage = isValidProduct(eachProduct);
				if (BusinessErrorTypes.SUCCESS.equals(validationMessage)) {
					
					validProducts.put(index,new ResultVO<Product>(eachProduct));
					
				} else {// invalid product
					productVO = new ResultVO<Product>(validationMessage, false, eachProduct);// providing each product back so as to know what each one failed on
					productVOList.put(index,productVO);
				}
				index++;
			}
			if(validProducts != null && validProducts.size()>0){
				boolean allSuccess= productDAO.insertProductBatch(validProducts.values());// inserting as batch to DB all the records in map
				
					for(ResultVO<Product> eachInserted: validProducts.values()){
						if (allSuccess) {
						eachInserted.setMessage(BusinessErrorTypes.SUCCESS);
						eachInserted.setSuccessFlag(true);
						}else {
							eachInserted.setMessage(BusinessErrorTypes.PRODUCT_CREATION_ERROR);
							eachInserted.setSuccessFlag(false);
						}	
					}
					
				} 
			productVOList.putAll(validProducts);
			}

		System.out.println("all products after batch insert"+productVOList);
		return productVOList;

	}
	@Override
	public ResultVO<Partner> create(Partner partner) throws BusinessException {
		// TODO Auto-generated method stub
		ResultVO<Partner> partnerVO = null;
		if (partner != null) {

			if (partnerDAO.isExists(partner.getPartnerId())) {
				partnerVO = new ResultVO<Partner>(BusinessErrorTypes.PARTNER_EXISTS, false, null);
				return partnerVO;
			} else {
				Partner fromDB = partnerDAO.insertPartner(partner);
				if (fromDB.getPartnerId() != null) {
					partnerVO = new ResultVO<Partner>(BusinessErrorTypes.SUCCESS, true, fromDB);
				} else {
					partnerVO = new ResultVO<Partner>(BusinessErrorTypes.PARTNER_CREATION_ERROR, false, null);
				}
			}
		} else {
			partnerVO = new ResultVO<Partner>(BusinessErrorTypes.NO_INPUT_GIVEN, false, null);
			return partnerVO;
		}
		return partnerVO;
	}

	@Override
	public ResultVO<Partner> get(Integer partnerID) throws BusinessException {
		// TODO Auto-generated method stub
		ResultVO<Partner> partnerVO = null;
		Partner fromDB = partnerDAO.getByID(partnerID, false);
		if (fromDB != null && fromDB.getPartnerId() != null) {
			partnerVO = new ResultVO<Partner>(BusinessErrorTypes.SUCCESS, true, fromDB);
		} else {
			partnerVO = new ResultVO<Partner>(BusinessErrorTypes.NO_PARTNER_FOUND, false, new Partner(partnerID));
		}

		return partnerVO;
	}

	@Override
	public ResultListVO<Partner> getByName(String partnerName) throws BusinessException {
		// TODO Auto-generated method stub
		ResultListVO<Partner> listOfPartners = null;
		if (partnerName != null && !Constants.EMPTY_STRING.equals(partnerName)) {
			List<Partner> partnerList = partnerDAO.getPartnersByName(partnerName);
			if (partnerList != null && partnerList.size() > 0) {
				listOfPartners = new ResultListVO<Partner>(BusinessErrorTypes.SUCCESS, true, partnerList);
			} else {
				listOfPartners = new ResultListVO<Partner>(BusinessErrorTypes.NO_PARTNER_FOUND, false, null);
			}

		} else {
			listOfPartners = new ResultListVO<Partner>(BusinessErrorTypes.NO_INPUT_GIVEN, false, null);
		}

		return listOfPartners;
	}

	public void orderRequestCreated(List<OrderItem> orderItem) {
		// TODO Auto-generated method stub

		String url = Constants.EMPTY_STRING;
		for (OrderItem item : orderItem) {
			url = item.getPartner().getPartnerServiceURL();
			invokePartnerService(url, item);

		}

	}

	public void invokePartnerService(String url, OrderItem item) {
		// TODO Auto-generated method stub
		// To implement during next phase when implementing web services
	}

	@Override
	public ResultVO<Partner> update(Partner model) throws BusinessException {
		// TODO Auto-generated method stub
		ResultVO<Partner> result = null;
		if(model!= null){	
			System.out.println("inside update:"+ model);
			if(partnerDAO.isExists(model.getPartnerId())){
				Partner updatedPartner = partnerDAO.updatePartner(model);
				result = new ResultVO<Partner>(BusinessErrorTypes.SUCCESS, true, updatedPartner);
			}else{
				result= new ResultVO<Partner>(BusinessErrorTypes.NO_PARTNER_FOUND, false, null);
			}
			
			
		}else {
			result = new ResultVO<Partner>(BusinessErrorTypes.NO_INPUT_GIVEN, false, null);
		}
		return result;
	}
	
	@Override
	public ResultVO<Partner> validate(Partner partner) throws BusinessException {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				ResultVO<Partner> customerVO = null;
				Partner customer = partnerDAO.validate(partner.getEmail(),partner.getPassword());
				if(customer!=null && customer.getPartnerId()!=null){
					customerVO = new ResultVO<Partner>(BusinessErrorTypes.SUCCESS,true,customer);
				}
				return customerVO;
	}
	
	

}
