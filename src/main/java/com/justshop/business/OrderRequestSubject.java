package com.justshop.business;

import java.util.List;

import com.justshop.model.OrderItem;

public interface OrderRequestSubject {
	
	public void subscribe(OrderRequestObserver observer);
	public void notifyObserver(List<OrderItem> orderItems);

}
