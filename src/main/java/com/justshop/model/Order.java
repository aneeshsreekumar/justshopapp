package com.justshop.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.FetchMode;

import com.justshop.service.representation.OrderRepresentation;


@NamedQueries({
	@NamedQuery(
	name = "Order.findOrdersForCustomer",
	query = "select o from Order o where o.customer.customerId = :customerIDValue"
	)
})
@Entity
@Table(name="order_t")
public class Order {
	
	@Id
	@Column(name="order_id")
	@SequenceGenerator(name="orderSequence", sequenceName ="order_seq", initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="orderSequence")
	private Integer orderID;
	
	private Boolean isCancelled;
	
	@Column(name="totalpaid")
	private Double totalPaid;
	
	@ManyToOne
	@JoinColumn(name="customer_id",nullable=false)
	private Customer customer;
	
	@ManyToOne
	@JoinColumn(name="payment_detail_id",nullable=false)
	private PaymentDetails paymentDetail;
	
	@OneToMany(mappedBy="order",cascade={CascadeType.PERSIST,CascadeType.MERGE})
	@OrderBy("orderItemID ASC")
	private List<OrderItem> orderItems = new ArrayList<OrderItem>();
	
	
	
	
	
	public Integer getOrderID() {
		return orderID;
	}

	public void setOrderID(Integer orderID) {
		this.orderID = orderID;
	}

	
	
	
	public List<OrderItem> getOrderItems() {
		return orderItems;
	}

	public void setOrderItems(List<OrderItem> orderItems) {
		this.orderItems = orderItems;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((orderID == null) ? 0 : orderID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (orderID == null) {
			if (other.orderID != null)
				return false;
		} else if (!orderID.equals(other.orderID))
			return false;
		return true;
	}

	/*public Order(Long creditCardNumber, Date expiryDate, String nameOnCard, Customer customer) {
		super();
		this.creditCardNumber = creditCardNumber;
		this.expiryDate = expiryDate;
		this.nameOnCard = nameOnCard;
		this.customer = customer;
	}*/

	public Order(Integer paymentDetailID) {
		super();
		this.orderID = paymentDetailID;
	}
	
	

	public Double getTotalPaid() {
		return totalPaid;
	}

	public void setTotalPaid(Double totalPaid) {
		this.totalPaid = totalPaid;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public PaymentDetails getPaymentDetail() {
		return paymentDetail;
	}

	public void setPaymentDetail(PaymentDetails paymentDetail) {
		this.paymentDetail = paymentDetail;
	}

	public Order(Double totalPaid, Customer customer, PaymentDetails paymentDetail) {
		super();
		this.totalPaid = totalPaid;
		this.customer = customer;
		this.paymentDetail = paymentDetail;
	}

	public Order() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void addOrderItem(OrderItem orderItem) {
		// TODO Auto-generated method stub
		orderItems.add(orderItem);
	}

	@Override
	public String toString() {
		return "Order [orderID=" + orderID + ", totalPaid=" + totalPaid + ", customer=" + customer + ", paymentDetail="
				+ paymentDetail + ", orderItems=" + orderItems + "]";
	}

	public Boolean getIsCancelled() {
		return isCancelled;
	}

	public void setIsCancelled(Boolean isCancelled) {
		this.isCancelled = isCancelled;
	}
	
	public OrderRepresentation createRepresentationFromModel() {
		// TODO Auto-generated method stub
		OrderRepresentation representation = new OrderRepresentation();
		representation.setCustomerID(this.getCustomer().getCustomerId());
		representation.setIsCancelled(this.getIsCancelled());
		representation.setOrderID(this.getOrderID());
		representation.setPaymentDetailID(this.getPaymentDetail().getPaymentDetailID());
		representation.setTotalPaid(this.getTotalPaid());
		representation.createLinks();
		return representation;
	}
	
	
	

}
