package com.justshop.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.justshop.service.representation.CustomerMiniRepresentation;
import com.justshop.service.representation.CustomerRepresentation;
import com.justshop.service.representation.PartnerMiniRepresentation;
import com.justshop.service.representation.PartnerRepresentation;


@NamedQueries({
	@NamedQuery(
			name = "Partner.findPartnersByName",
			query = "select p from Partner p where lower(p.name) like :nameValue"
			)
})
@Entity
@Table(name="partner")
public class Partner {

	@Id
	@Column(name="partner_id")
	@SequenceGenerator(name="partnerSequence", sequenceName ="partner_seq", initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="partnerSequence")
	private Integer partnerId;
	private String name;
	private String address;
	private String city;
	private String country;
	private String zip;
	private String state;
	private String email;
	private String password;
	
	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	@Column(name="partner_service_url")
	private String partnerServiceURL;	
	@Column(name="partner_type")
	private Integer partnerType;
	
	@OneToMany(mappedBy="partner",cascade={CascadeType.PERSIST,CascadeType.REMOVE})
	private Set<Product> products = new HashSet<Product>();
	
	@OneToMany(mappedBy="partner")
	private List<OrderItem> orderItems = new ArrayList<OrderItem>();
	
	public Integer getPartnerId() {
		return partnerId;
	}


	public void setPartnerId(Integer partnerId) {
		this.partnerId = partnerId;
	}

	public String getPartnerServiceURL() {
		return partnerServiceURL;
	}


	public void setPartnerServiceURL(String partnerServiceURL) {
		this.partnerServiceURL = partnerServiceURL;
	}
	
	public List<OrderItem> getOrderItems() {
		return orderItems;
	}


	public void setOrderItems(List<OrderItem> orderItems) {
		this.orderItems = orderItems;
	}


	public void addProduct(Product prod){
		
		products.add(prod);
		prod.setPartner(this);
		
	}
	
	public Partner(Integer id) {
		super();
		this.partnerId = id;
	}
	
	public Partner(){
		
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public String getZip() {
		return zip;
	}


	public void setZip(String zip) {
		this.zip = zip;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public Integer getPartnerType() {
		return partnerType;
	}


	public void setPartnerType(Integer partnerType) {
		this.partnerType = partnerType;
	}


	public Set<Product> getProducts() {
		return products;
	}


	public void setProducts(Set<Product> products) {
		this.products = products;
	}


	@Override
	public String toString() {
		return "Partner [partnerId=" + partnerId + ", name=" + name + ", address=" + address + ", city=" + city
				+ ", country=" + country + ", zip=" + zip + ", state=" + state + ", partnerType=" + partnerType + "]";
	}


	public Partner(String name, String address, String city, String country, String zip,
			String state, Integer partnerType) {
		super();
		this.name = name;
		this.address = address;
		this.city = city;
		this.country = country;
		this.zip = zip;
		this.state = state;
		this.partnerType = partnerType;
	}


	public Partner(String email, String password) {
		super();
		this.email = email;
		this.password = password;
	}
	
	public PartnerMiniRepresentation createMiniPartnerRepresentation(){
		PartnerMiniRepresentation customer = new PartnerMiniRepresentation(this.getPartnerId(),this.getName(),this.getEmail());
		customer.createLinks();		
		return customer;
	}
	
	public PartnerRepresentation createRepresentation(){
		
		PartnerRepresentation representation = new PartnerRepresentation();
		representation.setName(this.getName());
		representation.setAddress(this.getAddress());
		representation.setPartnerId(this.getPartnerId());
		representation.setCountry(this.getCountry());
		representation.setCity(this.getCity());
		representation.setState(this.getState());
		representation.setZip(this.getZip());
		representation.setEmail(this.getEmail());
		representation.setPassword(this.getPassword());
		representation.createLinks();
		return representation;
		
	}
	
	
	

	
}
