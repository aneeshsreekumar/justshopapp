package com.justshop.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.justshop.service.representation.CartItemRepresentation;
import com.justshop.service.representation.CustomerMiniRepresentation;



@Entity
@Table(name="cart_item")
public class CartItem {
	
	
	
	@Override
	public int hashCode() {
		return cartItemId.hashCode();
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CartItem other = (CartItem) obj;
		if (cartItemId == null) {
			if (other.cartItemId != null)
				return false;
		} else if (!cartItemId.equals(other.cartItemId))
			return false;
		return true;
	}

	@EmbeddedId
	private CartItemPK cartItemId = new CartItemPK();
	
	
	@ManyToOne
	@JoinColumn(name="product_id",nullable=false,updatable=false,insertable=false)
	private Product product;
	
	
	@ManyToOne
	@JoinColumn(name="customer_id",nullable=false,updatable=false,insertable=false)
	private Customer customer;
	
	
	@Column(name="qty")
	private Integer quantity;
	
	@Column(name="delivery_type")
	private String deliveryType;
	
	@Column(name="item_not_avlbl")
	private Boolean itemNoLongerAvailable;
	
	public CartItemPK getCartItemId() {
		return cartItemId;
	}
	public void setCartItemId(CartItemPK cartItemId) {
		this.cartItemId = cartItemId;
	}
	public Boolean getItemNoLongerAvailable() {
		return itemNoLongerAvailable;
	}
	public void setItemNoLongerAvailable(Boolean itemNoLongerAvailable) {
		this.itemNoLongerAvailable = itemNoLongerAvailable;
	}
	public String getDeliveryType() {
		return deliveryType;
	}
	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
		
	}
	
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
		
	}
	
	public CartItem(Product product, Customer customer, Integer quantity, String deliveryType) {
		super();
		this.cartItemId.setCustomerId(customer.getCustomerId());
		this.cartItemId.setProductId(product.getProductId());
		this.product = product;
		this.customer = customer;
		this.quantity = quantity;
		this.deliveryType=deliveryType;
		
		associateCartItem();
	}
	
	public CartItem(){
		
	}
	public CartItem(Product product2, Customer customer2) {
		// TODO Auto-generated constructor stub
		this.cartItemId.setCustomerId(customer2.getCustomerId());
		this.cartItemId.setProductId(product2.getProductId());
		this.customer=customer2;
		this.product = product2;
	}
	
	public void associateCartItem(){
		customer.addCartItem(this);
		product.addCartItem(this);
	}
	
	public void removeCartItem(){
		customer.removeShoppingCartItem(this);
		product.removeShoppingCartItem(this);
	}
	
	@Override
	public String toString() {
		return "CartItem [cartItemId=" + cartItemId + ", quantity=" + quantity + ", deliveryType=" + deliveryType
				+ ", itemNoLongerAvailable=" + itemNoLongerAvailable + "]";
	}
	public CartItemRepresentation createRepresentationFromModel() {
		// TODO Auto-generated method stub
		CartItemRepresentation productRep = new CartItemRepresentation();
		productRep.setCustomerID(getCartItemId().getCustomerId());
		productRep.setProduct(getProduct().createRepresentationFromModel());
		productRep.setDeliveryType(getDeliveryType());
		productRep.setQuantity(getQuantity());
		productRep.setItemNoLongerAvailable(getItemNoLongerAvailable());
		productRep.createLinks();		
		return productRep;
	}
	
	
	
	
	
	
	

}
