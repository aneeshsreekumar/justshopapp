package com.justshop.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.justshop.service.representation.CustomerMiniRepresentation;
import com.justshop.service.representation.CustomerRepresentation;
import com.justshop.service.representation.LinkRepresentation;
import com.justshop.service.representation.LinkRepresentationTypes;


@NamedQueries({
	@NamedQuery(
	name = "Customer.findCustomersByName",
	query = "select p from Customer p where p.firstName = :customerName"
	)
})
@Entity
@Table(name="customer")
public class Customer {
	@Id
	@Column(name="customer_id")
	/*@TableGenerator(name="TABLE_GEN", table="SEQUENCE_TABLE", pkColumnName="SEQ_NAME",
    valueColumnName="SEQ_COUNT", pkColumnValue="PRODUCT_SEQ", allocationSize=1)*/
	@SequenceGenerator(name="customerSequence", sequenceName ="customer_seq", initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="customerSequence")
	private Integer customerId;
	
	@Column(name="first_name")
	private String firstName;
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="password")
	private String password;
	
	
	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getEmailAddress() {
		return emailAddress;
	}


	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}


	public String getPhoneNumber() {
		return phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Column(name="email_address")
	private String emailAddress;
	
	@Column(name="phone_number")
	private String phoneNumber;
	
	
	
	
	@OneToMany(mappedBy="customer",cascade={CascadeType.PERSIST},fetch=FetchType.EAGER)
	private List<CartItem> shoppingCartItems = new ArrayList<CartItem>();
	
	@OneToMany(mappedBy="customer",cascade={CascadeType.PERSIST})
	private List<PaymentDetails> paymentDetails = new ArrayList<PaymentDetails>();
	
	public Integer getCustomerId() {
		return customerId;
	}


	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public List<CartItem> getShoppingCartItems() {
		return shoppingCartItems;
	}
	
	public Boolean removeShoppingCartItem(CartItem item) {
		return shoppingCartItems.remove(item);
	}


	public void setShoppingCartItems(List<CartItem> shoppingCartItems) {
		this.shoppingCartItems = shoppingCartItems;
	}


	public List<PaymentDetails> getPaymentDetails() {
		return paymentDetails;
	}


	public void setPaymentDetails(List<PaymentDetails> paymentDetails) {
		this.paymentDetails = paymentDetails;
	}


	public void addCartItem(CartItem prod){
		
		shoppingCartItems.add(prod);
		prod.setCustomer(this);
		
	}
	
	public Customer(Integer id) {
		super();
		this.customerId = id;
	}
	
	public Customer(){
		
	}


	public Customer(String firstName, String lastName, String emailAddress, String phoneNumber) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailAddress = emailAddress;
		this.phoneNumber = phoneNumber;
		
	}


	public Customer(String emailAddress,String password) {
		super();
		this.password = password;
		this.emailAddress = emailAddress;
	}
	
	public CustomerMiniRepresentation createMiniCustomerRepresentation(){
		CustomerMiniRepresentation customer = new CustomerMiniRepresentation(this.getCustomerId(),this.getFirstName(),this.getEmailAddress(),getShoppingCartItems().size());
		customer.createLinks();		
		return customer;
	}
	
	public CustomerRepresentation createRepresentation(){
		
		CustomerRepresentation representation = new CustomerRepresentation();
		representation.setFirstName(getFirstName());
		representation.setLastName(getLastName());
		representation.setCustomerID(getCustomerId().toString());
		representation.setEmailAddress(getEmailAddress());
		representation.setPassword(getPassword());
		representation.setPhoneNumber(getPhoneNumber());
		representation.createLinks();
		return representation;
		
	}

}
