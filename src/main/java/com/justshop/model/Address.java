package com.justshop.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="Address")
public class Address {

	@Id
	@Column(name="address_id")
	@TableGenerator(name="TABLE_GEN", table="SEQUENCE_TABLE", pkColumnName="SEQ_NAME",
    valueColumnName="SEQ_COUNT", pkColumnValue="PRODUCT_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_GEN")
	private Integer addressId;
	private String address;
	
/*	@Column(name="customer_id")
	private Integer customerId;*/
	
	@Column(name="address_type")
	private Integer addressType;
	
	@Column(name="address_line1")
	private String addressLine1;
	
	@Column(name="address_line2")
	private String addressLine2;
	
	@Column(name="zip")
	private String zip;
	
	@Column(name="state")
	private String state;
	
	@Column(name="Country")
	private String country;
	
	@Column(name="City")
	private String city;
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	@ManyToOne
	@JoinColumn(name="customer_id",nullable=false)
	private Customer customer;
	
	public Address() {
		super();
		this.addressId = addressId;
	}

	public Address(String address1, String address2, String zip, String state, String country, String city, String addresType) {
		super();
		this.addressLine1 = address1;
		this.addressLine2 = address2;
		this.zip = zip;
		this.state = state;
		this.country = country;
		this.city = city;
		this.addressType = addressType;
	}

}
