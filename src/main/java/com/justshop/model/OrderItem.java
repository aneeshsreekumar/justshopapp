package com.justshop.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.justshop.service.representation.OrderItemRepresentation;



@Entity
@Table(name="order_item")
public class OrderItem {
	
	@Id
	@Column(name="order_item_id")
	@SequenceGenerator(name="orderItemSequence", sequenceName ="order_item_seq", initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="orderItemSequence")
	private Integer orderItemID;
	
	private Boolean isCancelled;
	
	private String deliveryType;
	@Column(name="qty")
	private Integer quantity;
	@Column(name="unit_price")
	private Double unitPrice;
	
	
	@ManyToOne
	@JoinColumn(name="product_id",nullable=false)
	private Product product;
	
	
	@ManyToOne
	@JoinColumn(name="order_id",nullable=false)
	private Order order;
	
	@OneToOne(mappedBy="orderItem",cascade={CascadeType.PERSIST})
	private DeliveryRequest delivery;
	
	@ManyToOne
	@JoinColumn(name="partner_id",nullable=false)
	private Partner partner;
	
	
	public String getDeliveryType() {
		return deliveryType;
	}
	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}
	public Integer getOrderItemID() {
		return orderItemID;
	}
	public void setOrderItemID(Integer orderItemID) {
		this.orderItemID = orderItemID;
	}
	
	
	public Partner getPartner() {
		return partner;
	}
	public void setPartner(Partner partner) {
		this.partner = partner;
	}
	public DeliveryRequest getDelivery() {
		return delivery;
	}
	public void setDelivery(DeliveryRequest delivery) {
		this.delivery = delivery;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	
	
	public OrderItem(){
		
	}
	public OrderItem(Product product2, Order order2, Integer quantity, Double unitPrice, String deliveryType, Partner partner) {
		// TODO Auto-generated constructor stub
		
		this.order=order2;
		System.out.println("order in orderitem:"+order);
		this.product = product2;
		this.deliveryType=deliveryType;
		associateOrderItem();
		this.quantity=quantity;
		this.unitPrice = unitPrice;
		this.partner = partner;
	}
	
	public void associateOrderItem(){
		order.addOrderItem(this);
		product.addOrderItem(this);
		
	}
	
	@Override
	public String toString() {
		return "OrderItem [orderItemID=" + orderItemID + ", isCancelled=" + isCancelled + ", deliveryType="
				+ deliveryType + ", delivery=" + delivery + ", quantity=" + quantity + "]";
	}
	public Boolean getIsCancelled() {
		return isCancelled;
	}
	public void setIsCancelled(Boolean isCancelled) {
		this.isCancelled = isCancelled;
	}
	public Double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}	
	
	public OrderItemRepresentation createRepresentationFromModel(){
		
		OrderItemRepresentation orderItemRep = new OrderItemRepresentation();
		orderItemRep.setDeliveryRequestID(this.getDelivery().getDeliveryId());
		orderItemRep.setDeliveryType(this.getDeliveryType());
		orderItemRep.setIsCancelled(this.getIsCancelled());
		orderItemRep.setOrderItemID(this.getOrderItemID());
		orderItemRep.setQuantity(this.getQuantity());
		orderItemRep.setTotalPrice(this.getUnitPrice()*this.getQuantity());
		
		orderItemRep.setDeliveryRequestID(this.getDelivery().getDeliveryId());
		orderItemRep.setOrderID(this.getOrder().getOrderID());
		orderItemRep.setProductID(this.getProduct().getProductId());
		orderItemRep.setPartnerID(this.getPartner().getPartnerId());
		orderItemRep.createLinks();
		return orderItemRep;
	}

}
