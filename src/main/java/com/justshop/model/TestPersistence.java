package com.justshop.model;

import javax.persistence.EntityManager;

public class TestPersistence {
	
	
	
	  public static void main(String[] args) {
	    Product product = new Product();
	    EntityManager em = null;
	    CartItem cartItem = null;
	    try{
	    	
	    	product = new Product();
	    	Customer customer = new Customer();
	    product.setAvailableQuantity(12);
	        product.setDescription("From loginservlet");
	        Product prod2 = new Product();
	        prod2.setDescription("second one");
	        prod2.setAvailableQuantity(2);
	                
	      cartItem = new CartItem();
	     // cartItem.setCartName("my cart");
	      cartItem.setQuantity(3);
	      cartItem.setProduct(product);
	      customer.addCartItem(cartItem); 
	      
	      
	      cartItem = new CartItem();
	      cartItem.setProduct(prod2);
	     // cartItem.setCartName("my cart");
	      cartItem.setQuantity(4);
	      customer.addCartItem(cartItem); 
	     
	     
	     em = PersistenceManager.INSTANCE.getEntityManager();
	    em.getTransaction()
	        .begin();
	    em.persist(product);
	    em.persist(prod2);
	    em.persist(customer);
	    
	    em.getTransaction()
	        .commit();
	 	        
	    Product prod = em.find(Product.class, 1);
	    System.out.println("Product retrieved:"+prod);
	    }
	    finally{
	    	if(em!=null) {
	    		em.close();;

	    	}
		    	
	    }
	    
	  }


}
