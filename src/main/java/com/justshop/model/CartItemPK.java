package com.justshop.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;



@Embeddable
public class CartItemPK implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5186134279153782969L;

	@Column(name="customer_id")
	private Integer customerId;
	
	@Column(name="product_id")
	private Integer productId;
	
	public CartItemPK(){
		
	}
	
	public CartItemPK(Integer customerID, Integer productID){
		this.customerId = customerID;
		this.productId = productID;
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((customerId == null) ? 0 : customerId.hashCode());
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CartItemPK other = (CartItemPK) obj;
		if (customerId == null) {
			if (other.customerId != null)
				return false;
		} else if (!customerId.equals(other.customerId))
			return false;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		return true;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	@Override
	public String toString() {
		return "CartItemPK [customerId=" + customerId + ", productId=" + productId + "]";
	}
	
	
	
	
	
	
	
}