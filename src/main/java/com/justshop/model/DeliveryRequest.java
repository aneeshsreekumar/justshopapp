package com.justshop.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="delivery_request")
public class DeliveryRequest {

	public String getDeliveryType() {
		return deliveryType;
	}

	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

	public String getTrackingStatus() {
		return trackingStatus;
	}

	public void setTrackingStatus(String trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	@Id
	@Column(name="delivery_id")
	@SequenceGenerator(name="deliverySequence", sequenceName ="delivery_seq", initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="deliverySequence")
	private Integer deliveryId;
	

	@Column(name="delivery_type")
	private String deliveryType;
	@Column(name="tracking_number")
	private String trackingNumber;
	@Column(name="tracking_status")
	private String trackingStatus;
	
	@OneToOne
	@JoinColumn(name="order_item_id",nullable=false)
	private OrderItem orderItem;
	
	
	
	public OrderItem getOrderItem() {
		return orderItem;
	}

	public void setOrderItem(OrderItem orderItem) {
		this.orderItem = orderItem;
	}

	public Integer getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(Integer deliveryId) {
		this.deliveryId = deliveryId;
	}
	
	public DeliveryRequest(Integer id) {
		super();
		this.deliveryId = id;
	}
	
	public DeliveryRequest() {
		// TODO Auto-generated constructor stub
	}

	public DeliveryRequest(String deliveryType, String trackingNumber, String trackingStatus, OrderItem orderItem) {
		super();
		this.deliveryType = deliveryType;
		this.trackingNumber = trackingNumber;
		this.trackingStatus = trackingStatus;
		this.orderItem = orderItem;
		associateToOrderItem();
	}

	public void associateToOrderItem() {
		// TODO Auto-generated method stub
		orderItem.setDelivery(this);
	}

	@Override
	public String toString() {
		return "DeliveryRequest [deliveryId=" + deliveryId + ", deliveryType=" + deliveryType + ", trackingNumber="
				+ trackingNumber + ", trackingStatus=" + trackingStatus + "]";
	}

	
	
}
