package com.justshop.model;

public enum TrackingStatus {
	
	CREATED, INTRANSIT, DELIVERED, ACKNOWLEDGED, CANCELED, RETURNED

}
