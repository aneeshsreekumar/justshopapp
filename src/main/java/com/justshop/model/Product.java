package com.justshop.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.justshop.service.representation.ProductRepresentation;


@NamedQueries({
	@NamedQuery(
	name = "Product.findProductsByKeyword",
	query = "select p from Product p where p.keyWords like :keywordsValue"
	),
	@NamedQuery(
			name = "Product.findProductsByName",
			query = "select p from Product p where LOWER(p.name) like :nameValue"
			)
})
@Entity
/*@Indexed
@AnalyzerDef(name = "searchanalyzer",
tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
filters = {
  @TokenFilterDef(factory = LowerCaseFilterFactory.class),
  @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
    @Parameter(name = "language", value = "English")
  })
})*/
@Table(name="product")
public class Product {

	@Id
	@Column(name="product_id")
	@SequenceGenerator(name="productSequence", sequenceName ="product_seq", initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="productSequence")
	private Integer productId;
	@Column(name="available_quantity")
	private Integer availableQuantity;
	@Column(name="availability_status")
	private String status;	
	@Column(name="unit_price")
	private Double unitPrice;
	
	/*@Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO)
	@Analyzer(definition="searchanalyzer")
	*/
	private String description;
	
	/*@Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO)
	@Analyzer(definition="searchanalyzer")
	*/
	private String name;
	
	
	@Column(name="keywords")
	/*@Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO)
	@Analyzer(definition="searchanalyzer")
	*/
	private String keyWords;
	
	private String image;
	
	
	
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}

	@OneToMany(mappedBy="product")
	private Set<CartItem> shoppingCartItems = new HashSet<CartItem>();
	
	@OneToMany(mappedBy="product")
	private Set<OrderItem> orderItems = new HashSet<OrderItem>();
	
	
	@ManyToOne
	@JoinColumn(name="partner_id",nullable=false)
	private Partner partner;
	
	
	
	public Set<OrderItem> getOrderItems() {
		return orderItems;
	}
	public void setOrderItems(Set<OrderItem> orderItems) {
		this.orderItems = orderItems;
	}
	public Set<CartItem> getShoppingCartItems() {
		return shoppingCartItems;
	}
	public void setShoppingCartItems(Set<CartItem> shoppingCartItems) {
		this.shoppingCartItems = shoppingCartItems;
	}
	public Boolean removeShoppingCartItem(CartItem item) {
		return shoppingCartItems.remove(item);
	}
	
	public void addCartItem(CartItem prod){
		
		shoppingCartItems.add(prod);
		prod.setProduct(this);
		
	}
	
	public Double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getKeyWords() {
		return keyWords;
	}
	public void setKeyWords(String keyWords) {
		this.keyWords = keyWords;
	}
	
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer id) {
		this.productId = id;
	}
	
	public Integer getAvailableQuantity() {
		return availableQuantity;
	}
	public void setAvailableQuantity(Integer availableQuantity) {
		this.availableQuantity = availableQuantity;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void addOrderItem(OrderItem orderItem) {
		// TODO Auto-generated method stub
		orderItems.add(orderItem);
	}
	
	public void setPartner(Partner partner) {
		// TODO Auto-generated method stub
		this.partner = partner;
	}
	
	public Partner getPartner() {
		return partner;
	}
	
	public Product(Integer id) {
		super();
		this.productId = id;
	}
	
	public Product() {
		super();
		this.productId = productId;
	}
	@Override
	public String toString() {
		return "Product [id=" + productId + ", qty=" + availableQuantity + ", description="
				+ description + "]";
	}
	public Product(String name, Integer quantity, String description, String keyWords, Partner partner) {
		super();
		this.name = name;
		this.availableQuantity = quantity;
		this.description = description;
		this.keyWords = keyWords;
		this.partner = partner;
		
	}

	
	//for testing
	public Product(Integer productId, String name, Integer quantity, String description, String keyWords) {
		super();
		this.productId=productId;
		this.name = name;
		this.availableQuantity = quantity;
		this.description = description;
		this.keyWords = keyWords;
		
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		return true;
	}
	
	//utility method
	public ProductRepresentation createRepresentationFromModel() {
		// TODO Auto-generated method stub
		ProductRepresentation representation = new ProductRepresentation();
		representation.setName(this.getName());
		representation.setDescription(this.getDescription());
		representation.setKeywords(this.getKeyWords());
		representation.setStatus(this.getStatus());
		representation.setAvailableQuantity(this.getAvailableQuantity());
		representation.setUnitPrice(this.getUnitPrice());	
		representation.setProductID(this.getProductId());
		representation.setImage(this.getImage());
		representation.createLinks();
		return representation;
	}
	
	

}
