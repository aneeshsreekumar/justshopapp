package com.justshop.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.justshop.service.representation.PaymentRepresentation;
import com.justshop.service.representation.ProductRepresentation;
import com.justshop.util.Constants;

@NamedQueries({
	@NamedQuery(
	name = "PaymentDetail.findPaymentCustomer",
	query = "select p from PaymentDetails p where p.paymentDetailID = :paymentDetailID and p.customer.customerId = :customerID"
	)
})
@Entity
@Table(name="payment_details")
public class PaymentDetails {
	
	@Id
	@Column(name="payment_detail_id")
	@SequenceGenerator(name="paymentSequence", sequenceName ="payment_detail_seq", initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="paymentSequence")
	private Integer paymentDetailID;
	@Column(name="credit_card_num")
	private String creditCardNumber;
	
	private String creditCardType;
	
	public String getCreditCardType() {
		return creditCardType;
	}

	public void setCreditCardType(String creditCardType) {
		this.creditCardType = creditCardType;
	}

	@Column(name="expiry_date")
	private Date expiryDate;
	@Column(name="name_on_card")
	private String nameOnCard;
	
	@ManyToOne
	@JoinColumn(name="customer_id",nullable=false)
	private Customer customer;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((paymentDetailID == null) ? 0 : paymentDetailID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentDetails other = (PaymentDetails) obj;
		if (paymentDetailID == null) {
			if (other.paymentDetailID != null)
				return false;
		} else if (!paymentDetailID.equals(other.paymentDetailID))
			return false;
		return true;
	}

	public PaymentDetails(String creditCardNumber, Date expiryDate, String nameOnCard, Customer customer) {
		super();
		this.creditCardNumber = creditCardNumber;
		this.expiryDate = expiryDate;
		this.nameOnCard = nameOnCard;
		this.customer = customer;
	}

	public PaymentDetails(Integer paymentDetailID) {
		super();
		this.paymentDetailID = paymentDetailID;
	}

	public PaymentDetails() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getPaymentDetailID() {
		return paymentDetailID;
	}

	public void setPaymentDetailID(Integer paymentDetailID) {
		this.paymentDetailID = paymentDetailID;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getNameOnCard() {
		return nameOnCard;
	}

	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public String toString() {
		return "PaymentDetails [paymentDetailID=" + paymentDetailID + ", creditCardNumber=" + creditCardNumber
				+ ", expiryDate=" + expiryDate + ", nameOnCard=" + nameOnCard + "]";
	}

	public PaymentRepresentation createRepresentationFromModel() {
		PaymentRepresentation representation = new PaymentRepresentation();
		representation.setCreditCardType(this.creditCardType);
		representation.setCreditCardNumber(this.creditCardNumber);
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_PATTERN);
		System.out.println("sdf.format(this.getExpiryDate())"+sdf.format(this.getExpiryDate()));
		representation.setExpiryDate(sdf.format(this.getExpiryDate()));
		representation.setNameOnCard(this.getNameOnCard());
		representation.setPaymentDetailID(this.paymentDetailID);
		representation.setCustomerID(customer.getCustomerId());
		representation.createLinks();
		return representation;
	}

	
	
	
	
	
	

}
