package com.justshop.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

public class ValidationUtil {
	
	public static boolean isValid(Integer value){
		
		if(value!=null && value > 0)
			return true;
		else 
			return false;
	}
	
	public static boolean isValid(Double value){
		
		if(value!=null && value > 0)
			return true;
		else 
			return false;
	}
	
	public static boolean isValid(String value){
		
		if(value!=null && !Constants.EMPTY_STRING.equals(value))
			return true;
		else 
			return false;
	}
	
	public static boolean isValidDate(String expiryDate) {
		// TODO Auto-generated method stub
		boolean isValid = false;
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_PATTERN);
		sdf.setLenient(false);
		try {
			sdf.parse(expiryDate);
			isValid = true;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			isValid = false;
		}
		
		return isValid;
	}

	public static boolean isValidEmail(String email) {
		// TODO Auto-generated method stub
		return Constants.EMAIL_PATTERN.matcher(email).matches();
	}

	public static boolean isValidPhone(String phone) {
		// TODO Auto-generated method stub
		return Constants.PHONE_PATTERN.matcher(phone).matches();
	}

	public static boolean isValidZip(String zip) {
		// TODO Auto-generated method stub
		return Constants.ZIP_PATTERN.matcher(zip).matches();
	}

}
