package com.justshop.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utility {
	
	public static String replaceParameters(String source, String... params){
		StringBuffer sbf = new StringBuffer();
		Pattern pattern = Pattern.compile("\\{(.*?)\\}");
		Matcher matcher = pattern.matcher(source);
		int i =  0;
		int start = 0;	
		int lastEnd = 0;
		while(matcher.find()){
		
			/*System.out.println(matcher.start());
			System.out.println(matcher.end());
			System.out.println(matcher.group());
			*/sbf.append(source.substring(start,matcher.start())).append(params[i]);
			i++;
			start = matcher.end();
			/*System.out.println("start:"+start);
			System.out.println("i:"+i);
			System.out.println(sbf.toString());
			*/
			lastEnd = matcher.end();
		}
		if(lastEnd<source.length()){// appending remaining portion
			sbf.append(source.substring(lastEnd, source.length()));
		}
		
		return sbf.toString();
	}

	public static String appendParameters(String uRI, String... parameter) {
		// TODO Auto-generated method stub
		StringBuffer sbf = new StringBuffer(uRI);
		for(String param: parameter){
			sbf.append(Constants.SLASH+param);
		}
		return sbf.toString();
	}

}
