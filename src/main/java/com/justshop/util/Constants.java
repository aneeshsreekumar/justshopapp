package com.justshop.util;

import java.util.regex.Pattern;

public interface Constants {
	
	//Messages
	public String EMPTY_STRING = "";
	public String SLASH = "/";
	public String LEFT_CURL ="{";
	public String RIGHT_CURL ="}";
	public String REG_INTEGER = " : \\d+}";
	
	
	public String PARTNER_NOT_FOUND="Partner not found";
	public int BATCH_FLUSH_SIZE = 10;
	public String SUCCESS = "success";
	public String ERROR = "Internal Error Occured";
	public String OBJECT_EXISTS = "exists";
	public String NOT_FOUND = "Not Found";
	public String NO_INPUT_GIVEN = "No Input Given";
	public String BAD_INPUT_GIVEN = "Invalid Input Given";

	public String NO_PARTNER_FOUND = "No Partner Found"; 
	public String NO_PARTNERS_FOUND = "No Partners Found";
	public String NO_PARTNER_ID_GIVEN = "No Partner ID given";
	public String PARTNER_EXISTS = "Partner already exists";
	public String PARTNER_CREATION_ERROR = "Partner Creation Error";
	public String PARTNER_UPDATION_ERROR = "Partner Updation Error";
	
	public String BATCH_FAILED_ERROR = "Entire Batch Failed Error";
	public String BATCH_PRODUCT_NO_NAME = "Name is required for Product";
	public String BATCH_PRODUCT_NO_DESCRIPTION = "Description is required for Product";
	public String BATCH_PRODUCT_NO_KEYWORDS = "Keywords is required for Product";
	public String BATCH_PRODUCT_NO_STATUS = "Status is required for Product";
	
	public String VALID = "Valid";
	public String PRODUCT_CREATION_ERROR = "Product creation error";
	public String NO_PRODUCTS_FOUND = "No Products Found";
	public String PRODUCT_EXISTS = "Product already exists";
	
	
	public String NO_ORDERS_FOUND = "No Order Items Found";
	public String NO_ORDER_FOUND = "No Order Found";
	public String ORDER_CANCEL_ERROR = "Error during order cancel";
	public String ORDER_CREATION_ERROR = "Order creation error";
	
	public String PAYMENT_EXISTS = "Payment already exists";
	public String PAYMENT_CREATION_ERROR = "Error during payment creation";
	
	public String SOME_ITEMS_NOT_AVAILABLE = "Some items not available.Check Cart";
	public String SELECTED_PAYMENT_NOT_FOUND = "Selected Payment Not Found";
	public String NO_PAYMENTS_ADDED = "No Payments Added For Customer";
	public String NO_CUSTOMER_EXISTS = "No Customer Exists";
	public String NOT_VALID_CUSTOMER = "Customer credentials invalid";
	
	public String CUSTOMER_EXISTS = "Customer Already Exists";
	public String CUSTOMER_CREATION_ERROR = "Error during customer creation";
	public String CARTITEM_UPDATION_ERROR = "Cart Item Updation Error";
	
	public String CART_FETCH_ERROR = "Error during getting shopping cart";
	public String CARTITEM_ADDITION_ERROR = "Error during adding product to cart";
	public String NO_CART_ITEM_FOUND = "No Cart Item Found";
	
	public String MESSAGE_SEPARATOR = "#!#";
	public String DATE_PATTERN = "MM/dd/yyyy";
	public Pattern PHONE_PATTERN = Pattern.compile("[0-9][0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]");
	public Pattern EMAIL_PATTERN = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
	public Pattern ZIP_PATTERN = Pattern.compile("\\d{5}(-\\d{4})?");
	
	//TODO inserted by ANEESH on [Nov 9, 2015, 5:44:03 PM]: Change to read from web.xml or pom
	//public String hostURL = "http://localhost:8080/justshop/services";
	//dev URL
	public String hostURL = "https://justshop-dev.herokuapp.com/services";
	public String POST = "POST";
	public String PUT = "PUT";
	public String DELETE = "DELETE";
	public String GET = "GET";
	public String APPLICATION_JSON="application/json";
	public String APPLICATION_XML="application/xml";
	public String MULTIPART_FORM = "multipart/form-data";
	
	public String SELF = "self";
	
	// urls to services
	public String VALIDATE = "/validate";
	public String SAVE = "/save";
	public String SAVE_FORM = "/saveform";
	public String UPDATE = "/update";
	public String DELETE_PATH = "/delete";
	public String SEARCH = "/search";
	public String ADD = "/add";
	public String GET_URL = "/get";
	
	
	public String BASE_CUSTOMER_URL = "/customer";
	public String CUSTOMER_ID = "customerId";
	public String ORDER_ID = "orderId";
	public String CART_ITEM_ID = "cartItemId";
	public String PRODUCT_ID = "productId";
	public String PARTNER_ID = "partnerId";
	public String SEARCH_NAME = "searchName";
	
	public String CUSTOMER_ID_PARAMETER = SLASH+LEFT_CURL+CUSTOMER_ID+REG_INTEGER;
	public String PRODUCT_ID_PARAMETER = SLASH+LEFT_CURL+PRODUCT_ID+REG_INTEGER;
	public String CART_ID_PARAMETER = SLASH+LEFT_CURL+CART_ITEM_ID+REG_INTEGER;
	public String PARTNER_ID_PARAMETER = SLASH+LEFT_CURL+PARTNER_ID+REG_INTEGER;
	public String ORDER_ID_PARAMETER = SLASH+LEFT_CURL+ORDER_ID+REG_INTEGER;;
	
	
	//cart
	public String BASE_CART_URL = "/cart";
	public String UPDATE_CART_ITEM_PATH = BASE_CUSTOMER_URL+BASE_CART_URL+UPDATE;
	public String DELETE_CART_ITEM_PATH = BASE_CUSTOMER_URL+CUSTOMER_ID_PARAMETER+BASE_CART_URL+DELETE_PATH+PRODUCT_ID_PARAMETER;
	public String GET_CART_URL = Constants.BASE_CUSTOMER_URL+Constants.CUSTOMER_ID_PARAMETER+Constants.BASE_CART_URL+Constants.GET_URL;
	
	
	
	
	public String BASE_PRODUCT_URL = "/product";
	
	public String SEARCH_NAME_PARAMETER = SLASH+LEFT_CURL+SEARCH_NAME+RIGHT_CURL;
	public String PRODUCT_SEARCH_URL = BASE_PRODUCT_URL+SEARCH;
	
	public String GET_PAYMENT_URL = Constants.BASE_CUSTOMER_URL+Constants.CUSTOMER_ID_PARAMETER+"/payment/{paymentId}";
	
	
	public String ADD_CART_ITEM = BASE_CUSTOMER_URL+BASE_CART_URL+ADD;//"/customer/cart/add"
	
	public String CREATE_PAYMENT_URL="/payment/add";
	public String CARTITEM_DELETION_ERROR = "Cart Item Deletion Error";
	
	public String CREATE_ORDER_URL = "/order/create";
	public String GET_PRODUCT_URL = Constants.BASE_PRODUCT_URL+Constants.PRODUCT_ID_PARAMETER;
	public String GET_CUSTOMER_URL = Constants.BASE_CUSTOMER_URL+Constants.CUSTOMER_ID_PARAMETER;
	public String PARTNER_ROLE = "P";
	public String CUSTOMER_ROLE = "C";
	
	public String BASE_PARTNER_URL = "/partners";
	public String GET_PARTNER_URL = BASE_PARTNER_URL+PARTNER_ID_PARAMETER;
	public String ADD_PRODUCT = "/product/add";
	public String ADD_BATCH = "/product/addBatch";
	public String ADD_BATCH_FILE = "/product/addBatchFile";
	public String ADD_PARTNER_PROD_URL = Constants.PARTNER_ID_PARAMETER+Constants.ADD_PRODUCT;
	public String UPDATE_PARTNER_URL = Constants.UPDATE+Constants.PARTNER_ID_PARAMETER;
	public String ADD_PARTNER_BATCH_PROD_URL = Constants.PARTNER_ID_PARAMETER+Constants.ADD_BATCH;
	public String ADD_PARTNER_BATCHFILE_PROD_URL = Constants.PARTNER_ID_PARAMETER+Constants.ADD_BATCH_FILE;
	public String NOT_VALID_PARTNER = "Partner Credentials Invalid";
	public String BASE_ORDER_URL = "/order";
	public String GET_ALL_ORDERS_URL = "/all"+Constants.CUSTOMER_ID_PARAMETER;
	public String CANCEL_ORDER_URL = "/cancel"+Constants.ORDER_ID_PARAMETER;
	
	
	
	
	
	

}
