package com.justshop.view;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.justshop.model.TestPersistence;



@WebServlet("/test")
public class LoginServlet extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LoginServlet(){
		super();
	}
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
	 	System.out.println("inside doGet servlet");
	 	TestPersistence.main(null);

 }

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
		System.out.println("inside doPost servlet");
	 	
		TestPersistence.main(null);

	 }
	

}
