package com.justshop.service;

import java.io.File;
import java.io.FileReader;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;

import com.justshop.business.exception.BusinessErrorTypes;
import com.justshop.business.exception.BusinessException;
import com.justshop.service.representation.BatchProductRepresentation;
import com.justshop.service.representation.PartnerRepresentation;
import com.justshop.service.representation.PartnerRequest;
import com.justshop.service.representation.ProductRepresentation;
import com.justshop.service.representation.ProductRequest;
import com.justshop.service.workflow.PartnerActivity;
import com.justshop.util.Constants;
import com.justshop.util.ValidationUtil;

@CrossOriginResourceSharing(
        allowAllOrigins = true
)

@Path("/partners/")
public class PartnerResource implements PartnerService {

	@GET
	@Produces({"application/xml" , "application/json"})
	@Path(Constants.PARTNER_ID_PARAMETER)
	@Override
	public PartnerRepresentation getPartner(@PathParam(Constants.PARTNER_ID) Integer id) throws BusinessException {
		System.out.println("GET METHOD Request from Client with PartnerRequest String ............." + id);
		PartnerActivity empActivity = new PartnerActivity();
		return empActivity.getPartner(id);
	}
	
	@GET
	@Produces({"application/xml" ,"application/json"})
	@Path("/search/{name}")
	@Override
	public Response getPartners(@PathParam("name") String name) throws BusinessException {
		// TODO Auto-generated method stub
		System.out.println("getPartners:" + name);
		PartnerActivity activity = new PartnerActivity();
		
		List<PartnerRepresentation> representations  = null;
		
		if(!ValidationUtil.isValid(name)){
			throw new BusinessException(BusinessErrorTypes.BAD_INPUT_GIVEN);
		}else{
			representations = activity.getPartners(name);
		}
		System.out.println("getPartners:" + representations);
		GenericEntity<List<PartnerRepresentation>> entity = new GenericEntity<List<PartnerRepresentation>>(representations){};
		return Response.ok(entity).build();
	}
	
	
	@POST
	@Produces({"application/xml" , "application/json"})
	@Consumes({"application/xml" , "application/json"})
	@Path("/add")
	@Override
	public PartnerRepresentation createPartner(PartnerRequest  request) throws BusinessException {
		System.out.println("POST METHOD Request from Client with ............." + request);
		PartnerActivity activity = new PartnerActivity();
		PartnerRepresentation representation = null;
		
		if(!activity.isValidPartner(request)){
			throw new BusinessException(BusinessErrorTypes.BAD_INPUT_GIVEN);
		}else{
			representation = activity.createPartner(request);
		}
		return representation;
	}
	
	
	@POST
	@Consumes({"multipart/form-data"})
	@Path("/addform")
	public PartnerRepresentation createFormEmployee(MultivaluedMap<String, String> formMap) throws BusinessException {
		System.out.println("POST METHOD Request from Client with form............." + formMap);
		PartnerActivity activity = new PartnerActivity();
		PartnerRepresentation representation = null;
		
		if(!activity.isValidPartner(formMap)){
			throw new BusinessException(BusinessErrorTypes.BAD_INPUT_GIVEN);
		}else{
			representation = activity.createPartner(new PartnerRequest(formMap));
		}
		return representation;
	}
	
	
	
	
	
	@PUT
	@Produces({"application/xml" , "application/json"})
	@Consumes({"application/xml" , "application/json"})
	@Path(Constants.UPDATE_PARTNER_URL)
	@Override
	public PartnerRepresentation updatePartner(PartnerRequest  request,@PathParam(Constants.PARTNER_ID) Integer partnerID) throws BusinessException {
		System.out.println("POST METHOD Request from Client with ............." + request);
		PartnerActivity activity = new PartnerActivity();
		PartnerRepresentation representation = null;
		
		if(!activity.isValidPartner(request) && ValidationUtil.isValid(partnerID)){
			throw new BusinessException(BusinessErrorTypes.BAD_INPUT_GIVEN);
		}else{
			representation = activity.updatePartner(request, partnerID);
		}
		return representation;
	}
	
	
	@POST
	@Produces({"application/xml" , "application/json"})
	@Consumes({"application/xml" , "application/json"})
	@Path(Constants.ADD_PARTNER_PROD_URL)
	@Override
	public ProductRepresentation addProduct(ProductRequest  request,@PathParam(Constants.PARTNER_ID) Integer partnerID) throws BusinessException {
		System.out.println("POST METHOD Request from Client with ............." + request);
		PartnerActivity activity = new PartnerActivity();
		ProductRepresentation representation = null;
		
		if(!activity.isValidProduct(request) && ValidationUtil.isValid(partnerID)){
			throw new BusinessException(BusinessErrorTypes.BAD_INPUT_GIVEN);
		}else{
			representation = activity.addProduct(request,partnerID);
		}
		return representation;
	}
	
	@POST
	@Produces({"application/xml" , "application/json"})
	@Consumes({"application/xml" , "application/json"})
	@Path(Constants.ADD_PARTNER_BATCH_PROD_URL)
	@Override
	public List<BatchProductRepresentation> addProductBatch(List<ProductRequest>  request,@PathParam(Constants.PARTNER_ID) Integer partnerID) throws BusinessException {
		System.out.println("POST METHOD Request from Client with ............." + request);
		PartnerActivity activity = new PartnerActivity();
		List<BatchProductRepresentation> representation = null;
		
		if(!activity.isValidProductBatch(request) && ValidationUtil.isValid(partnerID)){
			throw new BusinessException(BusinessErrorTypes.BAD_INPUT_GIVEN);
		}else{
			representation = activity.addProducts(request,partnerID);
		}
		return representation;
	}
	
	/*@POST
	@Consumes({"multipart/form-data"})
	@Path(Constants.ADD_PARTNER_BATCHFILE_PROD_URL)
	@Override
	public List<BatchProductRepresentation> addProductBatchFile(@FormParam("fileAttachment") Attachment attachment,@PathParam(Constants.PARTNER_ID) Integer partnerID) throws BusinessException {
		System.out.println("POST METHOD addProductBatchFile ............." + attachment);
		
		PartnerActivity activity = new PartnerActivity();
		List<BatchProductRepresentation> representation = null;
		FileReader file = (FileReader)attachment.getObject();
		
		if(!activity.isValidProductBatch(request) && ValidationUtil.isValid(partnerID)){
			throw new BusinessException(BusinessErrorTypes.BAD_INPUT_GIVEN);
		}else{
			representation = activity.addProducts(request,partnerID);
		}
		return representation;
	}*/

	
	
	

	
}
