package com.justshop.service;

import javax.ws.rs.core.Response;

import com.justshop.business.exception.BusinessException;
import com.justshop.service.representation.OrderRepresentation;
import com.justshop.service.representation.OrderRequest;

public interface OrderService {
	
	
	public Response getOrder(Integer orderID) throws BusinessException;
	public Response getOrders(Integer customerID) throws BusinessException;
	public OrderRepresentation createOrder(OrderRequest orderRequest) throws BusinessException;
	public Response cancelOrder(Integer orderID) throws BusinessException;

}
