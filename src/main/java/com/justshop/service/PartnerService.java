package com.justshop.service;

import java.util.List;

import javax.jws.WebService;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;

import com.justshop.business.exception.BusinessException;
import com.justshop.service.representation.BatchProductRepresentation;
import com.justshop.service.representation.PartnerRepresentation;
import com.justshop.service.representation.PartnerRequest;
import com.justshop.service.representation.ProductRepresentation;
import com.justshop.service.representation.ProductRequest;

@WebService
public interface PartnerService {

	public PartnerRepresentation getPartner(Integer partnerId) throws BusinessException;

	public Response getPartners(String name) throws BusinessException;

	public PartnerRepresentation createPartner(PartnerRequest request) throws BusinessException;

	public PartnerRepresentation updatePartner(PartnerRequest request, Integer partnerID) throws BusinessException;

	public ProductRepresentation addProduct(ProductRequest request, Integer partnerID) throws BusinessException;

	public List<BatchProductRepresentation> addProductBatch(List<ProductRequest> request, Integer partnerID)
			throws BusinessException;

	/*List<BatchProductRepresentation> addProductBatchFile(Attachment attachment, Integer partnerID)
			throws BusinessException;*/
	

}
