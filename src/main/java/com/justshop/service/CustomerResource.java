package com.justshop.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;

import com.justshop.business.exception.BusinessErrorTypes;
import com.justshop.business.exception.BusinessException;
import com.justshop.service.representation.CartItemRepresentation;
import com.justshop.service.representation.CartItemRequest;
import com.justshop.service.representation.CustomerRepresentation;
import com.justshop.service.representation.CustomerRequest;
import com.justshop.service.representation.HeaderRepresentation;
import com.justshop.service.representation.PaymentRepresentation;
import com.justshop.service.representation.PaymentRequest;
import com.justshop.service.representation.PaymentsRepresentation;
import com.justshop.service.representation.ProductRepresentation;
import com.justshop.service.representation.ValidationRequest;
import com.justshop.service.workflow.CustomerActivity;
import com.justshop.service.workflow.PartnerActivity;
import com.justshop.util.Constants;
import com.justshop.util.ValidationUtil;

@CrossOriginResourceSharing(
        allowAllOrigins = true
)

public class CustomerResource implements CustomerService {

	
	@POST
	@Produces({"application/xml" ,"application/json"})
	@Consumes({"application/xml" , "application/json"})
	@Path(Constants.VALIDATE)
	@Override
	public Response validate(ValidationRequest  customerRequest) throws BusinessException {
		System.out.println("GET METHOD Request from Client with employeeRequest String ............." + customerRequest);
		
		HeaderRepresentation representation = null;
		if(customerRequest!=null && Constants.PARTNER_ROLE.equals(customerRequest.getRoleType())){
			PartnerActivity activity = new PartnerActivity();
			representation = activity.validatePartner(customerRequest);
		}else if(Constants.CUSTOMER_ROLE.equals(customerRequest.getRoleType())){
			CustomerActivity activity = new CustomerActivity();
			representation = activity.validateCustomer(customerRequest);	
		}
		
		 NewCookie cookie = new NewCookie("username", customerRequest.getEmail());
		 NewCookie cookie2 = new NewCookie("password", customerRequest.getPassword());
		 NewCookie cookie3 = new NewCookie("roleType", customerRequest.getRoleType());
		 
		return Response.ok(representation).cookie(cookie,cookie2,cookie3).build();
	}
	
	@GET
	@Produces({"application/xml" ,"application/json"})
	@Path(Constants.GET_CUSTOMER_URL)
	@Override
	public CustomerRepresentation getCustomer(@PathParam(Constants.CUSTOMER_ID) Integer id) throws BusinessException {
		System.out.println("GET METHOD Request from Client with employeeRequest String ............." + id);
		CustomerActivity activity = new CustomerActivity();
		CustomerRepresentation representation = null;
		
		if(!ValidationUtil.isValid(id)){
			throw new BusinessException(BusinessErrorTypes.BAD_INPUT_GIVEN);
		}else{
			representation = activity.getCustomer(id);
		}
		return representation;
	}
	
	
	
	@PUT
	@Produces({"application/xml" , "application/json"})
	@Consumes({"application/xml" , "application/json"})
	@Path(Constants.GET_CUSTOMER_URL)
	@Override
	public CustomerRepresentation updateCustomer(@PathParam(Constants.CUSTOMER_ID) Integer id,CustomerRequest customer) throws BusinessException {
		CustomerActivity custActivity = new CustomerActivity();
		System.out.println("addProduct:" + customer);
		return custActivity.updateCustomer(id,customer);
		
	}
	

	@POST
	@Produces({"application/xml" , "application/json"})
	@Consumes({"application/xml" , "application/json"})
	@Path(Constants.BASE_CUSTOMER_URL+Constants.SAVE)
	@Override
	public CustomerRepresentation createEmployee(CustomerRequest  customerRequest) throws BusinessException {
		System.out.println("POST METHOD Request from Client with ............." + customerRequest);
		CustomerActivity activity = new CustomerActivity();
		CustomerRepresentation representation = null;
		
		if(!activity.isValidCustomer(customerRequest)){
			throw new BusinessException(BusinessErrorTypes.BAD_INPUT_GIVEN);
		}else{
			representation = activity.createEmployee(customerRequest);
		}
		return representation;
	}
	
	
	@POST
	@Consumes({"multipart/form-data"})
	@Path(Constants.BASE_CUSTOMER_URL+Constants.SAVE_FORM)
	public CustomerRepresentation createFormEmployee(MultivaluedMap<String, String> formMap) throws BusinessException {
		System.out.println("POST METHOD Request from Client with form............." + formMap);
		CustomerActivity activity = new CustomerActivity();
		CustomerRepresentation representation = null;
		
		if(!activity.isValidCustomer(formMap)){
			throw new BusinessException(BusinessErrorTypes.BAD_INPUT_GIVEN);
		}else{
			representation = activity.createEmployee(new CustomerRequest(formMap));
		}
		return representation;
	}
	
	
	@GET
	@Produces({"application/xml" , "application/json"})
	@Consumes()
	@Path(Constants.GET_PRODUCT_URL)
	@Override
	public ProductRepresentation getProductByID(@PathParam(Constants.PRODUCT_ID) Integer productID) throws BusinessException {
		CustomerActivity custActivity = new CustomerActivity();
		System.out.println("GET METHOD product id " + productID);
		return custActivity.getProductsByID(productID);
		
	}
	
	@GET
	@Produces({"application/xml" , "application/json"})
	@Consumes()
	@Path(Constants.PRODUCT_SEARCH_URL+Constants.SEARCH_NAME_PARAMETER)
	@Override
	public List<ProductRepresentation> getProductsByName(@PathParam(Constants.SEARCH_NAME) String name) throws BusinessException {
		CustomerActivity custActivity = new CustomerActivity();
		System.out.println("GET METHOD search name " + name);
		
		return custActivity.getProductsByName(name);
		
	}
	
	@POST
	@Produces({"application/xml" , "application/json"})
	@Consumes({"application/xml" , "application/json"})
	@Path(Constants.ADD_CART_ITEM)
	@Override
	public CartItemRepresentation addProduct(CartItemRequest cartItem) throws BusinessException {
		CustomerActivity custActivity = new CustomerActivity();
		System.out.println("addProduct:" + cartItem);
		return custActivity.addProduct(cartItem);
		
	}
	
	@PUT
	@Produces({"application/xml" , "application/json"})
	@Consumes({"application/xml" , "application/json"})
	@Path(Constants.UPDATE_CART_ITEM_PATH)
	@Override
	public CartItemRepresentation updateProduct(CartItemRequest cartItem) throws BusinessException {
		CustomerActivity custActivity = new CustomerActivity();
		System.out.println("addProduct:" + cartItem);
		return custActivity.updateProduct(cartItem);
		
	}
	
	@DELETE
	@Produces({"application/xml" , "application/json"})
	@Consumes({"application/xml" , "application/json"})
	@Path(Constants.DELETE_CART_ITEM_PATH)
	@Override
	public CartItemRepresentation deleteProduct(@PathParam(Constants.CUSTOMER_ID) Integer customerId,@PathParam(Constants.PRODUCT_ID) Integer productId) 
			throws BusinessException {
		CustomerActivity custActivity = new CustomerActivity();
		System.out.println("addProduct:" + productId + customerId);
		return custActivity.deleteProduct(customerId,productId);
		
	}
	
	@GET
	@Produces({Constants.APPLICATION_XML , Constants.APPLICATION_JSON})
	@Path(Constants.GET_CART_URL)
	@Override
	public List<CartItemRepresentation> getCart(@PathParam(Constants.CUSTOMER_ID) Integer customerId) throws BusinessException {
		CustomerActivity custActivity = new CustomerActivity();
		System.out.println("addProduct:" + customerId);
		List<CartItemRepresentation> rep = custActivity.getCart(customerId);
		System.out.println("cart rep:"+rep);
		return rep;
		
	}
	
	@GET
	@Produces({Constants.APPLICATION_XML , Constants.APPLICATION_JSON})
	@Path(Constants.GET_CART_URL+Constants.CART_ID_PARAMETER)
	@Override
	public CartItemRepresentation getCartItem(@PathParam(Constants.CUSTOMER_ID) Integer customerId, @PathParam(Constants.CART_ITEM_ID) Integer cartItemId) throws BusinessException {
		CustomerActivity custActivity = new CustomerActivity();
		System.out.println("getCartItem:" + cartItemId);
		CartItemRepresentation rep = custActivity.getCartItem(customerId,cartItemId);
		System.out.println("cart rep:"+rep);
		return rep;
		
	}
	
	
	@GET
	@Produces({"application/xml" ,"application/json"})
	@Path(Constants.BASE_CUSTOMER_URL+Constants.CUSTOMER_ID_PARAMETER+"/payment/all")
	@Override
	public PaymentsRepresentation getAllPayment(@PathParam(Constants.CUSTOMER_ID) Integer customerId) throws BusinessException {
		System.out.println("getPayment:" + customerId);
		CustomerActivity custActivity = new CustomerActivity();
		return custActivity.getAllPayment(customerId);
	}
	
	@GET
	@Produces({"application/xml" ,"application/json"})
	@Path(Constants.GET_PAYMENT_URL)
	@Override
	public PaymentRepresentation getPayment(@PathParam("paymentId") Integer paymentId, @PathParam(Constants.CUSTOMER_ID) Integer customerId) throws BusinessException {
		System.out.println("getPayment:" + paymentId);
		CustomerActivity custActivity = new CustomerActivity();
		return custActivity.getPayment(customerId,paymentId);
	}
	
	@POST
	@Consumes({"multipart/form-data"})
	@Path(Constants.CREATE_PAYMENT_URL)
	@Override
	public PaymentRepresentation createPayment(@FormParam("nameOnCard") String nameOnCard,
			@FormParam("creditCardNumber") String creditCardNumber,
			@FormParam("creditCardType") String creditCardType, @FormParam("expiryDate") String expiryDate, @FormParam("customerId") Integer customerId) throws BusinessException {
		System.out.println("POST METHOD createPayment:"+ nameOnCard);
		CustomerActivity activity = new CustomerActivity();
		PaymentRepresentation representation = null;
		if(!activity.isValidPaymentInput(nameOnCard,creditCardNumber,creditCardType,expiryDate,customerId)){
			throw new BusinessException(BusinessErrorTypes.BAD_INPUT_GIVEN);
		}else{
			representation = activity.createPayment(new PaymentRequest(nameOnCard,creditCardNumber,creditCardType,expiryDate,customerId));
		}
		
		return representation;
	}


	
	
}