package com.justshop.service;


import java.util.List;

import javax.jws.WebService;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import com.justshop.business.exception.BusinessException;
import com.justshop.service.representation.CartItemRepresentation;
import com.justshop.service.representation.CartItemRequest;
import com.justshop.service.representation.CustomerRepresentation;
import com.justshop.service.representation.CustomerRequest;
import com.justshop.service.representation.PaymentRepresentation;
import com.justshop.service.representation.PaymentsRepresentation;
import com.justshop.service.representation.ProductRepresentation;
import com.justshop.service.representation.ValidationRequest;

@WebService
public interface CustomerService {
	   
	
	public CustomerRepresentation getCustomer(Integer customerId) throws BusinessException;
	public CustomerRepresentation createEmployee(CustomerRequest  customerRequest) throws BusinessException;
	public CustomerRepresentation createFormEmployee(MultivaluedMap<String, String> formMap) throws BusinessException;
	public List<ProductRepresentation> getProductsByName(String name) throws BusinessException;
	public CartItemRepresentation addProduct(CartItemRequest cartItem) throws BusinessException;
	public PaymentRepresentation getPayment(Integer paymentId,Integer customerId) throws BusinessException;

	public PaymentRepresentation createPayment(String nameOnCard, String creditCardNumber, String creditCardType,
			String expiryDate, Integer customerId) throws BusinessException;
	CartItemRepresentation updateProduct(CartItemRequest cartItem) throws BusinessException;
	List<CartItemRepresentation> getCart(Integer customerId) throws BusinessException;
	Response validate(ValidationRequest customerRequest) throws BusinessException;
	PaymentsRepresentation getAllPayment(Integer customerId) throws BusinessException;
	CartItemRepresentation deleteProduct(Integer customerId, Integer productId) throws BusinessException;
	ProductRepresentation getProductByID(Integer productID) throws BusinessException;
	CartItemRepresentation getCartItem(Integer customerId, Integer cartItemId) throws BusinessException;
	CustomerRepresentation updateCustomer(Integer id, CustomerRequest customer) throws BusinessException;
	
	

}
