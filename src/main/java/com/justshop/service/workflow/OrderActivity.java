package com.justshop.service.workflow;

import java.util.ArrayList;
import java.util.List;

import com.justshop.business.OrderBusiness;
import com.justshop.business.OrderBusinessFacade;
import com.justshop.business.exception.BusinessException;
import com.justshop.model.Order;
import com.justshop.model.OrderItem;
import com.justshop.service.representation.OrderItemRepresentation;
import com.justshop.service.representation.OrderRepresentation;
import com.justshop.service.representation.OrderRequest;
import com.justshop.vo.ResultListVO;
import com.justshop.vo.ResultVO;

public class OrderActivity {
	
	private static OrderBusinessFacade facade = new OrderBusiness();

	public OrderRepresentation getOrder(Integer id) throws BusinessException {

		ResultVO<Order> result = facade.getOrder(id);
		if(!result.isSuccessFlag()){
			throw new BusinessException(result.getMessage());
		}
		OrderRepresentation representation = result.getModel().createRepresentationFromModel();
		representation.setOrderItems(createOrderItemRepresentation(result));
		return representation;
	}

	

	public OrderRepresentation createOrder(OrderRequest request) throws BusinessException {
		// TODO Auto-generated method stub
		ResultVO<Order> result = facade.confirmOrder(request.getCustomerID(),request.getPaymentDetailID());
		if(!result.isSuccessFlag()){
			throw new BusinessException(result.getMessage());
		}
		OrderRepresentation representation = result.getModel().createRepresentationFromModel();
		representation.setOrderItems(createOrderItemRepresentation(result));
		return representation;
	}
	

	public List<OrderRepresentation> getOrders(Integer customerID) throws BusinessException {
		// TODO Auto-generated method stub
		ResultListVO<Order> result = facade.getOrders(customerID);
		System.out.println("getOrders in orderactivity:"+ result);
		if(!result.isSuccessFlag()){
			throw new BusinessException(result.getMessage());
		}
		List<OrderRepresentation> orderItemReps = createOrderRepresentationList(result);		
		
		return orderItemReps;
		
		
		//return null;
	}
	
	/*private OrderRepresentation createOrderRepresentation(final ResultVO<Order> result) {
		// TODO Auto-generated method stub
		OrderRepresentation representation = new OrderRepresentation();
		representation.setCustomerID(result.getModel().getCustomer().getCustomerId());
		representation.setIsCancelled(result.getModel().getIsCancelled());
		representation.setOrderID(result.getModel().getOrderID());
		representation.setPaymentDetailID(result.getModel().getPaymentDetail().getPaymentDetailID());
		representation.setTotalPaid(result.getModel().getTotalPaid());
		
		return representation;
	}
	*/
	
	
	private List<OrderItemRepresentation> createOrderItemRepresentation(final ResultVO<Order> result) {
		// TODO Auto-generated method stub
		List<OrderItem> orderItems = result.getModel().getOrderItems();
		List<OrderItemRepresentation> orderItemReps = new ArrayList<OrderItemRepresentation>(orderItems.size());
		OrderItemRepresentation orderItemRep = null;
		for(OrderItem orderItem: orderItems){
			orderItemRep = orderItem.createRepresentationFromModel();
			orderItemReps.add(orderItemRep);			
		}
		return orderItemReps;
	}
	
	private List<OrderRepresentation> createOrderRepresentationList(final ResultListVO<Order> result) {
		// TODO Auto-generated method stub
		List<Order> orders = result.getModelList();
		List<OrderRepresentation> orderReps = new ArrayList<OrderRepresentation>(orders.size());
		for(Order order: orders){
			orderReps.add(order.createRepresentationFromModel());			
		}
		return orderReps;
	}



	public OrderRepresentation cancelOrder(Integer orderID) throws BusinessException {
		// TODO Auto-generated method stub
		ResultVO<Order> result = facade.cancelOrder(orderID);
		if(!result.isSuccessFlag()){
			throw new BusinessException(result.getMessage());
		}
		OrderRepresentation representation = new OrderRepresentation();
		representation.setOrderID(result.getModel().getOrderID());
		representation.setIsCancelled(result.isSuccessFlag());
		return representation;
	}

	

}
