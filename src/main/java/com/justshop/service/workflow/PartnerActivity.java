package com.justshop.service.workflow;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MultivaluedMap;

import com.justshop.business.PartnerBusiness;
import com.justshop.business.PartnerBusinessFacade;
import com.justshop.business.exception.BusinessErrorTypes;
import com.justshop.business.exception.BusinessException;
import com.justshop.model.Customer;
import com.justshop.model.Partner;
import com.justshop.model.Product;
import com.justshop.service.representation.BatchProductRepresentation;
import com.justshop.service.representation.CustomerHeaderRepresentation;
import com.justshop.service.representation.ErrorRepresentation;
import com.justshop.service.representation.HeaderRepresentation;
import com.justshop.service.representation.LinkRepresentation;
import com.justshop.service.representation.LinkRepresentationTypes;
import com.justshop.service.representation.PartnerHeaderRepresentation;
import com.justshop.service.representation.PartnerRepresentation;
import com.justshop.service.representation.PartnerRequest;
import com.justshop.service.representation.ProductRepresentation;
import com.justshop.service.representation.ProductRequest;
import com.justshop.service.representation.ValidationRequest;
import com.justshop.util.ValidationUtil;
import com.justshop.vo.ResultListVO;
import com.justshop.vo.ResultVO;

public class PartnerActivity {

	private static PartnerBusinessFacade facade = new PartnerBusiness();
	
	public PartnerRepresentation getPartner(Integer id) throws BusinessException {
		
		ResultVO<Partner> result = facade.get(id);
		if (!result.isSuccessFlag()) {
			throw new BusinessException(result.getMessage());
		}		
		PartnerRepresentation representation = result.getModel().createRepresentation();
		representation.getLinks().add(new LinkRepresentation(LinkRepresentationTypes.UPDATE_PARTNER,id.toString()));
		return representation;
	}
	
	

	public List<PartnerRepresentation> getPartners(String name) throws BusinessException {
		ResultListVO<Partner> result = facade.getByName(name);
		if (!result.isSuccessFlag()) {
			throw new BusinessException(result.getMessage());
		}
		List<PartnerRepresentation> representationList = new ArrayList<PartnerRepresentation>(result.getModelList().size());
		PartnerRepresentation representation = null;
		for (Partner model : result.getModelList()) {
			representation = model.createRepresentation();			
			representationList.add(representation);
		}
		return representationList;
	}

	public boolean isValidPartner(MultivaluedMap<String, String> formMap) {
		// TODO Auto-generated method stub
		return ValidationUtil.isValid(formMap.getFirst("name"))
				&& ValidationUtil.isValid(formMap.getFirst("address"))
				&& ValidationUtil.isValid(formMap.getFirst("city"))
				&& ValidationUtil.isValid(formMap.getFirst("state"))
				&& ValidationUtil.isValid(formMap.getFirst("country"))
						&& ValidationUtil.isValidZip(formMap.getFirst("zip"));
	}

	public boolean isValidPartner(PartnerRequest request) {
		// TODO Auto-generated method stub
		return ValidationUtil.isValid(request.getName())
				&& ValidationUtil.isValid(request.getAddress())
				&& ValidationUtil.isValid(request.getCity())
				&& ValidationUtil.isValidZip(request.getZip())
				&& ValidationUtil.isValid(request.getCountry());
	}

	public PartnerRepresentation createPartner(PartnerRequest request) throws BusinessException {
		// TODO Auto-generated method stub
		Partner model = createPartnerModelFromRequest(request);
		ResultVO<Partner> result = facade.create(model);
		if (!result.isSuccessFlag()) {
			throw new BusinessException(result.getMessage());
		}
		PartnerRepresentation representation = result.getModel().createRepresentation();

		return representation;
	}

	public PartnerRepresentation updatePartner(PartnerRequest request, Integer partnerID) throws BusinessException {
		// TODO Auto-generated method stub
		Partner model = createPartnerModelFromRequest(request);
		model.setPartnerId(partnerID);
		System.out.println("partner model: "+ model);
		ResultVO<Partner> result = facade.update(model);
		if (!result.isSuccessFlag()) {
			throw new BusinessException(result.getMessage());
		}
		PartnerRepresentation representation = result.getModel().createRepresentation();

		return representation;
	}

	public boolean isValidProduct(ProductRequest request) {
		// TODO Auto-generated method stub
		return ValidationUtil.isValid(request.getName())
				&& ValidationUtil.isValid(request.getDescription())
				&& ValidationUtil.isValid(request.getKeywords())
				&& ValidationUtil.isValid(request.getStatus())
				&& ValidationUtil.isValid(request.getQuantity())
				&& ValidationUtil.isValid(request.getUnitPrice());
	}

	
	public ProductRepresentation addProduct(ProductRequest request, Integer partnerID) throws BusinessException {
		// TODO Auto-generated method stub
		Product model = createModelFromRequest(request);
		model.setPartner(new Partner(partnerID));
		ResultVO<Product> result = facade.addProduct(model);
		if (!result.isSuccessFlag()) {
			throw new BusinessException(result.getMessage());
		}
		ProductRepresentation representation = result.getModel().createRepresentationFromModel();

		return representation;
	}

	private Partner createPartnerModelFromRequest(PartnerRequest request) {
		// TODO Auto-generated method stub
		Partner model = new Partner();
		model.setName(request.getName());
		model.setAddress(request.getAddress());
		model.setCity(request.getCity());
		model.setCountry(request.getCountry());
		model.setZip(request.getZip());
		model.setState(request.getState());	
		model.setEmail(request.getEmail());
		model.setPassword(request.getPassword());	
		
		return model;
	}

	private Product createModelFromRequest(ProductRequest request) {
		// TODO Auto-generated method stub
		Product model = new Product();
		model.setName(request.getName());
		model.setDescription(request.getDescription());
		model.setKeyWords(request.getKeywords());
		model.setStatus(request.getStatus());
		model.setAvailableQuantity(request.getQuantity());
		model.setUnitPrice(request.getUnitPrice());	
		
		return model;
	}

	public boolean isValidProductBatch(List<ProductRequest> requestList) {
		// TODO Auto-generated method stub
		int size = requestList.size();
		System.out.println("size before:"+ size);
		for (ProductRequest each: requestList){
			if(!isValidProduct(each)){
				size--;
			}
				
		}
		System.out.println("size:"+size);
		// only if all products are invalid this will return false so that correct products in batch can be processed.
		//return size!=0 ;
		return true;
	}

	public List<BatchProductRepresentation> addProducts(List<ProductRequest> request, Integer partnerID) throws BusinessException {
		// TODO Auto-generated method stub
		List<Product> modelList = new ArrayList<Product>(request.size());
		Product model = null;
		List<BatchProductRepresentation> representation = null;
		Partner partner = new Partner(partnerID);
		for (ProductRequest each : request) {
			model = createModelFromRequest(each);
			model.setPartner(partner);
			modelList.add(model);
		}
		System.out.println("modelList:" + modelList);
		Map<Integer, ResultVO<Product>> resultMap = facade.addProducts(modelList);
		System.out.println("resultMap:" + resultMap);
		if (resultMap != null) {
			ResultVO<Product> eachResult = null;
			BatchProductRepresentation eachRepresentation = null;
			representation = new ArrayList<BatchProductRepresentation>(resultMap.size());
			for (Integer key : resultMap.keySet()) {
				eachResult = resultMap.get(key);
				if (eachResult != null) {
					eachRepresentation = createBatchProductFromModel(eachResult);
					representation.add(eachRepresentation);
				}
			}
		} else {
			throw new BusinessException(BusinessErrorTypes.BATCH_FAILED_ERROR);
		}

		System.out.println("representation:" + representation);

		return representation;
	}

	private BatchProductRepresentation createBatchProductFromModel(ResultVO<Product> eachResult) {
		// TODO Auto-generated method stub
		BatchProductRepresentation representation = new BatchProductRepresentation();
		representation.setName(eachResult.getModel().getName());
		representation.setDescription(eachResult.getModel().getDescription());
		representation.setKeywords(eachResult.getModel().getKeyWords());
		representation.setStatus(eachResult.getModel().getStatus());
		representation.setAvailableQuantity(eachResult.getModel().getAvailableQuantity());
		representation.setUnitPrice(eachResult.getModel().getUnitPrice());	
		representation.setProductID(eachResult.getModel().getProductId());
		representation.setIsSuccess(eachResult.isSuccessFlag());
		representation.createLinks();
		
		if(!eachResult.isSuccessFlag())// if not successful include error message for each
			representation.setError(new ErrorRepresentation(eachResult.getMessage().getErrorMessage(), eachResult.getMessage().getErrorCode()));
		return representation;
	}

	public HeaderRepresentation validatePartner(ValidationRequest customerRequest) throws BusinessException {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		ResultVO<Partner> result = facade.validate(new Partner(customerRequest.getEmail(),customerRequest.getPassword()));
		if (!result.isSuccessFlag()) {
			throw new BusinessException(result.getMessage());
		}
		PartnerHeaderRepresentation header = new PartnerHeaderRepresentation();
		header.setPartner(result.getModel().createMiniPartnerRepresentation());
		header.createLinks();
		return header;
	}
	
	
}
