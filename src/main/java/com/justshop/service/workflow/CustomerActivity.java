package com.justshop.service.workflow;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MultivaluedMap;

import com.justshop.business.CustomerBusiness;
import com.justshop.business.CustomerBusinessFacade;
import com.justshop.business.exception.BusinessException;
import com.justshop.model.CartItem;
import com.justshop.model.Customer;
import com.justshop.model.PaymentDetails;
import com.justshop.model.Product;
import com.justshop.service.representation.CartItemRepresentation;
import com.justshop.service.representation.CartItemRequest;
import com.justshop.service.representation.CustomerHeaderRepresentation;
import com.justshop.service.representation.CustomerRepresentation;
import com.justshop.service.representation.CustomerRequest;
import com.justshop.service.representation.ValidationRequest;
import com.justshop.service.representation.HeaderRepresentation;
import com.justshop.service.representation.LinkRepresentation;
import com.justshop.service.representation.PaymentRepresentation;
import com.justshop.service.representation.PaymentRequest;
import com.justshop.service.representation.PaymentsRepresentation;
import com.justshop.service.representation.ProductRepresentation;
import com.justshop.util.ValidationUtil;
import com.justshop.vo.ResultListVO;
import com.justshop.vo.ResultVO;

/**
 * This class' responsibility is to manage the workflow of
 * accessing/creating/updating/deleting resources using the EmployeeDOA object.
 *
 */
public class CustomerActivity {

	private static CustomerBusinessFacade facade = new CustomerBusiness();

	/*
	 * public Set<CustomerRepresentation> getCustomers() {
	 * 
	 * Set<Employee> employees = new HashSet<Employee>();
	 * Set<CustomerRepresentation> employeeRepresentations = new
	 * HashSet<CustomerRepresentation>(); employees = facade.getAllEmployees();
	 * 
	 * Iterator<Employee> it = employees.iterator(); while(it.hasNext()) {
	 * Employee emp = (Employee)it.next(); CustomerRepresentation
	 * employeeRepresentation = new CustomerRepresentation();
	 * employeeRepresentation.setGid(emp.getGid());
	 * employeeRepresentation.setFirstName(emp.getFirstName());
	 * employeeRepresentation.setLastName(emp.getLastName());
	 * 
	 * //now add this representation in the list
	 * employeeRepresentations.add(employeeRepresentation); } return
	 * employeeRepresentations; }
	 */

	public CustomerRepresentation getCustomer(Integer id) throws BusinessException {

		ResultVO<Customer> result = facade.get(id);
		if (!result.isSuccessFlag()) {
			throw new BusinessException(result.getMessage());
		}
		CustomerRepresentation representation = result.getModel().createRepresentation();
		
		
		return representation;
	}

	public CustomerRepresentation createEmployee(CustomerRequest request) throws BusinessException {
		// TODO Auto-generated method stub
		Customer model = new Customer();
		model.setFirstName(request.getFirstName());
		model.setLastName(request.getLastName());
		model.setEmailAddress(request.getEmail());
		model.setPhoneNumber(request.getPhoneNumber());
		model.setPassword(request.getPassword());
		ResultVO<Customer> result = facade.create(model);
		if (!result.isSuccessFlag()) {
			throw new BusinessException(result.getMessage());
		}
		CustomerRepresentation representation = result.getModel().createRepresentation();

		return representation;
	}

	public List<ProductRepresentation> getProductsByName(String name) throws BusinessException {
		// TODO Auto-generated method stub
		ResultListVO<Product> result = facade.getProductsByName(name);
		if (!result.isSuccessFlag()) {
			throw new BusinessException(result.getMessage());
		}
		List<ProductRepresentation> representationList = new ArrayList<ProductRepresentation>(result.getModelList().size());
		ProductRepresentation representation = null;
		for (Product model : result.getModelList()) {
			representation = model.createRepresentationFromModel();
			representationList.add(representation);
		}
		return representationList;
	}

	public CartItemRepresentation addProduct(CartItemRequest request) throws BusinessException {
		// TODO Auto-generated method stub
		CartItem model = new CartItem(new Product(request.getProductID()), new Customer(request.getCustomerID()),
				request.getQuantity(), request.getDeliveryType());

		ResultVO<CartItem> result = facade.addProduct(model);
		if (!result.isSuccessFlag()) {
			throw new BusinessException(result.getMessage());
		}
		CartItemRepresentation representation = new CartItemRepresentation();
		representation.setCustomerID(result.getModel().getCustomer().getCustomerId());
		representation.setProduct(result.getModel().getProduct().createRepresentationFromModel());
		representation.setQuantity(result.getModel().getQuantity());
		representation.setItemNoLongerAvailable(result.getModel().getItemNoLongerAvailable());
		representation.setDeliveryType(result.getModel().getDeliveryType());

		return representation;
	}

	public PaymentRepresentation getPayment(Integer customerId, Integer paymentId) throws BusinessException {
		// TODO Auto-generated method stub
		PaymentDetails model = new PaymentDetails(paymentId);
		model.setCustomer(new Customer(customerId));
		ResultVO<PaymentDetails> result = facade.getPayment(model);
		if (!result.isSuccessFlag()) {
			throw new BusinessException(result.getMessage());
		}
		PaymentRepresentation representation = null;
		if (result != null && result.isSuccessFlag()) {
			representation = result.getModel().createRepresentationFromModel();
		} else {
			// no payment found
		}

		return representation;
	}

	public PaymentRepresentation createPayment(PaymentRequest request) throws BusinessException {
		// TODO Auto-generated method stub
		PaymentDetails model = new PaymentDetails();
		model.setCreditCardNumber(request.getCreditCardNumber());
		model.setCreditCardType(request.getCreditCardType());
		model.setExpiryDate(request.getExpiryDate());
		model.setCustomer(new Customer(request.getCustomerId()));
		ResultVO<PaymentDetails> result = facade.createPayment(model);
		if (!result.isSuccessFlag()) {
			throw new BusinessException(result.getMessage());
		}
		PaymentRepresentation representation = result.getModel().createRepresentationFromModel();

		return representation;
	}

	public boolean isValidPaymentInput(String nameOnCard, String creditCardNumber, String creditCardType,
			String expiryDate, Integer customerId) {
		// TODO Auto-generated method stub
		System.out.println(
				nameOnCard + ":" + creditCardNumber + ":" + creditCardType + ":" + expiryDate + ":" + customerId);

		return ValidationUtil.isValid(nameOnCard) && ValidationUtil.isValid(creditCardNumber)
				&& ValidationUtil.isValid(creditCardType) && ValidationUtil.isValidDate(expiryDate);
	}

	public boolean isValidCustomer(MultivaluedMap<String, String> formMap) {
		// TODO Auto-generated method stub
		/*
		 * private String firstName; private String lastName; private String
		 * email; private String phoneNumber;
		 */

		return ValidationUtil.isValid(formMap.getFirst("firstName"))
				&& ValidationUtil.isValid(formMap.getFirst("lastName"))
				&& ValidationUtil.isValidEmail(formMap.getFirst("email"))
				&& ValidationUtil.isValidPhone(formMap.getFirst("phoneNumber"));
	}

	public boolean isValidCustomer(CustomerRequest customerRequest) {
		// TODO Auto-generated method stub
		return ValidationUtil.isValid(customerRequest.getFirstName())
				&& ValidationUtil.isValid(customerRequest.getLastName())
				&& ValidationUtil.isValidEmail(customerRequest.getEmail())
				&& ValidationUtil.isValidPhone(customerRequest.getPhoneNumber());
	}

	public CartItemRepresentation updateProduct(CartItemRequest request) throws BusinessException {
		// TODO Auto-generated method stub
		CartItem model = new CartItem(new Product(request.getProductID()), new Customer(request.getCustomerID()),
				request.getQuantity(), request.getDeliveryType());

		ResultVO<CartItem> result = facade.updateProduct(model);
		if (!result.isSuccessFlag()) {
			throw new BusinessException(result.getMessage());
		}
		CartItemRepresentation representation = new CartItemRepresentation();
		representation.setCustomerID(result.getModel().getCustomer().getCustomerId());
		representation.setProduct(result.getModel().getProduct().createRepresentationFromModel());
		representation.setQuantity(result.getModel().getQuantity());
		representation.setItemNoLongerAvailable(result.getModel().getItemNoLongerAvailable());
		representation.setDeliveryType(result.getModel().getDeliveryType());

		return representation;
	}

	public List<CartItemRepresentation> getCart(Integer customerId) throws BusinessException {
		// TODO Auto-generated method stub
		ResultListVO<CartItem> result = facade.getCart(customerId);
		if (!result.isSuccessFlag()) {
			throw new BusinessException(result.getMessage());
		}
		List<CartItemRepresentation> prodRepList = new ArrayList<CartItemRepresentation>(result.getModelList().size());
		CartItemRepresentation productRep = null;
		
		
		for (CartItem prod : result.getModelList()) {
			productRep = prod.createRepresentationFromModel();
			prodRepList.add(productRep);
		}
		return prodRepList;
	}

	public HeaderRepresentation validateCustomer(ValidationRequest customerRequest) throws BusinessException {
		// TODO Auto-generated method stub
		ResultVO<Customer> result = facade.validate(new Customer(customerRequest.getEmail(),customerRequest.getPassword()));
		if (!result.isSuccessFlag()) {
			throw new BusinessException(result.getMessage());
		}
		CustomerHeaderRepresentation header = new CustomerHeaderRepresentation();
		header.setCustomer(result.getModel().createMiniCustomerRepresentation());
		header.createLinks();
		return header;
	}

	public PaymentsRepresentation getAllPayment(Integer customerId) throws BusinessException {
		// TODO Auto-generated method stub
		
		PaymentsRepresentation allPayments = null;
		ResultListVO<PaymentDetails> result = facade.getAllPayments(customerId);
		if (!result.isSuccessFlag()) {
			throw new BusinessException(result.getMessage());
		}
		List<PaymentRepresentation> representationList = new ArrayList<PaymentRepresentation>(result.getModelList().size());
		PaymentRepresentation representation = null;
		for (PaymentDetails model : result.getModelList()) {
			representation = model.createRepresentationFromModel();
			representation.createLinks();
			representationList.add(representation);
		}
		allPayments = new PaymentsRepresentation();
		allPayments.setPayment(representationList);
		allPayments.setCustomerId(customerId);
		
		allPayments.createLinks();
		return allPayments;
		
	}

	public CartItemRepresentation deleteProduct(Integer customerId, Integer productId) throws BusinessException {
		// TODO Auto-generated method stub
	

		ResultVO<CartItem> result = facade.deleteProduct(customerId,productId);
		if (!result.isSuccessFlag()) {
			throw new BusinessException(result.getMessage());
		}
		CartItemRepresentation representation = new CartItemRepresentation();
		representation.setCustomerID(result.getModel().getCustomer().getCustomerId());
		representation.setProduct(result.getModel().getProduct().createRepresentationFromModel());
		representation.setDeleted(true);

		return representation;
	}

	public ProductRepresentation getProductsByID(Integer productID) throws BusinessException {
		// TODO Auto-generated method stub
		ResultVO<Product> result = facade.getProduct(productID);
		if (!result.isSuccessFlag()) {
			throw new BusinessException(result.getMessage());
		}
		ProductRepresentation representation = result.getModel().createRepresentationFromModel();
		

		return representation;
	}

	public CartItemRepresentation getCartItem(Integer customerId, Integer cartItemId) throws BusinessException {
		// TODO Auto-generated method stub
		ResultVO<CartItem> result = facade.getCartItem(customerId,cartItemId);
		if (!result.isSuccessFlag()) {
			throw new BusinessException(result.getMessage());
		}
		CartItemRepresentation representation = result.getModel().createRepresentationFromModel();
		

		return representation;
	}

	public CustomerRepresentation updateCustomer(Integer customerID,CustomerRequest request) {
		// TODO Auto-generated method stub
	/*	Customer model = new Customer(new Customer(request.getCustomerID()),
				request.getQuantity(), request.getDeliveryType());

		ResultVO<Customer> result = facade.updateProduct(model);
		if (!result.isSuccessFlag()) {
			throw new BusinessException(result.getMessage());
		}
		CustomerRepresentation representation = new CustomerRepresentation();
		representation.setCustomerID(result.getModel().getCustomer().getCustomerId());
		representation.setProduct(result.getModel().getProduct().createRepresentationFromModel());
		representation.setQuantity(result.getModel().getQuantity());
		representation.setItemNoLongerAvailable(result.getModel().getItemNoLongerAvailable());
		representation.setDeliveryType(result.getModel().getDeliveryType());*/

		return null;
	}

}
