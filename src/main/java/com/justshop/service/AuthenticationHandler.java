package com.justshop.service;

import javax.ws.rs.core.Response;

import org.apache.cxf.configuration.security.AuthorizationPolicy;
import org.apache.cxf.jaxrs.ext.RequestHandler;
import org.apache.cxf.jaxrs.model.ClassResourceInfo;
import org.apache.cxf.message.Message;

import com.justshop.business.exception.BusinessException;
import com.justshop.dao.CustomerDAO;
import com.justshop.dao.PartnerDAO;

public class AuthenticationHandler implements RequestHandler {
 
    public Response handleRequest(Message m, ClassResourceInfo resourceClass) {
        /*AuthorizationPolicy policy = (AuthorizationPolicy)m.get(AuthorizationPolicy.class);
        
        String username = policy.getUserName();
        String password = policy.getPassword(); 
        System.out.println("username:"+username);
        System.out.println("password:"+password);
        *//*HttpServletRequest request = (HttpServletRequest) m.get(AbstractHTTPDestination.HTTP_REQUEST);
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
        	System.out.println("cookies not null:");
          for (Cookie cookie : cookies) {
            System.out.println(cookie.getName());
          }
        }*/
        
        
        //if (isValidUser(username,password)) {
        if (true) {
            // let request to continue
            return null;
        } else {
            // authentication failed, request the authetication, add the realm name if needed to the value of WWW-Authenticate 
            return Response.status(401).header("WWW-Authenticate", "Basic").build();
        }
    }

	private boolean isValidUser(String username, String password) {
		// TODO Auto-generated method stub
		CustomerDAO customer = new CustomerDAO();
		PartnerDAO partner = new PartnerDAO();
		
		try {
			if(customer.validate(username, password) != null){
				return true;		
			}else if(partner.validate(username, password) !=null) {
				return true;
			}
		} catch (BusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		
		return false;
	}
 
}