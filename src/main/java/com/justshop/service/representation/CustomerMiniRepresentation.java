package com.justshop.service.representation;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "customer")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class CustomerMiniRepresentation extends AbstractRepresentation{
	
	private Integer customerID;
	private String firstName;
	private String emailAddress;
	
	private Integer numOfItemsInCart;
	
	

	public Integer getNumOfItemsInCart() {
		return numOfItemsInCart;
	}

	public void setNumOfItemsInCart(Integer numOfItemsInCart) {
		this.numOfItemsInCart = numOfItemsInCart;
	}

	public CustomerMiniRepresentation() {}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	

	

	public Integer getCustomerID() {
		return customerID;
	}

	public void setCustomerID(Integer customerID) {
		this.customerID = customerID;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public CustomerMiniRepresentation(Integer gid, String firstName, String emailAddress, Integer numofItems) {
		super();
		this.customerID = gid;
		this.firstName = firstName;
		this.emailAddress = emailAddress;
		this.numOfItemsInCart = numofItems;
	}
	
	public void createLinks(){
		
		links = new ArrayList<LinkRepresentation>();
		LinkRepresentation self = new LinkRepresentation(LinkRepresentationTypes.SELF_CUSTOMER,customerID.toString());
		links.add(self);
		LinkRepresentation cart = new LinkRepresentation(LinkRepresentationTypes.GET_CART,customerID.toString());
		links.add(cart);
		LinkRepresentation allOrders = new LinkRepresentation(LinkRepresentationTypes.GET_ALL_ORDERS,customerID.toString());
		links.add(allOrders);
		
	}

	

	
	
}
