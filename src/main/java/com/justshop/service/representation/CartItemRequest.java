package com.justshop.service.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="CartItem")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")

public class CartItemRequest {
	
	private Integer customerID;
	private Integer productID;
	private Integer quantity;
	private String deliveryType;
	
	
	public Integer getCustomerID() {
		return customerID;
	}
	public void setCustomerID(Integer customerID) {
		this.customerID = customerID;
	}
	public Integer getProductID() {
		return productID;
	}
	public void setProductID(Integer productID) {
		this.productID = productID;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getDeliveryType() {
		return deliveryType;
	}
	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}
	@Override
	public String toString() {
		return "CartItemRequest [customerID=" + customerID + ", productID=" + productID + ", quantity=" + quantity
				+ ", deliveryType=" + deliveryType + "]";
	}
	
	
	
	

}
