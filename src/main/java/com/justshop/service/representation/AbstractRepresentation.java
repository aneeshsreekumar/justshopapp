package com.justshop.service.representation;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;


@XmlAccessorType(XmlAccessType.FIELD)
public abstract class AbstractRepresentation {
	
	@XmlElementWrapper(name="links")
	@XmlElement(name="link")
	protected List<LinkRepresentation> links;

	public List<LinkRepresentation> getLinks() {
		return links;
	}

	public void setLinks(List<LinkRepresentation> links) {
		this.links = links;
	}
	
	public abstract void createLinks();

}
