package com.justshop.service.representation;

import com.justshop.util.Constants;

public enum LinkRepresentationTypes {
	
	UPDATE_CART_ITEM("Update",Constants.hostURL+Constants.UPDATE_CART_ITEM_PATH,Constants.APPLICATION_JSON,Constants.PUT),
	SELF_CUSTOMER(Constants.SELF,Constants.hostURL+Constants.GET_CUSTOMER_URL,Constants.APPLICATION_JSON,Constants.GET),
	GET_CART("Cart",Constants.hostURL+Constants.GET_CART_URL,Constants.APPLICATION_JSON,Constants.GET),
	CREATE_ORDER("Finalize Order",Constants.hostURL+Constants.CREATE_ORDER_URL,Constants.APPLICATION_JSON,Constants.POST),
	CREATE_PAYMENT("Add Payment",Constants.hostURL+Constants.CREATE_PAYMENT_URL,Constants.APPLICATION_JSON,Constants.POST),
	DELETE_CART_ITEM("Delete CartItem",Constants.hostURL+Constants.DELETE_CART_ITEM_PATH,Constants.APPLICATION_JSON,Constants.DELETE),
	SELF_PAYMENT(Constants.SELF,Constants.hostURL+Constants.GET_PAYMENT_URL,Constants.APPLICATION_JSON,Constants.GET),
	SELF_PRODUCT(Constants.SELF,Constants.hostURL+Constants.GET_PRODUCT_URL,Constants.APPLICATION_JSON,Constants.GET),
	SELF_CART_ITEM(Constants.SELF,Constants.hostURL+Constants.GET_CART_URL+Constants.CART_ID_PARAMETER,Constants.APPLICATION_JSON,Constants.GET),
	SEARCH_PRODUCT("Search Product",Constants.hostURL+Constants.PRODUCT_SEARCH_URL,Constants.APPLICATION_JSON,Constants.GET),
	SELF_PARTNER(Constants.SELF,Constants.hostURL+Constants.GET_PARTNER_URL,Constants.APPLICATION_JSON,Constants.GET),
	UPDATE_PARTNER("Update Partner",Constants.hostURL+Constants.BASE_PARTNER_URL+Constants.UPDATE_PARTNER_URL,Constants.APPLICATION_JSON,Constants.PUT),
	ADD_PARTNER_PROD("Add Product",Constants.hostURL+Constants.BASE_PARTNER_URL+Constants.ADD_PARTNER_PROD_URL,Constants.APPLICATION_JSON,Constants.POST),
	ADD_PARTNER_BATCH_PROD("Batch Add",Constants.hostURL+Constants.BASE_PARTNER_URL+Constants.ADD_PARTNER_BATCH_PROD_URL,Constants.APPLICATION_JSON,Constants.POST), 
	SELF_ORDER(Constants.SELF,Constants.hostURL+Constants.BASE_ORDER_URL+Constants.ORDER_ID_PARAMETER,Constants.APPLICATION_JSON,Constants.GET),
	CANCEL_ORDER("Cancel Order",Constants.hostURL+Constants.BASE_ORDER_URL+Constants.CANCEL_ORDER_URL,Constants.APPLICATION_JSON,Constants.DELETE),
	GET_ALL_ORDERS("My Orders",Constants.hostURL+Constants.BASE_ORDER_URL+Constants.GET_ALL_ORDERS_URL,Constants.APPLICATION_JSON,Constants.GET), 
	GET_PRODUCT("Get Product",Constants.hostURL+Constants.GET_PRODUCT_URL,Constants.APPLICATION_JSON,Constants.GET);
	
	
	
	private String rel;
	private String URI;
	private String mediaType;
	private String action;
	
	LinkRepresentationTypes(String rel, String URI, String mediaType, String action){
		this.rel = rel;
		this.URI = URI;
		this.mediaType = mediaType;
		this.action = action;
		
	}

	public String getRel() {
		return rel;
	}

	public void setRel(String rel) {
		this.rel = rel;
	}

	public String getURI() {
		return URI;
	}

	public void setURI(String uRI) {
		URI = uRI;
	}

	public String getMediaType() {
		return mediaType;
	}

	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

		
	
	
}
