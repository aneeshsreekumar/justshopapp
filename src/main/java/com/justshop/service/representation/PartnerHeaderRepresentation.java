package com.justshop.service.representation;

public class PartnerHeaderRepresentation extends HeaderRepresentation {
	
private PartnerMiniRepresentation partner;
	
	public PartnerMiniRepresentation getPartner() {
		return partner;
	}


	public void setPartner(PartnerMiniRepresentation customer) {
		this.partner = customer;
	}	
	

}
