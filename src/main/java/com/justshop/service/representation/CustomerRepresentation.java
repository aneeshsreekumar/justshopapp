package com.justshop.service.representation;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "Customer")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class CustomerRepresentation extends AbstractRepresentation{
	
	private String customerID;
	private String lastName;
	private String firstName;
	private String emailAddress;
	private String phoneNumber;
	private String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public CustomerRepresentation() {}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	@Override
	public void createLinks() {
		// TODO Auto-generated method stub
		links = new ArrayList<LinkRepresentation>();
		LinkRepresentation link = new LinkRepresentation(LinkRepresentationTypes.SELF_CUSTOMER,getCustomerID().toString());
		links.add(link);
		link = new LinkRepresentation(LinkRepresentationTypes.SEARCH_PRODUCT);
		links.add(link);
		link = new LinkRepresentation(LinkRepresentationTypes.GET_CART,getCustomerID().toString());
		links.add(link);
		
	}

	

	
	
}
