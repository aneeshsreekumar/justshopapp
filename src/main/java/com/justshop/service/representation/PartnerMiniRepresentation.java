package com.justshop.service.representation;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "partner")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class PartnerMiniRepresentation extends AbstractRepresentation{
	
	
		private Integer partnerID;
		private String name;
		private String emailAddress;
		
		
		

		
		public PartnerMiniRepresentation() {}

		
		

		

		

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Integer getPartnerID() {
			return partnerID;
		}

		public void setPartnerID(Integer partnerID) {
			this.partnerID = partnerID;
		}

		public String getEmailAddress() {
			return emailAddress;
		}

		public void setEmailAddress(String emailAddress) {
			this.emailAddress = emailAddress;
		}

		public PartnerMiniRepresentation(Integer gid, String firstName, String emailAddress) {
			super();
			this.partnerID = gid;
			this.name = firstName;
			this.emailAddress = emailAddress;
			
		}
		
		public void createLinks(){
			LinkRepresentation link = null;
			links = new ArrayList<LinkRepresentation>();
			link = new LinkRepresentation(LinkRepresentationTypes.SELF_PARTNER,partnerID.toString());
			links.add(link);
			link = new LinkRepresentation(LinkRepresentationTypes.ADD_PARTNER_PROD,partnerID.toString());
			links.add(link);
			link = new LinkRepresentation(LinkRepresentationTypes.ADD_PARTNER_BATCH_PROD,partnerID.toString());
			links.add(link);
			
		}

	


}
