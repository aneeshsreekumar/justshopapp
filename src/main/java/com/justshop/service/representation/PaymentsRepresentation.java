package com.justshop.service.representation;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "payments")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class PaymentsRepresentation extends AbstractRepresentation {
	
	private Integer customerId;
	
	
	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	private List<PaymentRepresentation> payment;

	public List<PaymentRepresentation> getPayment() {
		return payment;
	}

	public void setPayment(List<PaymentRepresentation> payment) {
		this.payment = payment;
	}

	public PaymentsRepresentation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PaymentsRepresentation(List<PaymentRepresentation> payment) {
		super();
		this.payment = payment;
	}

	@Override
	public void createLinks() {
		// TODO Auto-generated method stub
		links = new ArrayList<LinkRepresentation>();
		LinkRepresentation link = new LinkRepresentation(LinkRepresentationTypes.CREATE_ORDER);
		links.add(link);
		LinkRepresentation link2 = new LinkRepresentation(LinkRepresentationTypes.CREATE_PAYMENT);
		links.add(link2);		
	}
	
	
	
	
	
	

}
