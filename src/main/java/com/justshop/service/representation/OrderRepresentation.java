package com.justshop.service.representation;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement(name = "Order")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class OrderRepresentation extends AbstractRepresentation{
	
	
	private Integer orderID;
	private Boolean isCancelled;
	private Double totalPaid;
	private Integer customerID;
	private Integer paymentDetailID;
	private List<OrderItemRepresentation> orderItems = new ArrayList<OrderItemRepresentation>();
	public Integer getOrderID() {
		return orderID;
	}
	public void setOrderID(Integer orderID) {
		this.orderID = orderID;
	}
	public Boolean getIsCancelled() {
		return isCancelled;
	}
	public void setIsCancelled(Boolean isCancelled) {
		this.isCancelled = isCancelled;
	}
	public Double getTotalPaid() {
		return totalPaid;
	}
	public void setTotalPaid(Double totalPaid) {
		this.totalPaid = totalPaid;
	}
	public Integer getCustomerID() {
		return customerID;
	}
	public void setCustomerID(Integer customerID) {
		this.customerID = customerID;
	}
	public Integer getPaymentDetailID() {
		return paymentDetailID;
	}
	public void setPaymentDetailID(Integer paymentDetailID) {
		this.paymentDetailID = paymentDetailID;
	}
	public List<OrderItemRepresentation> getOrderItems() {
		return orderItems;
	}
	public void setOrderItems(List<OrderItemRepresentation> orderItems) {
		this.orderItems = orderItems;
	}
	public OrderRepresentation(Integer orderID, Boolean isCancelled, Double totalPaid, Integer customerID,
			Integer paymentDetailID, List<OrderItemRepresentation> orderItems) {
		super();
		this.orderID = orderID;
		this.isCancelled = isCancelled;
		this.totalPaid = totalPaid;
		this.customerID = customerID;
		this.paymentDetailID = paymentDetailID;
		this.orderItems = orderItems;
	}
	public OrderRepresentation() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "OrderRepresentation [orderID=" + orderID + ", isCancelled=" + isCancelled + ", totalPaid=" + totalPaid
				+ ", customerID=" + customerID + ", paymentDetailID=" + paymentDetailID + ", orderItems=" + orderItems
				+ "]";
	}
	@Override
	public void createLinks() {
		// TODO Auto-generated method stub
		links = new ArrayList<LinkRepresentation>();
		LinkRepresentation link = new LinkRepresentation(LinkRepresentationTypes.SELF_ORDER,getOrderID().toString());
		links.add(link);
		if(isCancelled == null || !isCancelled){// if its not cancelled add cancel link
			link = new LinkRepresentation(LinkRepresentationTypes.CANCEL_ORDER,getOrderID().toString());
			links.add(link);
		}
		
		
	}
	
	public void createCancelLinks(){
		LinkRepresentation allOrders = new LinkRepresentation(LinkRepresentationTypes.GET_ALL_ORDERS,customerID.toString());
		links.add(allOrders);
	}

	
	
	
}
