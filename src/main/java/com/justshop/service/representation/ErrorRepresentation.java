package com.justshop.service.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "error")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class ErrorRepresentation {
	private String errorMessage;
	private Integer errorCode;
	
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public Integer getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	public ErrorRepresentation() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ErrorRepresentation(String errorMessage, Integer errorCode) {
		// TODO Auto-generated constructor stub
		this.errorMessage = errorMessage;
		this.errorCode = errorCode;
	}
	
	
	
}
