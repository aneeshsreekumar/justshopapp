package com.justshop.service.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.justshop.util.Utility;


@XmlAccessorType(XmlAccessType.FIELD)
public class LinkRepresentation {
	private String rel;
	private String URI;
	private String mediaType;
	private String action;
	public String getRel() {
		return rel;
	}
	public void setRel(String rel) {
		this.rel = rel;
	}
	public String getURI() {
		return URI;
	}
	public void setURI(String uRI) {
		URI = uRI;
	}
	public String getMediaType() {
		return mediaType;
	}
	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public LinkRepresentation() {
		super();
		// TODO Auto-generated constructor stub
	}
	public LinkRepresentation(String rel, String uRI, String mediaType, String action) {
		super();
		this.rel = rel;
		URI = uRI;
		this.mediaType = mediaType;
		this.action = action;
	}
	public LinkRepresentation(LinkRepresentationTypes representationType) {
		// TODO Auto-generated constructor stub
		this.rel = representationType.getRel();
		this.URI = representationType.getURI();
		this.mediaType = representationType.getMediaType();
		this.action = representationType.getAction();
	}
	
	public LinkRepresentation(LinkRepresentationTypes representationType, String... parameter) {
		// TODO Auto-generated constructor stub
		this(representationType);
		if(URI.indexOf("{")>0)
			this.URI = Utility.replaceParameters(URI, parameter);
		else 
			this.URI = Utility.appendParameters(URI, parameter);
	}
	
	
	
	
	
}
