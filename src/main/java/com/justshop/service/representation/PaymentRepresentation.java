package com.justshop.service.representation;

import java.util.ArrayList;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "Payment")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class PaymentRepresentation extends AbstractRepresentation {
	
	private Integer paymentDetailID;
	private String creditCardNumber;
	private String creditCardType;
	private String expiryDate;
	private String nameOnCard;
	private Integer customerID;
	
	public Integer getPaymentDetailID() {
		return paymentDetailID;
	}
	public void setPaymentDetailID(Integer paymentDetailID) {
		this.paymentDetailID = paymentDetailID;
	}
	public String getCreditCardNumber() {
		return creditCardNumber;
	}
	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}
	public String getCreditCardType() {
		return creditCardType;
	}
	public void setCreditCardType(String creditCardType) {
		this.creditCardType = creditCardType;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getNameOnCard() {
		return nameOnCard;
	}
	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}
	public PaymentRepresentation(Integer paymentDetailID, String creditCardNumber, String creditCardType,
			String expiryDate, String nameOnCard, Integer customerID) {
		super();
		this.paymentDetailID = paymentDetailID;
		this.creditCardNumber = creditCardNumber;
		this.creditCardType = creditCardType;
		this.expiryDate = expiryDate;
		this.nameOnCard = nameOnCard;
		this.customerID = customerID;
	}
	
	public PaymentRepresentation() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Integer getCustomerID() {
		return customerID;
	}
	public void setCustomerID(Integer customerID) {
		this.customerID = customerID;
	}
	@Override
	public String toString() {
		return "PaymentRepresentation [paymentDetailID=" + paymentDetailID + ", expiryDate=" + expiryDate
				+ ", nameOnCard=" + nameOnCard + ", customerID=" + customerID + "]";
	}
	
	@Override
	public void createLinks() {
		// TODO Auto-generated method stub
		links = new ArrayList<LinkRepresentation>();
		LinkRepresentation self = new LinkRepresentation(LinkRepresentationTypes.SELF_PAYMENT,customerID.toString(),paymentDetailID.toString());
		links.add(self);	
	}
	
	
	
	

}
