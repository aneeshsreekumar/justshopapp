package com.justshop.service.representation;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "OrderItem")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class OrderItemRepresentation extends AbstractRepresentation{
	
	private Integer orderItemID;
	private Boolean isCancelled;
	private String deliveryType;
	private Integer quantity;
	private Double totalPrice;
	
	private Integer productID;
	private Integer orderID;
	private Integer deliveryRequestID;
	private Integer partnerID;
	public Integer getOrderItemID() {
		return orderItemID;
	}
	public void setOrderItemID(Integer orderItemID) {
		this.orderItemID = orderItemID;
	}
	public Boolean getIsCancelled() {
		return isCancelled;
	}
	public void setIsCancelled(Boolean isCancelled) {
		this.isCancelled = isCancelled;
	}
	public String getDeliveryType() {
		return deliveryType;
	}
	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public Integer getProductID() {
		return productID;
	}
	public void setProductID(Integer productID) {
		this.productID = productID;
	}
	public Integer getOrderID() {
		return orderID;
	}
	public void setOrderID(Integer orderID) {
		this.orderID = orderID;
	}
	public Integer getDeliveryRequestID() {
		return deliveryRequestID;
	}
	public void setDeliveryRequestID(Integer deliveryRequestID) {
		this.deliveryRequestID = deliveryRequestID;
	}
	public Integer getPartnerID() {
		return partnerID;
	}
	public void setPartnerID(Integer partnerID) {
		this.partnerID = partnerID;
	}
	public OrderItemRepresentation() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public void createLinks() {
		// TODO Auto-generated method stub
		links = new ArrayList<LinkRepresentation>();
		LinkRepresentation self = new LinkRepresentation(LinkRepresentationTypes.GET_PRODUCT,productID.toString());
		links.add(self);
		
	}
	
	
	

}
