package com.justshop.service.representation;

public class CustomerHeaderRepresentation extends HeaderRepresentation{
	
	
	private CustomerMiniRepresentation customer;
	
	public CustomerMiniRepresentation getCustomer() {
		return customer;
	}


	public void setCustomer(CustomerMiniRepresentation customer) {
		this.customer = customer;
	}	
		

}
