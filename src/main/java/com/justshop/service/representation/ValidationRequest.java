package com.justshop.service.representation;

import javax.ws.rs.core.MultivaluedMap;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class ValidationRequest {
	
	
	private String email;
	private String password;
	private String roleType;
	
	
	

	public ValidationRequest() {}
	
	public ValidationRequest(String email2, String password,String roleType) {
		
		
		email = email2;
		this.password = password;
		this.roleType = roleType;
		// TODO Auto-generated constructor stub
	}

	public ValidationRequest(MultivaluedMap<String, String> formMap) {
		// TODO Auto-generated constructor stub
		
		
		email = formMap.getFirst("email");
		password = formMap.getFirst("password");
		roleType = formMap.getFirst("roleType");
		
	}

	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "CustomerValidationRequest [email=" + email + ", password= ]" + password + ":roleType:"+roleType;
	}

	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	

	
	

}
