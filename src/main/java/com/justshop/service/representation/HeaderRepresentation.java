package com.justshop.service.representation;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "header")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class HeaderRepresentation extends AbstractRepresentation{
	
	public HeaderRepresentation() {}

	public void createLinks(){
		
		links = new ArrayList<LinkRepresentation>();
		LinkRepresentation search = new LinkRepresentation(LinkRepresentationTypes.SEARCH_PRODUCT);
		links.add(search);
		
	}

	
	
}
