package com.justshop.service.representation;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="CartItem")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")

public class CartItemMiniRepresentation extends AbstractRepresentation {

	
	private Integer customerID;
	
	private Integer quantity;
	private String deliveryType;
	private Boolean itemNoLongerAvailable;
	
	private ProductRepresentation product;
	
	
	public ProductRepresentation getProduct() {
		return product;
	}
	public void setProduct(ProductRepresentation product) {
		this.product = product;
	}
	public Integer getCustomerID() {
		return customerID;
	}
	public void setCustomerID(Integer customerID) {
		this.customerID = customerID;
	}
	
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getDeliveryType() {
		return deliveryType;
	}
	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}
	public Boolean getItemNoLongerAvailable() {
		return itemNoLongerAvailable;
	}
	public void setItemNoLongerAvailable(Boolean itemNoLongerAvailable) {
		this.itemNoLongerAvailable = itemNoLongerAvailable;
	}
	
	public CartItemMiniRepresentation(Integer customerID, ProductRepresentation product, Integer quantity,
			String deliveryType, Boolean itemNoLongerAvailable) {
		super();
		
		this.customerID = customerID;
		this.product = product;
		this.quantity = quantity;
		this.deliveryType = deliveryType;
		this.itemNoLongerAvailable = itemNoLongerAvailable;
	}
	public CartItemMiniRepresentation() {
		// TODO Auto-generated constructor stub
	}
	
	public void createLinks(){
		links = new ArrayList<LinkRepresentation>();
		LinkRepresentation link = new LinkRepresentation(LinkRepresentationTypes.UPDATE_CART_ITEM);
		links.add(link);
	}
	
	
}
