package com.justshop.service.representation;

import javax.ws.rs.core.MultivaluedMap;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class CustomerRequest {
	
	private String firstName;
	private String lastName;
	private String email;
	private String phoneNumber;
	private String password;
	
	
	

	public CustomerRequest() {}
	
	public CustomerRequest(String firstName2, String lastName2, String email2, String phoneNumber2,String passw) {
		
		firstName = firstName2;
		lastName = lastName2;
		email = email2;
		phoneNumber = phoneNumber2;
		password = passw;
		// TODO Auto-generated constructor stub
	}

	public CustomerRequest(MultivaluedMap<String, String> formMap) {
		// TODO Auto-generated constructor stub
		
		firstName = formMap.getFirst("firstName");
		lastName = formMap.getFirst("lastName");
		email = formMap.getFirst("email");
		password = formMap.getFirst("password");
		phoneNumber = formMap.getFirst("phoneNumber");
		
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return "CustomerRequest [firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", phoneNumber=" + phoneNumber + "]";
	}
	
	

}
