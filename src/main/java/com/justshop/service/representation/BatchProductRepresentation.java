package com.justshop.service.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "Product")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class BatchProductRepresentation extends ProductRepresentation{
	
	private Boolean isSuccess;
	private ErrorRepresentation error;

	public BatchProductRepresentation() {}

	
	public Boolean getIsSuccess() {
		return isSuccess;
	}



	public void setIsSuccess(Boolean isSuccess) {
		this.isSuccess = isSuccess;
	}



	public ErrorRepresentation getError() {
		return error;
	}

	public void setError(ErrorRepresentation error) {
		this.error = error;
	}
	

	
}
