package com.justshop.service.representation;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "Partner")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class PartnerRepresentation extends AbstractRepresentation{
	
	private Integer partnerId;
	private String name;
	private String address;
	private String city;
	private String country;
	private String zip;
	private String state;
	private String email;
	private String password;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public PartnerRepresentation() {}
	
	public Integer getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(Integer partnerId) {
		this.partnerId = partnerId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}

	@Override
	public void createLinks() {
		// TODO Auto-generated method stub
		links = new ArrayList<LinkRepresentation>();
		LinkRepresentation link = new LinkRepresentation(LinkRepresentationTypes.SELF_PARTNER,partnerId.toString());
		links.add(link);
		link = new LinkRepresentation(LinkRepresentationTypes.SEARCH_PRODUCT);
		links.add(link);
		link = new LinkRepresentation(LinkRepresentationTypes.ADD_PARTNER_PROD,partnerId.toString());
		links.add(link);
		link = new LinkRepresentation(LinkRepresentationTypes.ADD_PARTNER_BATCH_PROD,partnerId.toString());
		links.add(link);
	}
	
}
