package com.justshop.service.representation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.justshop.util.Constants;

public class PaymentRequest {
	
	private String nameOnCard;
	private String creditCardNumber;
	private String creditCardType;
	private Date expiryDate;
	private Integer customerId;
	
	public String getNameOnCard() {
		return nameOnCard;
	}

	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public String getCreditCardType() {
		return creditCardType;
	}

	public void setCreditCardType(String creditCardType) {
		this.creditCardType = creditCardType;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	

	public PaymentRequest(String nameOnCard, String creditCardNumber, String creditCardType, String expiryDate2,
			Integer customerId) {
		// TODO Auto-generated constructor stub
		this.nameOnCard = nameOnCard;
		this.creditCardNumber = creditCardNumber;
		this.creditCardType = creditCardType;
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_PATTERN);
		
		try {
			this.expiryDate = sdf.parse(expiryDate2);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.customerId = customerId;
	}

	public PaymentRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
