package com.justshop.service.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class OrderRequest {
	
	private Integer customerID;
	private Integer paymentDetailID;
	public Integer getCustomerID() {
		return customerID;
	}
	public void setCustomerID(Integer customerID) {
		this.customerID = customerID;
	}
	public Integer getPaymentDetailID() {
		return paymentDetailID;
	}
	public void setPaymentDetailID(Integer paymentDetailID) {
		this.paymentDetailID = paymentDetailID;
	}
	public OrderRequest(Integer customerID, Integer paymentDetailID) {
		super();
		this.customerID = customerID;
		this.paymentDetailID = paymentDetailID;
	}
	public OrderRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "OrderRequest [customerID=" + customerID + ", paymentDetailID=" + paymentDetailID + "]";
	}
	
	
	

}
