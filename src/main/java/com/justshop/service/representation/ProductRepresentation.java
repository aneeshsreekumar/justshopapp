package com.justshop.service.representation;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "Product")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class ProductRepresentation extends AbstractRepresentation{
	
	private Integer productID;
	private Integer availableQuantity;
	private String status;
	private Double unitPrice;
	private String description;
	private String name;
	private String keywords;
	private String image;
	
	
	




	public ProductRepresentation() {}

	

	

	public Integer getAvailableQuantity() {
		return availableQuantity;
	}

	public void setAvailableQuantity(Integer availableQuantity) {
		this.availableQuantity = availableQuantity;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}


	public Integer getProductID() {
		return productID;
	}


	public void setProductID(Integer productID) {
		this.productID = productID;
	}



	public String getImage() {
		return image;
	}





	public void setImage(String image) {
		this.image = image;
	}




	@Override
	public void createLinks() {
		links = new ArrayList<LinkRepresentation>();
		LinkRepresentation self = new LinkRepresentation(LinkRepresentationTypes.SELF_PRODUCT,productID.toString());
		links.add(self);
		
	}

	
}
