package com.justshop.service.representation;

import javax.ws.rs.core.MultivaluedMap;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class PartnerRequest {

	private String name;
	private String address;
	private String city;
	private String country;
	private String zip;
	private String state;
	private String email;
	private String password;
		
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public PartnerRequest(MultivaluedMap<String, String> formMap) {
		// TODO Auto-generated constructor stub
		name = formMap.getFirst("name");
		address = formMap.getFirst("address");
		city = formMap.getFirst("city");
		country = formMap.getFirst("country");
		zip = formMap.getFirst("zip");
		state = formMap.getFirst("state");
		email = formMap.getFirst("email");
		password = formMap.getFirst("password");
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	@Override
	public String toString() {
		return "PartnerRequest [name=" + name + ", address=" + address + ", city=" + city + ", country=" + country
				+ ", zip=" + zip + ", state=" + state + "]";
	}
	public PartnerRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
}
