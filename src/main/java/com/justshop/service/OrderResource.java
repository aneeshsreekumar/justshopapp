package com.justshop.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;

import com.justshop.business.exception.BusinessException;
import com.justshop.service.representation.OrderRepresentation;
import com.justshop.service.representation.OrderRequest;
import com.justshop.service.workflow.OrderActivity;
import com.justshop.util.Constants;

@CrossOriginResourceSharing(
        allowAllOrigins = true
)

@Path(Constants.BASE_ORDER_URL)
public class OrderResource implements OrderService{

	@GET
	@Produces({"application/xml" ,"application/json"})
	@Path(Constants.ORDER_ID_PARAMETER)
	@Override
	public Response getOrder(@PathParam(Constants.ORDER_ID) Integer orderID) throws BusinessException {
		// TODO Auto-generated method stub
		System.out.println("getOrder:" + orderID);
		OrderActivity orderActivity = new OrderActivity();
		OrderRepresentation representation = orderActivity.getOrder(orderID);		
		return Response.ok(representation).build();
	}

	
	@GET
	@Produces({"application/xml" ,"application/json"})
	@Path(Constants.GET_ALL_ORDERS_URL)
	@Override
	public Response getOrders(@PathParam(Constants.CUSTOMER_ID) Integer customerID) throws BusinessException {
		// TODO Auto-generated method stub
		System.out.println("getAllOrder:" + customerID);
		OrderActivity orderActivity = new OrderActivity();
		List<OrderRepresentation> representations = orderActivity.getOrders(customerID);
		System.out.println("getAllOrder:" + representations);
		GenericEntity<List<OrderRepresentation>> entity = new GenericEntity<List<OrderRepresentation>>(representations){};
		return Response.ok(entity).build();
	}

	@POST
	@Produces({"application/xml" , "application/json"})
	@Consumes({"application/xml" , "application/json"})
	@Path("/create")
	@Override
	public OrderRepresentation createOrder(OrderRequest  orderRequest) throws BusinessException {
		System.out.println("POST METHOD Request from Client with ............." + orderRequest);
		OrderActivity orderActivity = new OrderActivity();
		return orderActivity.createOrder(orderRequest);
	}
	
	
	@DELETE
	@Produces({"application/xml" ,"application/json"})
	@Path(Constants.CANCEL_ORDER_URL)
	@Override
	public Response cancelOrder(@PathParam(Constants.ORDER_ID) Integer orderID) throws BusinessException{
		// TODO Auto-generated method stub
		System.out.println("getOrder:" + orderID);
		OrderActivity orderActivity = new OrderActivity();
		OrderRepresentation representation = orderActivity.cancelOrder(orderID);
		return Response.ok(representation).build();
	}
	
	

}
