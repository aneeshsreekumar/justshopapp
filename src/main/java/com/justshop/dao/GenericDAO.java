package com.justshop.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;

import com.justshop.model.PersistenceManager;

public abstract class GenericDAO<T, ID extends Serializable> implements IGenericDAO<T, ID> {

	private Class<T> entityClass;
	
	public Class<T> getEntityClass() {
		return entityClass;
	}
	

	public EntityManager getEntityManager() {
		
			EntityManager entityManager = PersistenceManager.INSTANCE.getEntityManager();
		return entityManager;
	}
	
	

	public GenericDAO() {
		super();
		this.entityClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
		
	}
	
	
	/**
	 * Call this in finally
	 */
	public void closeEntityManager(EntityManager entityManager){

		if(entityManager!=null && entityManager.isOpen()){
			entityManager.close();
			
		}
		entityManager = null;
		
	}
	
	public T getByID(ID id, EntityManager entityManager, boolean lock){
		T entity = null;
		if(lock)
			entity = (T) entityManager.find(entityClass, id, LockModeType.PESSIMISTIC_WRITE);
		else
			entity = (T) entityManager.find(entityClass, id);
		
		return entity;
	}
	
	
	
	
	public List<T> findAll(){
		return null;
		
	}
	public List<T> findAllMatching(T obj, String[] excludeProps){
		return null;
		
	}
	public T persist(T entity, EntityManager entityManager){
		
		entityManager.persist(entity);
		
		return entity;
		
	}
	public void makeTransient(T entity){
		
	}
	public void flush(){
		
	}
	public void clear(){
		
	}
	
	 

}
