package com.justshop.dao;

import javax.persistence.EntityManager;

import com.justshop.model.CartItem;

public class CartItemDAO extends GenericDAO<CartItem, Integer>{

	
	public CartItem getCartItem(Integer id){
		
		// TODO inserted by ANEESH on [Sep 22, 2015, 2:23:23 PM]: CheckThis
		// don't delete since entity manager has to be obtained and stored
		EntityManager em = getEntityManager();
		CartItem cust = null;
		try {
			if (id != null) {
				cust = getByID(id, em,false);

			}
		} catch (Exception e) {
			e.printStackTrace();
			// em.getTransaction().rollback();

		} finally {

			closeEntityManager(em);

		}
		return cust;

	}
	
	public CartItem insertCartItem(CartItem cartItem){
		EntityManager em = getEntityManager();
		CartItem returnedCart=null;
		if(cartItem !=null){
			try{
			
				em.getTransaction().begin();
				
				 returnedCart = persist(cartItem,em);
				 //System.out.println("cartItem after persisting ininsertcartitem:"+cartItem);
				
				em.getTransaction().commit();
			}catch(Exception e){
				e.printStackTrace();
				em.getTransaction().rollback();
				
			}finally{
				closeEntityManager(em);
			}
			
			
			
		}
		return returnedCart;
	}
}
