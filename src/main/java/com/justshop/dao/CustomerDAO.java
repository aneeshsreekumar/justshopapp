package com.justshop.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.justshop.business.exception.BusinessErrorTypes;
import com.justshop.business.exception.BusinessException;
import com.justshop.model.CartItem;
import com.justshop.model.Customer;
import com.justshop.model.PaymentDetails;
import com.justshop.model.Product;
import com.justshop.vo.ResultListVO;
import com.justshop.vo.ResultVO;

public class CustomerDAO extends GenericDAO<Customer, Integer> {

	public Customer getByID(Integer id, boolean lock) throws BusinessException {

		// TODO inserted by ANEESH on [Sep 22, 2015, 2:23:23 PM]: CheckThis
		// don't delete since entity manager has to be obtained and stored
		EntityManager em = getEntityManager();
		Customer cust = null;
		try {
			if (id != null) {
				cust = getByID(id, em, lock);

			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException(BusinessErrorTypes.ERROR);

		} finally {

			closeEntityManager(em);

		}
		return cust;

	}

	public Customer insertCustomer(Customer customer) throws BusinessException {
		EntityManager em = getEntityManager();

		if (customer != null) {
			try {

				em.getTransaction().begin();
				persist(customer, em);

				em.getTransaction().commit();
			} catch (Exception e) {
				e.printStackTrace();
				if (em != null && em.isOpen() && em.getTransaction().isActive())
					em.getTransaction().rollback();
				throw new BusinessException(BusinessErrorTypes.ERROR);

			} finally {
				closeEntityManager(em);
			}

		}
		return customer;

	}

	public CartItem addProductToCart(CartItem cartItem) throws BusinessException {
		EntityManager em = getEntityManager();
		CartItem newItem = null;
		try {

			em.getTransaction().begin();
			Customer customer = getByID(cartItem.getCustomer().getCustomerId(), em, false);
			Product product = em.find(Product.class, cartItem.getProduct().getProductId());
			if (customer != null && product != null) {
				System.out.println("before get shopping cart");
				List<CartItem> items = customer.getShoppingCartItems();
				System.out.println("after get shopping cart");
				cartItem.setCustomer(customer);
				cartItem.setProduct(product);
				cartItem.setItemNoLongerAvailable(false);
				if (!items.contains(cartItem)) {
					System.out.println("inside add new item");
					System.out.println("before associate cart item");
					cartItem.associateCartItem();
					System.out.println("after associate cart item");
					em.persist(cartItem);

				} else {
					System.out.println("inside update existing qty item");
					for (CartItem present : items) {
						if (present.equals(cartItem)) {
							present.setQuantity((present.getQuantity() + cartItem.getQuantity()));
							cartItem = present;
						}
					}
				}

				persist(customer, em);
				em.getTransaction().commit();
				newItem = cartItem;
			}

		} catch (Exception e) {
			e.printStackTrace();
			if (em != null && em.isOpen() && em.getTransaction().isActive())
				em.getTransaction().rollback();
			throw new BusinessException(BusinessErrorTypes.ERROR);

		} finally {
			closeEntityManager(em);
		}
		return newItem;

	}

	public Boolean removeProductFromCart(CartItem cartItem) throws BusinessException {
		EntityManager em = getEntityManager();

		boolean removed = false;
		try {

			em.getTransaction().begin();
			System.out.println("cartItempk:" + cartItem.getCartItemId());
			CartItem cartItemDB = em.find(CartItem.class, cartItem.getCartItemId());
			System.out.println("cartItem after find:" + cartItem);
			if(cartItemDB == null)
				throw new BusinessException(BusinessErrorTypes.NO_CART_ITEM_FOUND);
			cartItemDB.getCustomer().removeShoppingCartItem(cartItemDB);
			// cartItemDB.getProduct().removeShoppingCartItem(cartItemDB);
			em.remove(cartItemDB);
			em.getTransaction().commit();
			removed = true;
		} catch (Exception e) {
			e.printStackTrace();
			if (em != null && em.isOpen() && em.getTransaction().isActive())
				em.getTransaction().rollback();
			throw new BusinessException(BusinessErrorTypes.ERROR);

		} finally {
			closeEntityManager(em);
		}
		return removed;

	}

	public List<CartItem> fetchCart(Customer customer) throws BusinessException {
		List<CartItem> items = null;
		EntityManager em = getEntityManager();
		if (customer != null) {
			try {

				em.getTransaction().begin();

				Customer customerFetched = getByID(customer.getCustomerId(), em, false);
				items = customerFetched.getShoppingCartItems();

				em.getTransaction().commit();
			} catch (Exception e) {
				e.printStackTrace();
				if (em != null && em.isOpen() && em.getTransaction().isActive())
					em.getTransaction().rollback();
				throw new BusinessException(BusinessErrorTypes.ERROR);

			} finally {
				closeEntityManager(em);
			}

		}
		return items;

	}

	public List<Customer> searchCustomersByName(String customerName) throws BusinessException {
		// TODO Auto-generated method stub
		List<Customer> customers = null;

		if (customerName != null && customerName.length() > 0) {
			EntityManager em = getEntityManager();

			try {

				em.getTransaction().begin();
				TypedQuery<Customer> query = em.createNamedQuery("Customer.findCustomersByName", Customer.class);
				query.setParameter("customerName", customerName);
				customers = query.getResultList();

				em.getTransaction().commit();
			} catch (Exception e) {
				e.printStackTrace();
				if (em != null && em.isOpen() && em.getTransaction().isActive())
					em.getTransaction().rollback();
				throw new BusinessException(BusinessErrorTypes.ERROR);

			} finally {

				closeEntityManager(em);

			}

		} // end of if

		return customers;

	}

	public Double getTotalPrice(Customer customer) throws BusinessException {
		EntityManager em = getEntityManager();
		Double total = 0.0;
		try {
			if (customer != null && customer.getCustomerId() != null) {
				customer = getByID(customer.getCustomerId(), em, false);

			}

			List<CartItem> shoppingCartItems = customer.getShoppingCartItems();
			if (shoppingCartItems != null)
				for (CartItem item : shoppingCartItems) {
					System.out.println("item.getProduct().getUnitPrice():" + item.getProduct().getUnitPrice());
					System.out.println("item.getProduct().getUnitPrice():" + item.getQuantity());
					total += item.getProduct().getUnitPrice() * item.getQuantity();
				}

		} catch (Exception e) {
			e.printStackTrace();
			if (em != null && em.isOpen() && em.getTransaction().isActive())
				em.getTransaction().rollback();
			throw new BusinessException(BusinessErrorTypes.ERROR);

		} finally {

			closeEntityManager(em);

		}
		return total;
	}

	public boolean isExists(Integer id) throws BusinessException {
		// TODO Auto-generated method stub
		return (getByID(id, false) != null);

	}

	public ResultVO<PaymentDetails> getCustomerPayment(Integer customerId, Integer paymentDetailId, EntityManager em, boolean close)
			throws BusinessException {

		ResultVO<PaymentDetails> result = new ResultVO<PaymentDetails>();

		try {
			Customer customer = getByID(customerId, em, false);

			PaymentDetails selectedPayment = null;
			if (customer != null) {

				List<PaymentDetails> paymentDetails = customer.getPaymentDetails();
				if (paymentDetails != null && paymentDetails.size() > 0) {// has
																			// payments
																			// added
					System.out.println("paymentdtls:" + paymentDetails);
					for (PaymentDetails paymentdtl : paymentDetails) {
						if (paymentDetailId.equals(paymentdtl.getPaymentDetailID())) {
							selectedPayment = paymentdtl;
							break;
						}
					}
					if (selectedPayment != null) {

						selectedPayment.setCustomer(customer);
						result.setModel(selectedPayment);
						result.setSuccessFlag(true);
						result.setMessage(BusinessErrorTypes.SUCCESS);

					} else {
						// selected payment not found
						result = new ResultVO<PaymentDetails>(BusinessErrorTypes.SELECTED_PAYMENT_NOT_FOUND, false,
								null);
					}
				} else {
					// no payments add one
					result = new ResultVO<PaymentDetails>(BusinessErrorTypes.NO_PAYMENTS_ADDED, false, null);
				}
			} else {
				// no customer
				result = new ResultVO<PaymentDetails>(BusinessErrorTypes.NO_CUSTOMER_EXISTS, false, null);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = new ResultVO<PaymentDetails>(BusinessErrorTypes.ERROR, false, null);
		}finally {
			if(close)
			closeEntityManager(em);
		}
		return result;
	}

	public boolean removeUnavailableItemsFromCart(Integer customerId, EntityManager em) throws BusinessException {
		// TODO Auto-generated method stub
		boolean removed = false;
		try {
			Query query = em
					.createQuery("delete from CartItem where customer.customerId = ?1 and itemNoLongerAvailable=false");
			query.setParameter(1, customerId);
			int rows = query.executeUpdate();
			System.out.println("rows deleted:" + rows);
			removed = true;
		} catch (Exception e) {
			e.printStackTrace();
			if (em != null && em.isOpen() && em.getTransaction().isActive())
				em.getTransaction().rollback();
			throw new BusinessException(BusinessErrorTypes.ERROR);

		}
		return removed;
	}

	public CartItem updateCartItem(CartItem cartItem) throws BusinessException {
		// TODO Auto-generated method stub
		EntityManager em = getEntityManager();
		CartItem cartItemDB = null;
		try {

			em.getTransaction().begin();
			System.out.println("cartItempk:" + cartItem.getCartItemId());
			cartItemDB = em.find(CartItem.class, cartItem.getCartItemId());
			System.out.println("cartItem after find:" + cartItem);
			if (cartItemDB != null) {
				cartItemDB.setDeliveryType(cartItem.getDeliveryType());
				cartItemDB.setQuantity(cartItem.getQuantity());
				em.persist(cartItemDB);
				em.getTransaction().commit();
			} else {
				throw new BusinessException(BusinessErrorTypes.NO_CART_ITEM_FOUND);
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (!(e instanceof BusinessException)) {
				System.out.println("cameeeeeeeeeeee hereeeeeee" + e.getClass());
				throw new BusinessException(BusinessErrorTypes.CARTITEM_UPDATION_ERROR);
			} else {
				throw (BusinessException) e;
			}

		} finally {
			closeEntityManager(em);
		}
		return cartItemDB;

	}

	public Customer validate(String emailAddress, String password) throws BusinessException {
		// TODO Auto-generated method stub
		Customer customer = null;
		EntityManager em = getEntityManager();
		try {
			TypedQuery<Customer> query = em.createQuery(
					"select c from Customer c where c.emailAddress = ?1 and c.password=?2", Customer.class);
			System.out.println("emailAdd in customerDAO:"+emailAddress);
			System.out.println("password in customerDAO:"+password);
			query.setParameter(1, emailAddress);
			query.setParameter(2, password);
			customer = query.getSingleResult();
			customer.getShoppingCartItems().size();
		} catch (NoResultException nre) {
			nre.printStackTrace();
			throw new BusinessException(BusinessErrorTypes.NOT_VALID_CUSTOMER);
		} catch (Exception e) {
			e.printStackTrace();
			if (em != null && em.isOpen() && em.getTransaction().isActive())
				em.getTransaction().rollback();
			throw new BusinessException(BusinessErrorTypes.ERROR);

		}finally {
			closeEntityManager(em);
		}
		return customer;
	}
	
	public ResultListVO<PaymentDetails> getCustomerPayments(Integer customerId) {
		// TODO Auto-generated method stub
		EntityManager em = getEntityManager();
		ResultListVO<PaymentDetails> result = new ResultListVO<PaymentDetails>(null, false, null);

		try {
			Customer customer = getByID(customerId, em, false);

			
			if (customer != null) {

				List<PaymentDetails> paymentDetails = customer.getPaymentDetails();
				if (paymentDetails != null && paymentDetails.size() > 0) {
					result = new ResultListVO<PaymentDetails>(BusinessErrorTypes.SUCCESS, true, paymentDetails);
				} else {
					// no payments add one
					result = new ResultListVO<PaymentDetails>(BusinessErrorTypes.NO_PAYMENTS_ADDED, false, null);
				}
			} else {
				// no customer
				result = new ResultListVO<PaymentDetails>(BusinessErrorTypes.NO_CUSTOMER_EXISTS, false, null);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = new ResultListVO<PaymentDetails>(BusinessErrorTypes.ERROR, false, null);
		}finally {
			closeEntityManager(em);
		}
		return result;
	}

	public boolean deleteProduct(CartItem item) throws BusinessException {
		// TODO Auto-generated method stub
		EntityManager em = getEntityManager();
		CartItem cartItemDB = null;
		boolean deleted = false;
		try {

			em.getTransaction().begin();
			System.out.println("item.getCartItemId():"+item.getCartItemId());
			cartItemDB = em.find(CartItem.class, item.getCartItemId());
			System.out.println("cartItem after find:" + cartItemDB);
			if (cartItemDB != null) { 
				em.remove(cartItemDB);
				deleted = true;
			} else {
			
				throw new BusinessException(BusinessErrorTypes.NO_CART_ITEM_FOUND);
			}
			
		}finally {
			closeEntityManager(em);
		}
		return deleted;

	}
	
	public CartItem getCartItem(Integer customerId, Integer productId) throws BusinessException {
		EntityManager em = getEntityManager();
		CartItem cartItemDB = null;
		CartItem item = new CartItem(new Product(productId),new Customer(customerId));
		
		try {

			em.getTransaction().begin();
			System.out.println("item.getCartItemId():"+item.getCartItemId());
			cartItemDB = em.find(CartItem.class, item.getCartItemId());
			System.out.println("cartItem after find:" + cartItemDB);
			if (cartItemDB ==  null) { 
				
				throw new BusinessException(BusinessErrorTypes.NO_CART_ITEM_FOUND);
			}
			
		}finally {
			closeEntityManager(em);
		}
		return cartItemDB;
		
	}

}
