package com.justshop.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;

import com.justshop.model.Customer;
import com.justshop.model.Partner;
import com.justshop.model.Product;
import com.justshop.util.Constants;
import com.justshop.vo.ResultVO;

public class ProductDAO extends GenericDAO<Product, Integer>{
	
	
	
		public Product getByID(Integer id, boolean lock){
		
		// TODO inserted by ANEESH on [Sep 22, 2015, 2:23:23 PM]: CheckThis
		// don't delete since entity manager has to be obtained and stored
		EntityManager em = getEntityManager();
		Product cust = null;
		try {
			if (id != null) {
				cust = getByID(id,em,lock);

			}
		} catch (Exception e) {
			e.printStackTrace();
			// em.getTransaction().rollback();

		} finally {

			closeEntityManager(em);

		}
		return cust;

	}
		
	public Product insertProduct(Product product){
		EntityManager em = getEntityManager();
			
				try{
					em.getTransaction().begin();
					
					persist(product,em);
					em.getTransaction().commit();
				}catch(Exception e){
					e.printStackTrace();
					em.getTransaction().rollback();
					
				}finally{
					closeEntityManager(em);
				}
				
		
			return product;
	}

	public List<Product> searchProductsByKeyWord(String keywords) {
		// TODO Auto-generated method stub
		List<Product> products = null;
		
		if(keywords !=null && keywords.length()>0){
		EntityManager em = getEntityManager();
		
		FullTextEntityManager fulltext = Search.getFullTextEntityManager(em);

			try{

				//TODO inserted by ANEESH on [Sep 22, 2015, 1:36:46 PM]: remove
				fulltext.createIndexer().startAndWait();
				//remove end
				em.getTransaction().begin();
				QueryBuilder qb = fulltext.getSearchFactory().buildQueryBuilder().forEntity(Product.class).get();
				org.apache.lucene.search.Query luceneQ = qb.keyword().onFields("keyWords","description").matching(keywords).createQuery();
				
				javax.persistence.Query jpaQuery = fulltext.createFullTextQuery(luceneQ, Product.class);
				products = jpaQuery.getResultList();
				
				em.getTransaction().commit();
			}catch(Exception e){
				e.printStackTrace();
				em.getTransaction().rollback();
				
			}finally{
				if(fulltext !=null){
					fulltext.close();
				}
				closeEntityManager(em);
				
			}
			
			
			
		}// end of if
		
		return products;
	}
	
	
	public List<Product> searchProducts(String keywords) {
		// TODO Auto-generated method stub
		List<Product> products = new ArrayList<Product>();
		
		if(keywords !=null && keywords.length()>0){
			EntityManager em = getEntityManager();
			


				try{

					em.getTransaction().begin();
					TypedQuery<Product> query = em.createNamedQuery("Product.findProductsByKeyword",Product.class);
					query.setParameter("keywordsValue", "%"+keywords+"%");
					products = query.getResultList();
					
					em.getTransaction().commit();
				}catch(Exception e){
					e.printStackTrace();
					em.getTransaction().rollback();
					
				}finally{
					
					closeEntityManager(em);
					
				}
				
				
				
			}// end of if
			
			return products;		
	}

	public List<Product> searchProductsByName(String name) {
		// TODO Auto-generated method stub
		List<Product> products = new ArrayList<Product>();
		
		if(name !=null && name.length()>0){
			EntityManager em = getEntityManager();
			


				try{

					em.getTransaction().begin();
					TypedQuery<Product> query = em.createNamedQuery("Product.findProductsByName",Product.class);
					query.setParameter("nameValue", "%"+name.toLowerCase()+"%");
					products = query.getResultList();
					
					em.getTransaction().commit();
				}catch(Exception e){
					e.printStackTrace();
					em.getTransaction().rollback();
					
				}finally{
					
					closeEntityManager(em);
					
				}
				
				
				
			}// end of if
			
			return products;

		
	}

	
	public void updateProduct(Product product) {
		EntityManager em = getEntityManager();
		
		if(product !=null){
			try{
			
				em.getTransaction().begin();
				
				Product dbProd = getByID(product.getProductId(),false);
				dbProd.setName(product.getName());
				dbProd.setDescription(product.getDescription());
				dbProd.setAvailableQuantity(product.getAvailableQuantity());
				dbProd.setKeyWords(product.getKeyWords());
				dbProd.setStatus(product.getStatus());
				dbProd.setUnitPrice(product.getUnitPrice());
				
				em.merge(dbProd);
				em.getTransaction().commit();
			}catch(Exception e){
				e.printStackTrace();
				em.getTransaction().rollback();
				
			}finally{
				closeEntityManager(em);
			}
			
			
			
		}
		
	}


	public boolean insertProductBatch(Collection<ResultVO<Product>> productVOList) {
		// TODO Auto-generated method stub
		
		EntityManager em = getEntityManager();
		boolean allInserted = false;
		if(productVOList !=null && productVOList.size()>0){
			try{
			
				em.getTransaction().begin();
				int currentIndex = 1;
				Partner partner = null;
				for(ResultVO<Product> productVO: productVOList){
					partner = productVO.getModel().getPartner();
					System.out.println("partner:"+partner);
					productVO.getModel().getPartner().addProduct(productVO.getModel());
					em.persist(productVO.getModel());
					if(currentIndex % Constants.BATCH_FLUSH_SIZE == 0){
						em.flush();
						em.clear();					
					}
					currentIndex++;
				}	
				
				em.getTransaction().commit();
				allInserted=true;
			}catch(Exception e){
				e.printStackTrace();
				em.getTransaction().rollback();
				
			}finally{
				closeEntityManager(em);
			}
			
		
			
		}
		
		return allInserted;
	}

	public boolean isExists(Integer id) {
		// TODO Auto-generated method stub
		return (getByID(id,false)!= null);
		
	}
}
