package com.justshop.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.justshop.business.exception.BusinessErrorTypes;
import com.justshop.business.exception.BusinessException;
import com.justshop.model.OrderItem;
import com.justshop.model.Partner;

public class PartnerDAO extends GenericDAO<Partner, Integer>{
	
	
	
		public Partner getByID(Integer id, boolean lock) throws BusinessException{
		
		// TODO inserted by ANEESH on [Sep 22, 2015, 2:23:23 PM]: CheckThis
		// don't delete since entity manager has to be obtained and stored
		EntityManager em = getEntityManager();
		Partner cust = null;
		try {
			if (id != null) {
				cust = getByID(id,em,lock);

			}
		} catch (Exception e) {
			e.printStackTrace();
			// em.getTransaction().rollback();
			throw new BusinessException(BusinessErrorTypes.ERROR);
		} finally {

			closeEntityManager(em);

		}
		return cust;

	}
	
	public Partner insertPartner(Partner partner) throws BusinessException{
		EntityManager em = getEntityManager();
		if(partner !=null){
			try{
			
				em.getTransaction().begin();
				
				persist(partner,em);
				
				em.getTransaction().commit();
			}catch(Exception e){
				e.printStackTrace();
				if(em!=null && em.isOpen() && em.getTransaction().isActive())
					em.getTransaction().rollback();
				throw new BusinessException(BusinessErrorTypes.PARTNER_CREATION_ERROR);
				
			}finally{
				closeEntityManager(em);
			}
			
			
			
		}
		return partner;
		
	}
	
	
	
	public List<Partner> getPartnersByName(String name) throws BusinessException {
		// TODO Auto-generated method stub
		List<Partner> partners = null;
		if(name !=null && name.length()>0){
			EntityManager em = getEntityManager();
			


				try{

					em.getTransaction().begin();
					TypedQuery<Partner> query = em.createNamedQuery("Partner.findPartnersByName",Partner.class);
					query.setParameter("nameValue", "%"+name.toLowerCase()+"%");
					partners = query.getResultList();
					
					em.getTransaction().commit();
				}catch(Exception e){
					e.printStackTrace();
					if(em!=null && em.isOpen() && em.getTransaction().isActive())
						em.getTransaction().rollback();
					throw new BusinessException(BusinessErrorTypes.ERROR);
					
				}finally{
					
					closeEntityManager(em);
					
				}
				
				
				
			}// end of if
			
				
		return partners;
	}

	
	public List<OrderItem> getOrderRequests(Partner partner) throws BusinessException {
		EntityManager em = getEntityManager();
		List<OrderItem> order = null;
		try {
			partner = getByID(partner.getPartnerId(),em, false);
			if (partner != null && partner.getPartnerId() != null) {
				order = partner.getOrderItems();
				int size = partner.getOrderItems().size();//for fetching order items as well
			}
		} catch (Exception e) {
			e.printStackTrace();
			 throw new BusinessException(BusinessErrorTypes.ERROR);

		} finally {

			closeEntityManager(em);

		}
		return order;

	}

	
	public boolean isExists(Integer id) throws BusinessException{
		// TODO Auto-generated method stub
		return (getByID(id,false)!= null);
		
	}

	public Partner updatePartner(Partner model) throws BusinessException{
		// TODO Auto-generated method stub
		EntityManager em = getEntityManager();
		Partner newModel = null;
		try{
		
			em.getTransaction().begin();
			System.out.println("partnerpk:"+model.getPartnerId());
			
			System.out.println("model after find:"+model);
			newModel = em.merge(model);
			em.getTransaction().commit();
						
		}catch(Exception e){
			e.printStackTrace();
			throw new BusinessException(BusinessErrorTypes.PARTNER_CREATION_ERROR);			
		}finally{
			closeEntityManager(em);
		}
		return newModel;
	}

	public Partner validate(String email, String password) throws BusinessException {
		// TODO Auto-generated method stub
		Partner customer = null;
		EntityManager em = getEntityManager();
		try {
			TypedQuery<Partner> query = em.createQuery(
					"select c from Partner c where c.email = ?1 and c.password=?2", Partner.class);
			query.setParameter(1, email);
			query.setParameter(2, password);
			customer = query.getSingleResult();
			
		} catch (NoResultException nre) {
			nre.printStackTrace();
			throw new BusinessException(BusinessErrorTypes.NOT_VALID_PARTNER);
		} catch (Exception e) {
			e.printStackTrace();
			if (em != null && em.isOpen() && em.getTransaction().isActive())
				em.getTransaction().rollback();
			throw new BusinessException(BusinessErrorTypes.ERROR);

		}finally {
			closeEntityManager(em);
		}
		return customer;
	}
	
}
