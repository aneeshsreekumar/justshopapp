package com.justshop.dao;

import java.util.List;

import javax.persistence.EntityManager;

public interface IGenericDAO<T,ID> {
	
	public T getByID(ID id, EntityManager em, boolean lock);
	public List<T> findAll();
	public List<T> findAllMatching(T obj, String[] excludeProps);
	public T persist(T entity,EntityManager em);
	public void makeTransient(T entity);
	public void flush();
	public void clear();
	
	
	

}
