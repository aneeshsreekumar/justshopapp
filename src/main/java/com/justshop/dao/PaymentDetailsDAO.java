package com.justshop.dao;

import javax.persistence.EntityManager;

import com.justshop.business.exception.BusinessErrorTypes;
import com.justshop.business.exception.BusinessException;
import com.justshop.model.Customer;
import com.justshop.model.PaymentDetails;

public class PaymentDetailsDAO extends GenericDAO<PaymentDetails, Integer> {

	public PaymentDetails getByID(Integer id, boolean lock) throws BusinessException {

		// TODO inserted by ANEESH on [Sep 22, 2015, 2:23:23 PM]: CheckThis
		// don't delete since entity manager has to be obtained and stored
		EntityManager em = getEntityManager();
		PaymentDetails cust = null;
		try {
			if (id != null) {
				cust = getByID(id, em, lock);

			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException(BusinessErrorTypes.ERROR);

		} finally {

			closeEntityManager(em);

		}
		return cust;
	}

	public PaymentDetails insertPaymentDetails(PaymentDetails paymentDetails) throws BusinessException {
		EntityManager em = getEntityManager();
		if (paymentDetails != null) {
			try {

				em.getTransaction().begin();
				CustomerDAO customerDAO = new CustomerDAO();
				Customer customer = customerDAO.getByID(paymentDetails.getCustomer().getCustomerId(), em, false);
				if(customer !=null){
					paymentDetails.setCustomer(customer);
					persist(paymentDetails,em);	
				}else{
					throw new BusinessException(BusinessErrorTypes.NO_CUSTOMER_EXISTS);
				}		
				em.getTransaction().commit();
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("BusinessException)e).getErrorType():"+((BusinessException)e).getErrorType());
				if(em!=null && em.isOpen() && em.getTransaction().isActive())
					em.getTransaction().rollback();
				if(!(e instanceof BusinessException)){
					System.out.println("cameeeeeeeeeeee hereeeeeee"+e.getClass());
					throw new BusinessException(BusinessErrorTypes.PAYMENT_CREATION_ERROR);
				}else{
					throw (BusinessException)e;
				}	

			} finally {
				closeEntityManager(em);
			}

		}
		return paymentDetails;

	}
	
	public boolean isExists(Integer id) throws BusinessException {
		// TODO Auto-generated method stub
		return (getByID(id,false)!= null);
		
	}
	
	
}
