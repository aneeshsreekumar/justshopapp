package com.justshop.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

import com.justshop.business.exception.BusinessErrorTypes;
import com.justshop.business.exception.BusinessException;
import com.justshop.model.CartItem;
import com.justshop.model.Customer;
import com.justshop.model.DeliveryRequest;
import com.justshop.model.Order;
import com.justshop.model.OrderItem;
import com.justshop.model.PaymentDetails;
import com.justshop.model.Product;
import com.justshop.model.TrackingStatus;
import com.justshop.vo.ResultVO;

public class OrderDAO extends GenericDAO<Order, Integer> {

		public Order getByID(Integer id, boolean lock) throws BusinessException{
		
		// TODO inserted by ANEESH on [Sep 22, 2015, 2:23:23 PM]: CheckThis
		// don't delete since entity manager has to be obtained and stored
		EntityManager em = getEntityManager();
		Order cust = null;
		try {
			if (id != null) {
				cust = getByID(id,em,lock);

			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException(BusinessErrorTypes.ERROR);

		} finally {

			closeEntityManager(em);

		}
		return cust;
	}
		
		
		public Order getDetailsByID(Integer id, boolean lock) throws BusinessException{
			
			// TODO inserted by ANEESH on [Sep 22, 2015, 2:23:23 PM]: CheckThis
			// don't delete since entity manager has to be obtained and stored
			EntityManager em = getEntityManager();
			Order order = null;
			try {
				order = getByID(id,em,lock);
				if(order!=null && order.getOrderItems()!=null){
					order.getOrderItems().size();// fetching detail
					order.getPaymentDetail();// fetching payment
				}
					//em.detach(order);
				
			} catch (Exception e) {
				e.printStackTrace();
				throw new BusinessException(BusinessErrorTypes.ERROR);

			} finally {

				closeEntityManager(em);

			}
			return order;
		}
	
	public ResultVO<Order> insertOrder(Integer customerId, Integer paymentDetailId) throws BusinessException {

		EntityManager em = getEntityManager();
		
		Order order = null;
		ResultVO<Order> result = new ResultVO<Order>(BusinessErrorTypes.ERROR, false, null);
		CustomerDAO customerDao = new CustomerDAO();
		ResultVO<PaymentDetails> paymentDetailResult = customerDao.getCustomerPayment(customerId, paymentDetailId,em,false);
		System.out.println("result after validate"+paymentDetailResult);
		if (paymentDetailResult!=null && paymentDetailResult.isSuccessFlag()) {
		EntityTransaction parentTran = em.getTransaction();
		try {
				parentTran.begin();
				// assume valid customer and payment since validated
				
				Double totaPaid = customerDao.getTotalPrice(paymentDetailResult.getModel().getCustomer());
				order = new Order(totaPaid, paymentDetailResult.getModel().getCustomer(), paymentDetailResult.getModel());
				result.setModel(order);
				System.out.println("after commiting order: " + order);
				List<CartItem> items = order.getCustomer().getShoppingCartItems();
				System.out.println("items:" + items);

				OrderItem orderItem = null;
				DeliveryRequest delivery = null;
				if (items != null && items.size() > 0) {
					System.out.println("inside order items if" + items.size());
					int availableQty = 0;
					for (CartItem item : items) {
						System.out.println("inside order items iteration" + item);
						// availableQty
						availableQty = item.getProduct().getAvailableQuantity();
						System.out.println("availableQty"+availableQty);
						if(item.getQuantity()<=availableQty){// item in cart is still available
							orderItem = new OrderItem(item.getProduct(), order, item.getQuantity(),item.getProduct().getUnitPrice(), item.getDeliveryType(),
									item.getProduct().getPartner());
							System.out.println("orderItem being inserted:"+orderItem);
							delivery = new DeliveryRequest(item.getDeliveryType(), null, TrackingStatus.CREATED.toString(),
									orderItem);
							
							Product product = item.getProduct();
							product.setAvailableQuantity(availableQty-item.getQuantity());
							em.merge(product);
							
						}else{// item is no longer available
							item.setItemNoLongerAvailable(true);
							result.setMessage(BusinessErrorTypes.SOME_ITEMS_NOT_AVAILABLE);		
							result.setSuccessFlag(true);
							em.persist(item);
						}
					}
					System.out.println("after updte loop items size:" + items.size() + ":"+ items);
					
					System.out.println("before persisting " + order);
					em.persist(order);
					if(customerDao.removeUnavailableItemsFromCart(order.getCustomer().getCustomerId(),em)){
						parentTran.commit();
						System.out.println("after committing finally :" + order);
						result.setMessage(BusinessErrorTypes.SUCCESS);
						System.out.println("result.getMessage() in insertorder"+result.getMessage());
						result.setSuccessFlag(true);// confirms order successful	
					}	
				}else{
					result.setMessage(BusinessErrorTypes.NO_CART_ITEM_FOUND);
					result.setSuccessFlag(false);
				}

						
						
			} catch (Exception e) {
				e.printStackTrace();
				if (parentTran != null && parentTran.isActive())
					parentTran.rollback();
				order = null;
				result.setSuccessFlag(false);
				result.setMessage(BusinessErrorTypes.ORDER_CREATION_ERROR);
			} finally {
				closeEntityManager(em);
			}
		}else{
			result.setMessage(paymentDetailResult.getMessage());
			result.setSuccessFlag(paymentDetailResult.isSuccessFlag());
		}
		return result;
	}
	
	
	

	/**
	 * @param orderId
	 * @return
	 * @throws BusinessException 
	 */
	public Order getOrderDetails(Integer orderId) throws BusinessException {
		EntityManager em = getEntityManager();
		Order order = null;
		try {
			if (orderId != null) {
				order = getByID(orderId,em, false);
				order.getOrderItems();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException(BusinessErrorTypes.ERROR);

		} finally {

			closeEntityManager(em);

		}
		return order;

	}

	/** Cancel Order
	 * @param order
	 * @return
	 * @throws BusinessException 
	 */
	public boolean cancelOrder(Integer orderId) throws BusinessException {
		// TODO Auto-generated method stub
		boolean cancelled = false;
		EntityManager em = getEntityManager();
		EntityTransaction parentTran = em.getTransaction();
		
		
			Order order = getByID(orderId, em,false);
			parentTran.begin();
			try{
				List<OrderItem> orderItems = order.getOrderItems();
				
				for(OrderItem eachItem: orderItems){
					eachItem.setIsCancelled(true);
					eachItem.getDelivery().setTrackingStatus(TrackingStatus.CANCELED.toString());
				}
				order.setIsCancelled(true);
				persist(order,em);
				parentTran.commit();
				cancelled=true;
			} catch (Exception e) {
				e.printStackTrace();
				if (parentTran != null && parentTran.isActive())
					parentTran.rollback();
				throw new BusinessException(BusinessErrorTypes.ORDER_CANCEL_ERROR);

			} finally {
				closeEntityManager(em);
			}
			
		return cancelled;

	}
	public boolean isExists(Integer id) throws BusinessException {
		// TODO Auto-generated method stub
		return (getByID(id,false)!= null);
		
	}
	
	public List<Order> getOrdersForCustomer(Integer customerID) throws BusinessException{
		
		List<Order> orders = null;
		
		if(customerID !=null && customerID >0){
			EntityManager em = getEntityManager();
			


				try{

					em.getTransaction().begin();
					TypedQuery<Order> query = em.createNamedQuery("Order.findOrdersForCustomer",Order.class);
					query.setParameter("customerIDValue", customerID);
					orders = query.getResultList();
					
					em.getTransaction().commit();
				}catch(Exception e){
					e.printStackTrace();
					if(em!=null && em.isOpen() && em.getTransaction().isActive())
						em.getTransaction().rollback();
					throw new BusinessException(BusinessErrorTypes.ERROR);
				}finally{
					
					closeEntityManager(em);
					
				}
				
				
				
			}// end of if
			
			return orders;

	}

}
