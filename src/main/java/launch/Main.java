package launch;

public class Main {

    public static void main(String[] args) throws Exception {

        /*String webappDirLocation = "src/main/webapp/";
        Tomcat tomcat = new Tomcat();

        //The port that we should run on can be set into an environment variable
        //Look for that variable and default to 8080 if it isn't there.
        String webPort = System.getenv("PORT");
        if(webPort == null || webPort.isEmpty()) {
            webPort = "8080";
        }

        tomcat.setPort(Integer.valueOf(webPort));
        try{
        Context context = tomcat.addWebapp("/", new File(webappDirLocation).getAbsolutePath());
        
        URI dburi = new URI(System.getenv("DATABASE_URL"));
        if(!dburi.getScheme().equals("postgres")) {
            System.out.println("I only know how to configure postgres databases. "+
                     "Don't know how to configure a data source for this URL: That doesn't mean it can't be done. I suggest forking the buildpack and hacking it on your own.");
            return;
        }

        String url = "jdbc:postgresql://"+dburi.getHost()+(dburi.getPort()!=-1? ":"+dburi.getPort() : "") + dburi.getPath();
        String username = dburi.getUserInfo().split(":")[0];
        String password = dburi.getUserInfo().split(":")[1];

        @SuppressWarnings("serial")
        Map<String,String> config = new HashMap<String,String>() {
            {
                put("maxActive", "20");
                put("maxIdle", "10");
                put("maxWait", "-1");
                put("name", "jdbc/JustShopDS");
            }
        };
        
        String jndiConfigJson = System.getenv("DATABASE_JNDI");
        if(jndiConfigJson!=null) {
            JSONObject jndiConfig = new JSONObject(jndiConfigJson);
            for(String s : config.keySet()) {
                try {
                    config.put(s,jndiConfig.getString(s));
                }
                catch (JSONException e) {}
            }
        }
        ContextResource c = new ContextResource();

        c.setAuth("Container");
        c.setType("javax.sql.DataSource");
        c.setName(config.get("name"));
        c.setProperty("username", username);
        c.setProperty("password", password);
        c.setProperty("url", url);
        c.setProperty("driverClassName", "org.postgresql.Driver");
        c.setProperty("maxActive", config.get("maxActive"));
        c.setProperty("maxIdle", config.get("maxIdle"));
        c.setProperty("maxWait", config.get("maxWait"));

        context.getNamingResources().addResource(c);

        System.out.println("Configured JNDI data source from environment: $DATABASE_URL => "+
                    config.get("name")+
                    ": url="+url+
                    ", username="+username+
                    ", config="+config);
        } catch (URISyntaxException e) {
        	System.out.println("Could not configure a JNDI data source from DATABASE_URL. Invalid URL: "+System.getenv("DATABASE_URL"));
        } catch (JSONException e) {
        // TODO Auto-generated catch block
        		e.printStackTrace();
        }


        
        System.out.println("configuring app with basedir: " + new File("./" + webappDirLocation).getAbsolutePath());

        tomcat.start();
        tomcat.getServer().await();  */
    }
}
