# JustShop #

JustShop is a completely RESTful web application which provides the user or client application JSON/XML based representations in to RESTful HTTP requests.

The web service is deployed into heroku and can be **publicly accessed** using the URLs provided for each functionality below:
### Details to Access Application

* Search item database by product
    * Search items having **"Baby"**    
    https://justshop-dev.herokuapp.com/services/product/search/Baby   
    * You could change last part of URL to say **"B"** to get all items with B   
    https://justshop-dev.herokuapp.com/services/product/search/B

* Get Customer Details
    * Get customer with ID 1   
    https://justshop-dev.herokuapp.com/services/customer/1

#### Use of HATEOAS
Hypermedia is used for changing state of application by providing links to change state of application.

* Get Shopping Cart of Customer having ID 5   
   https://justshop-dev.herokuapp.com/services/customer/5/cart/get/

#### Error handling 

Error handling is done as well to provide meaningful messages and appropriate HTTP Status codes to clients.

* Get customer who does not exist   
    https://justshop-dev.herokuapp.com/services/customer/100

#### POST, PUT and DELETE request examples

The PUT and POST would need a request object in either JSON or XML format. 
The DELETE uses HTTP delete method to be used. Hence just clicking the links would not work and appropriate HTTP request has to be created by client using proper HTTP verbs.

* POST - Insert New Customer:
    * https://justshop-dev.herokuapp.com/services/customer/save
    * JSON object:

```
#!json

     {
  "lastName": "Sreekumar",
  "firstName": "Aneesh",
  "email": "a@gmail.com",
  "phoneNumber": "504-845-1427"
}

```

# Developer Documentation #
## Technical Stack ##

  * **Language** : Java 1.7
  * **REST Framework** : Apache CXF 2.7.0
  * **Database** : Postgresql 9.4
  * **Server** : Apache Tomcat 7.0.57
  * **Others** : Hibernate/JPA 4.0.1.Final
  * **Tools/API** : Eclipse, JUnit 4.1, Apache Maven 3.2.5

## Design Document ##

https://drive.google.com/file/d/0B-mBbYzwM9mIcjJQLXBUQlU2MDg/view?usp=sharing


## How do I get set up? ##

### Configuration ###

#### Git installation:

    https://git-scm.com/book/en/v2/Getting-Started-Installing-Git

**SourceTree (Visual tool to check in check out files etc)**

     https://www.sourcetreeapp.com/


### Resources:

#### Git/SourceTree usage:

Go through the terms provided in the below one:
https://www.atlassian.com/git/tutorials/syncing

Go through the first three workflows in that same order. 

https://www.atlassian.com/git/tutorials/comparing-workflows

Would be sending a test repo in which I have added you so that you could try the creating branch merging creating pull requests etc. 

#### JPA configuration and patterns used:

Source: https://docs.jboss.org/hibernate/orm/4.0/hem/en-US/html/transactions.html#transactions-demarcation-jta

** 5.2. Database transaction demarcation **

Database (or system) transaction boundaries are always necessary. No communication with the database can occur outside of a database transaction (this seems to confuse many developers who are used to the auto-commit mode). Always use clear transaction boundaries, even for read-only operations. Depending on your isolation level and database capabilities this might not be required but there is no downside if you always demarcate transactions explicitly. You'll have to do operations outside a transaction, though, when you'll need to retain modifications in an EXTENDED persistence context.

A JPA application can run in non-managed (i.e. standalone, simple Web- or Swing applications) and managed Java EE environments. In a non-managed environment, an EntityManagerFactory is usually responsible for its own database connection pool. The application developer has to manually set transaction boundaries, in other words, begin, commit, or rollback database transactions itself. A managed environment usually provides container-managed transactions, with the transaction assembly defined declaratively through annotations of EJB session beans, for example. Programmatic transaction demarcation is then no longer necessary, even flushing the EntityManager is done automatically.

Usually, ending a unit of work involves four distinct phases:

commit the (resource-local or JTA) transaction (this automatically flushes the entity manager and persistence context)
close the entity manager (if using an application-managed entity manager)
handle exceptions
We'll now have a closer look at transaction demarcation and exception handling in both managed- and non-managed environments.

If an JPA persistence layer runs in a non-managed environment, database connections are usually handled by Hibernate's pooling mechanism behind the scenes. The common entity manager and transaction handling idiom looks like this:

	// Non-managed environment idiom
	EntityManager em = emf.createEntityManager();
	EntityTransaction tx = null;
	try {
	    tx = em.getTransaction();
	    tx.begin();

	    // do some work
	    ...

	    tx.commit();
	}
	catch (RuntimeException e) {
	    if ( tx != null && tx.isActive() ) tx.rollback();
	    throw e; // or display error message
	}
	finally {
	    em.close();
	}


You don't have to flush() the EntityManager explicitly - the call to commit() automatically triggers the synchronization.

A call to close() marks the end of an EntityManager. The main implication of close() is the release of resources - make sure you always close and never outside of guaranteed finally block.



#### Database configuration

**Setting up Datasource for Tomcat:**

Refer to config-tomcat for files used: 

**context.xml**
In your local servers/tomcat folder there would be context.xml

Under this tag
	 <WatchedResource>WEB-INF/web.xml</WatchedResource>

...

Add

    <Resource name="jdbc/JustShopDS" auth="Container" type="javax.sql.DataSource"  
            driverClassName="org.postgresql.Driver" url="jdbc:postgresql://localhost:5432/justshop"  
            username="****" password="****" maxActive="20" maxIdle="10" maxWait="-1" defaultTransactionIsolation="READ_COMMITTED" validationQuery="Select 1" />
        validationQuery="Select 1" />
	    <!-- Uncomment this to disable session persistence across Tomcat restarts -->

Then in **tomcat-users.xml**

	<role rolename="manager-gui"/>
		<role rolename="manager-script"/>
		<user username="****" password="***" roles="manager-gui,manager-script" />




#### Setting up PostGREMySQL:

1. Install postgresql v9.4.4 for windows 64 bit.

http://www.enterprisedb.com/products-services-training/pgdownload#windows

2. Open psql and connect using the pwd you used.
Just press enter for these three prompts localhost:, server:, port:, and user:
User would be postgres by default:
You can run
select current_user; to validate or see existing user
3. Then run
create database justshop;
and 
 \c justshop
 to use that database for subsequent queries
4. Confirm if persistence.xml has "create" and then run the tests

5. You should be able to see the tables created in **psql (SQL Shell)**

6. Do copy the context.xml and have the latest code with changes to pom.xml checked out in your local and run the redeploy maven  run configuration to deploy into tomcat. There shouldnt be an issue in creating.


**Maven run configurations in Eclipse:**

Refer to the Setup.docx Document in documents




## Issues/FAQ

** Getting compile time error "Duplicate generator named in persistence unit" error**

This is a JPA eclipse validation setting disable:
 * Select Window � Preferences
 * Expand Java Persistence � JPA � Errors/Warnings
 * Click Queries and generators
 * Set Duplicate generator defined to: Ignore
 * Click OK to apply changes and close the dialog
 * You can also set the value to Warning instead of Ignore.